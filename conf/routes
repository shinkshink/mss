# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~


# Home page
GET     /                                   					controllers.Application.index()

# Signup
GET     /captcha                    							controllers.SignUpController.captcha()
POST    /signup                     							controllers.SignUpController.submitAjax()

# Authentication
GET     /login                              					controllers.LoginController.login()
POST    /login                              					controllers.LoginController.authenticate()
GET     /logout                             					controllers.LoginController.logout()
GET     /admin                                                  controllers.LoginController.adminLogin()
POST    /admin                                                  controllers.LoginController.adminAuthenticate()
GET     /adminLogout                                            controllers.LoginController.adminLogout()

# Administrator
GET     /admin/main                                             controllers.AdminController.index()
#GET     /admin/deactivate                                       controllers.AdminController.deactivate()
#GET     /admin/activiate                                        controllers.AdminController.activate()
GET     /admin/listmember                                       controllers.AdminController.indexListMembers()
GET    /admin/viewMembers                                      controllers.AdminController.viewMembers()
POST    /admin/deactivate                                       controllers.AdminController.deactivate(username: String)
POST    /admin/activate                                         controllers.AdminController.activate(username: String)
GET     /admin/report/indexActiveMembers                        controllers.AdminController.indexActiveMembers()
GET     /admin/indexChangePassword                              controllers.AdminController.indexChangePassword()
POST    /admin/changePassword                                   controllers.AdminController.changePassword(username: String, password: String, repeatPassword: String)
GET     /admin/report/viewSalesReport						    controllers.ReportController.viewSalesReport()
GET     /admin/report/indexSalesReport                          controllers.ReportController.indexSalesReport()
GET		/admin/report/viewActiveMembers							controllers.ReportController.viewActiveMembers()
GET     /admin/report/indexDownloadReport    					controllers.ReportController.indexDownloadReport()
GET		/admin/report/viewDownloadReport						controllers.ReportController.viewDownloadReport()
GET     /admin/report/downloadReport/:name                      controllers.ReportController.downloadReport(name: String)
GET     /admin/indexAddFacilitator                              controllers.AdminController.indexAddFacilitator()
POST    /admin/addFacilitator                                   controllers.AdminController.addFacilitator()
GET     /admin/indexListFacilitators                            controllers.AdminController.indexListFacilitators()
GET     /admin/viewFacilitators                                 controllers.AdminController.viewFacilitators()
POST    /admin/addECredits                                      controllers.AdminController.addECredits(username: String, addecredit: String)

# Member
GET		/member													controllers.MemberController.index()
GET		/member/profile											controllers.MemberProfileController.display()
POST	/member/profile											controllers.MemberProfileController.submit()
GET		/member/countries										controllers.MemberProfileController.getCountries()
GET     /member/pendingassociates               				controllers.MemberController.indexPendingAssociates()
GET     /pendingassociates                  					controllers.MemberController.listPendingAssociates()
POST	/member/approve/:accountId/:mtype/:cmtype/:natype		controllers.MemberController.approveMember(accountId: Long, mtype: String, cmtype: String, natype: String)
GET     /member/transfer                        				controllers.MemberTransferController.displayTransferCredit()
POST    /member/transferCredit/:r/:amt         					controllers.MemberTransferController.submitTransferCredit(r: String, amt: String)
GET		/member/details/  										controllers.MemberController.getMemberByUsername(userName: String)

GET		/merchant/dashboard										controllers.MerchantController.dashboard()
POST    /merchant/purchase/:r/:amt         						controllers.MerchantController.submitPurchase(r: String, amt: String)
GET    	/merchant/sales         								controllers.MerchantController.salesHistory()
GET    /merchant/viewSales         								controllers.MerchantController.viewSales()

GET     /member/network/listAssociate/          				controllers.MemberNetworkController.listAssociates(userName: String, level:Int)
GET		/member/network/unilevel								controllers.MemberNetworkController.indexUnilevel(userName: String)
GET     /member/addOwnAccount         							controllers.MemberController.indexAddOwnAccount()
GET     /member/submitAddOwnAccount/:u/:pl      				controllers.MemberController.submitAddOwnAccount(u: String, pl: String)
GET     /member/displayAccountMember/:u         				controllers.MemberController.displayAccountMemberRewards(u: String)

#Board
GET		/member/network/board   								controllers.MemberNetworkController.indexBoard(userName: String)
GET     /member/network/getParentBoardAccount                   controllers.MemberNetworkController.getParentBoardAccount(board: Long)
GET		/member/network/displayParentBoard                      controllers.MemberNetworkController.displayParentBoard(board: Long)

# Rewards
GET     /member/reward/encashment             			 		controllers.RewardController.indexEncashment()
GET     /member/reward/cashEncashment/:amt			    		controllers.RewardController.submitCashEncashment(amt: String)

# JsRoutes
GET     /assets/js/routes             			controllers.Application.jsRoutes()
# Map static resources from the /public folder to the /assets URL path
GET     /assets/*file                       	controllers.Assets.at(path="/public", file)
