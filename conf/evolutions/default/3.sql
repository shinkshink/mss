# --- Sample dataset

# --- !Ups

--WARNING
--Please run command 'play test' after editing this file to make sure data is consistent with test scenarios
--If there are errors please update the unit tests accordingly

insert into person (person_id, first_name, middle_name, last_name) values (1, 'Reiner Jay','Jimenez', 'Juratil');
insert into account (account_id, person_id, email, password) values (1, 1, 'skyliondynastyking@gmail.com', '$2a$12$qXcExYi96E9ArnQiYJOCqOgB7AR/RyZQngKxgZXBrA0R8fL4N6sEG');
insert into member (member_id, active, approved_date, rank, registration_date, username, account_id, member_type_id) values (  1, true, current_timestamp, 'gold', current_timestamp, 'zillionaire', 1, 1);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (2, 1, 2, 2500, current_timestamp);

--member sponsored by jay
insert into person (person_id, first_name, last_name) values (2, 'Daniel', 'Elidia');
insert into account (account_id, person_id, email, password) values (2, 2, 'daniel@gmail.com', '$2a$12$iAjqtdbQ//ydwDcDbkzESejYI0UEi3UsRCGOJcz06XQgshFEi71XK');
insert into member (member_id, active, approved_date, rank, registration_date, username, account_id, member_type_id, sponsor_member_id, placement_member_id) values (2, true, current_timestamp, 'gold', current_timestamp, 'daniel', 2, 2, 1, 1);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (3, 2, 1, 3000, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (4, 2, 2, 1500, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (5, 2, 3, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (6, 2, 4, 0, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (1, 2, 1, 3, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (2, 2, 2, 4, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (3, 2, 3, 2, current_timestamp);


--test data for lists
insert into person (person_id, first_name, last_name) values (3, 'Nelson', 'Biasura');
insert into account (account_id, person_id, email, password) values (3, 3, 'nelson@gmail.com', '$2a$12$iAjqtdbQ//ydwDcDbkzESejYI0UEi3UsRCGOJcz06XQgshFEi71XK');
insert into member (member_id, active, approved_date, rank, registration_date, username, account_id, member_type_id, sponsor_member_id, placement_member_id) values (3, true, current_timestamp, 'gold', current_timestamp, 'nelson', 3, 2, 1, 1);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (7, 3, 1, 500, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (8, 3, 2, 600, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (9, 3, 3, 100, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (10, 3, 4, 0, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (4, 3, 1, 6, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (5, 3, 2, 8, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (6, 3, 3, 0, current_timestamp);

insert into person (person_id, first_name, last_name) values (4, 'Lantern', 'Biasura');
insert into account (account_id, person_id, email, password) values (4, 4, 'lantern@gmail.com', '$2a$12$iAjqtdbQ//ydwDcDbkzESejYI0UEi3UsRCGOJcz06XQgshFEi71XK');
insert into member (member_id, active, approved_date, rank, registration_date, username, account_id, member_type_id, sponsor_member_id, placement_member_id) values (4, true, current_timestamp, 'gold', current_timestamp, 'lantern', 4, 2, 1, 1);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (11, 4, 1, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (12, 4, 2, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (13, 4, 3, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (14, 4, 4, 0, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (7, 4, 1, 5, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (8, 4, 2, 12, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (9, 4, 3, 17, current_timestamp);

insert into person (person_id, first_name, last_name) values (5, 'Archer', 'Biasura');
insert into account (account_id, person_id, email, password) values (5, 5, 'archer@gmail.com', '$2a$12$iAjqtdbQ//ydwDcDbkzESejYI0UEi3UsRCGOJcz06XQgshFEi71XK');
insert into member (member_id, active, approved_date, rank, registration_date, username, account_id, member_type_id, sponsor_member_id, placement_member_id) values (5, true, current_timestamp, 'gold', current_timestamp, 'archer', 5, 2, 1, 1);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (15, 5, 1, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (16, 5, 2, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (17, 5, 3, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (18, 5, 4, 0, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (10, 5, 1, 2, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (11, 5, 2, 1, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (12, 5, 3, 0, current_timestamp);

insert into person (person_id, first_name, last_name) values (6, 'Superman', 'Biasura');
insert into account (account_id, person_id, email, password) values (6, 6, 'superman@gmail.com', '$2a$12$iAjqtdbQ//ydwDcDbkzESejYI0UEi3UsRCGOJcz06XQgshFEi71XK');
insert into member (member_id, active, approved_date, rank, registration_date, username, account_id, member_type_id, sponsor_member_id, placement_member_id) values (6, true, current_timestamp, 'gold', current_timestamp, 'superman', 6, 2, 1, 1);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (19, 6, 1, 500, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (20, 6, 2, 600, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (21, 6, 3, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (22, 6, 4, 0, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (13, 6, 1, 1, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (14, 6, 2, 2, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (15, 6, 3, 3, current_timestamp);


--merchant sponsored by jay
insert into person (person_id, first_name, last_name) values (7, 'Serin', 'Osman');
insert into account (account_id, person_id, email, password) values (7, 7, 'serin.osman@gmail.com', '$2a$12$iAjqtdbQ//ydwDcDbkzESejYI0UEi3UsRCGOJcz06XQgshFEi71XK');
insert into member (member_id, active, approved_date, rank, registration_date, username, account_id, member_type_id, sponsor_member_id, placement_member_id) values (7, true, current_timestamp, 'gold', current_timestamp, 'serin', 7, 3, 1, 1);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (23, 7, 2, 1000, current_timestamp);
--member sponsored by daniel
insert into person (person_id, first_name, last_name) values (8, 'Kurt', 'Trevelyan');
insert into account (account_id, person_id, email, password) values (8, 8, 'kurt.trevelyan@gmail.com', '$2a$12$iAjqtdbQ//ydwDcDbkzESejYI0UEi3UsRCGOJcz06XQgshFEi71XK');
insert into member (member_id, active, approved_date, rank, registration_date, username, account_id, member_type_id, sponsor_member_id, placement_member_id) values (8, true, current_timestamp, 'gold', current_timestamp, 'kurt', 8, 2, 2, 2);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (24, 8, 1, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (25, 8, 2, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (26, 8, 3, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (27, 8, 4, 0, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (16, 8, 1, 22, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (17, 8, 2, 23, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (18, 8, 3, 31, current_timestamp);

--member sponsored by kurt
insert into person (person_id, first_name, last_name) values (9, 'Joey', 'Milan');
insert into account (account_id, person_id, email, password) values (9, 9, 'joey.milan@gmail.com', '$2a$12$iAjqtdbQ//ydwDcDbkzESejYI0UEi3UsRCGOJcz06XQgshFEi71XK');
insert into member (member_id, active, approved_date, rank, registration_date, username, account_id, member_type_id, sponsor_member_id, placement_member_id) values (9, true, current_timestamp, 'gold', current_timestamp, 'joey', 9, 2, 8, 8);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (28, 9, 1, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (29, 9, 2, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (30, 9, 3, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (31, 9, 4, 0, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (19, 9, 1, 2, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (20, 9, 2, 4, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (21, 9, 3, 7, current_timestamp);

--member sponsored by kurt
insert into person (person_id, first_name, last_name) values (10, 'andi', 'alcana');
insert into account (account_id, person_id, email, password) values (10, 10, 'andi.alacan@gmail.com', '$2a$12$iAjqtdbQ//ydwDcDbkzESejYI0UEi3UsRCGOJcz06XQgshFEi71XK');
insert into member (member_id, active, approved_date, rank, registration_date, username, account_id, member_type_id, sponsor_member_id, placement_member_id) values (10, true, current_timestamp, 'gold', current_timestamp, 'andi', 10, 2, 9, 9);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (32, 10, 1, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (33, 10, 2, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (34, 10, 3, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (35, 8, 4, 0, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (22, 10, 1, 1, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (23, 10, 2, 1, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (24, 10, 3, 1, current_timestamp);
insert into encashment (encashment_id, amount, account_id, encashment_date, member_id) values (2, 150, 10, current_timestamp, 10);

--member sponsored by kurt
insert into person (person_id, first_name, last_name) values (11, 'rolly', 'cebllaos');
insert into account (account_id, person_id, email, password) values (11, 11, 'rolly.alacan@gmail.com', '$2a$12$iAjqtdbQ//ydwDcDbkzESejYI0UEi3UsRCGOJcz06XQgshFEi71XK');
insert into member (member_id, active, approved_date, rank, registration_date, username, account_id, member_type_id, sponsor_member_id, placement_member_id) values (11, true, current_timestamp, 'gold', current_timestamp, 'rolly', 11, 2, 10, 10);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (36, 11, 1, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (37, 11, 2, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (38, 11, 3, 0, current_timestamp);
insert into reward (reward_id, member_id, reward_type_id, amount, last_update) values (39, 11, 4, 0, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (25, 11, 1, 7, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (26, 11, 2, 7, current_timestamp);
insert into network_affiliate (network_affiliate_id, account_id, network_affiliate_type_id, count_reward, last_update) values (27, 11, 3, 7, current_timestamp);
insert into encashment (encashment_id, amount, account_id, encashment_date, member_id) values (1, 100, 11, current_timestamp, 11);
insert into encashment (encashment_id, amount, account_id, encashment_date, member_id) values (3, 150, 11, current_timestamp, 11);

insert into member_type (member_type_id, code, description) values (4, 'admin', 'Administrator');

insert into person (person_id, first_name, middle_name, last_name) values (12, 'Administrator','Administrator', 'Administrator');
insert into account (account_id, person_id, email, password) values (12, 12, 'mss_admin@gmail.com', '$2a$12$iAjqtdbQ//ydwDcDbkzESejYI0UEi3UsRCGOJcz06XQgshFEi71XK');
insert into member (member_id, active, approved_date, rank, registration_date, username, account_id, member_type_id) values (  12, true, current_timestamp, 'admin', current_timestamp, 'admin', 12, 4);


--Stub
--insert into stub (stub_id, claimant_account_id, network_affiliate_type_id,generation_date)

# --- !Downs
delete from encashment;
delete from transfer_detail;
delete from transfer;
delete from purchase;
delete from reward;
delete from member;
delete from account;
delete from person;
delete from network_affiliate;
delete from enrollment;
delete from stub;

