# --- !Ups

create table account (
    account_id bigint not null,
    email varchar(255) not null,
    password varchar(255) not null,
    person_id bigint not null,
    primary key (account_id)
);

create table address (
    address_id bigint not null,
    city_province varchar(255),
    state varchar(255),
    street varchar(255),
    zip_code bigint,
    country_id bigint,
    person_id bigint not null,
    primary key (address_id)
);

create table country (
    country_id bigint not null,
    iso varchar(2) not null,
    iso3 varchar(3),
    name varchar(80) not null,
    num_code integer,
    phone_code integer not null,
    proper_name varchar(80) not null,
    primary key (country_id)
);

create table member (
    member_id bigint not null,
    active boolean not null,
    approved_date timestamp,
    expiry_date timestamp,
    rank varchar(255),
    registration_date timestamp,
    username varchar(255) not null,
    account_id bigint not null,
    member_type_id bigint,
    sponsor_member_id bigint,
    placement_member_id bigint,
    primary key (member_id)
);

create table member_type (
    member_type_id bigint not null,
    code varchar(255) not null,
    description varchar(255),
    primary key (member_type_id)
);

create table person (
    person_id bigint not null,
    birth_date timestamp,
    first_name varchar(255),
    gender varchar(255),
    last_name varchar(255),
    middle_name varchar(255),
    mobile_number varchar(255),
    primary key (person_id)
);

create table purchase (
    purchase_id bigint not null,
    amount decimal(19,5),
    purchase_date timestamp not null,
    buyer_account_id bigint not null,
    merchant_account_id bigint not null,
    primary key (purchase_id)
);

create table reward (
    reward_id bigint not null,
    amount decimal(19,5),
    last_update timestamp,
    member_id bigint not null,
    reward_type_id bigint not null,
    primary key (reward_id)
);

create table reward_type (
    reward_type_id bigint not null,
    code varchar(255) not null,
    description varchar(255),
    primary key (reward_type_id)
);

create table transaction_type (
    transaction_type_id bigint not null,
    code varchar(255) not null,
    description varchar(255),
    primary key (transaction_type_id)
);

create table transfer (
    transfer_id bigint not null,
    amount decimal(19,5),
    date timestamp not null,
    purchase_id bigint,
    receiver_member_id bigint not null,
    reward_type_id bigint not null,
    sender_member_id bigint not null,
    transfer_type_id bigint not null,
    primary key (transfer_id)
);

create table transfer_detail (
    transfer_detail_id bigint not null,
    available_amount decimal(19,5),
    previous_amount decimal(19,5),
    member_id bigint not null,
    transaction_type_id bigint not null,
    transfer_id bigint not null,
    primary key (transfer_detail_id)
);

create table unilevel (
    unilevel_id integer not null,
    amount decimal(19,2) not null,
    primary key (unilevel_id)
);

create table transfer_type (
    transfer_type_id bigint not null,
    code varchar(255) not null,
    description varchar(255),
    primary key (transfer_type_id)
);

create table club_member_enroll_type (
    club_member_enroll_type_id bigint not null,
    code varchar(255) not null,
    description varchar(255),
    primary key (club_member_enroll_type_id)
);

create table network_affiliate_type (
    network_affiliate_type_id bigint not null,
    code varchar(255) not null,
    description varchar(255),
    primary key (network_affiliate_type_id)
);

create table network_affiliate (
	network_affiliate_id bigint not null,
    count_reward bigint not null,
    last_update timestamp,
    account_id bigint not null,
    network_affiliate_type_id bigint not null,
    primary key (network_affiliate_id)
);

create table enrollment (
	enrollment_id bigint not null,
    sponsor_account_id bigint not null,
    sponsee_account_id bigint not null,
    club_member_enroll_type_id bigint,
    network_affiliate_type_id bigint,
    approved_date timestamp,
    primary key (enrollment_id)
);

create table stub (
  stub_id bigint not null,
  generation_date timestamp not null,
  stub_count bigint not null,
  claimant_account_id bigint not null,
  network_affiliate_type_id bigint not null,
  primary key (stub_id)
);

create table encashment (
  encashment_id bigint not null,
  amount numeric(19,5),
  account_id bigint not null,
  encashment_date timestamp not null,
  member_id bigint not null,
  primary key (encashment_id)
);


alter table account 
    add constraint FK_iajp1nugms7a5wl86ecnjamw2 
    foreign key (person_id) 
    references person;

alter table address 
    add constraint FK_nyyg5dlcs74rm1girctd3mubi 
    foreign key (country_id) 
    references country;

alter table address 
    add constraint FK_5k57pkctki2o1wpmk2880r74j 
    foreign key (person_id) 
    references person;

alter table member 
    add constraint FK_i54h1gvvnejys85e9d9qo9f2u 
    foreign key (account_id) 
    references account;

alter table member 
    add constraint FK_7nt1vwpgx3enrvj9oxqkmpwyp 
    foreign key (member_type_id) 
    references member_type;
    
alter table member 
    add constraint FK_7nt1vwpgx3enrvj9ofghyae4f6 
    foreign key (placement_member_id) 
    references member;

alter table member 
    add constraint FK_7v72817hvnemf0wdy1i7qo2sh 
    foreign key (sponsor_member_id) 
    references member;

alter table purchase 
    add constraint FK_l80etqohfs9v1e3tibnxo10au 
    foreign key (buyer_account_id) 
    references member;

alter table purchase 
    add constraint FK_lw9m3d0oviiob05n2fdrpq2v9 
    foreign key (merchant_account_id) 
    references member;
    
alter table reward 
    add constraint FK_ophj8ltuvsxg6fja60u5pjqec 
    foreign key (member_id) 
    references member;

alter table reward 
    add constraint FK_s62ycf1s9ulcd2m1tjuvc5u6f 
    foreign key (reward_type_id) 
    references reward_type;

alter table transfer 
    add constraint FK_fv4d5oyvf2pwyo5hug6k1guau 
    foreign key (purchase_id) 
    references purchase;

alter table transfer 
    add constraint FK_3ptjjq6d6wyfy1i66tipoo3cj 
    foreign key (receiver_member_id) 
    references member;

alter table transfer 
    add constraint FK_1jvwvl6x9s1hu5xx4toa3n68h 
    foreign key (reward_type_id) 
    references reward_type;

alter table transfer 
    add constraint FK_evnb8o3uu7pbvxdv9cpg1erd0 
    foreign key (sender_member_id) 
    references member;

alter table transfer_detail 
    add constraint FK_huxgei3swp82i21mohmid6skr 
    foreign key (member_id) 
    references member;

alter table transfer_detail 
    add constraint FK_9skbtjyq9evneyabq455hbbpq 
    foreign key (transaction_type_id) 
    references transaction_type;

alter table transfer_detail 
    add constraint FK_o4njsfo8cpo7iob2osja8xlx 
    foreign key (transfer_id) 
    references transfer;
    
alter table network_affiliate 
    add constraint FK_htuj8ltuvsxg6fja60uhd8qec 
    foreign key (account_id) 
    references account;

alter table network_affiliate 
    add constraint FK_f82ycf1s9ulcd2m1tjuvc4h6f 
    foreign key (network_affiliate_type_id) 
    references network_affiliate_type;
    
alter table enrollment 
    add constraint FK_j8ldrf1s9ngfdy71tjuvc9kjg 
    foreign key (sponsor_account_id) 
    references account;

alter table enrollment 
    add constraint FK_gtjkn8j7gagfdy71tjuv7jhnm 
    foreign key (sponsee_account_id) 
    references account;
    
alter table enrollment 
    add constraint FK_u8k9olbc5g6cd2m1tmbfggt6h
    foreign key (network_affiliate_type_id) 
    references network_affiliate_type;
    
alter table enrollment 
    add constraint FK_u8k9olbc5g6cd2m6fg5fd4se3
    foreign key (club_member_enroll_type_id) 
    references club_member_enroll_type;

alter table stub 
    add constraint FK_u9k9olbc5g6cd2m6fg5fd4de5
    foreign key (network_affiliate_type_id) 
    references network_affiliate_type;
    
alter table encashment 
    add constraint FK_u9k9olbc5g6cd2m6fg5fd4av3
    foreign key (account_id) 
    references account;
    
alter table encashment 
    add constraint FK_u9k9olbc5g6cd2m6fg5fd4se2
    foreign key (member_id) 
    references member;
    

create sequence account_seq;
create sequence address_seq;
create sequence country_seq;
create sequence member_seq;
create sequence member_type_seq;
create sequence person_seq;
create sequence purchase_seq;
create sequence reward_seq;
create sequence reward_type_seq;
create sequence transaction_type_seq;
create sequence transfer_detail_seq;
create sequence transfer_seq;
create sequence transfer_type_seq;
create sequence club_member_enroll_type_seq;
create sequence network_affiliate_type_seq;
create sequence network_affiliate_seq;
create sequence enrollment_seq;
create sequence stub_seq;
create sequence encashment_seq;

# --- !Downs
SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists account;
drop table if exists member;
drop table if exists member_type;
drop table if exists address;
drop table if exists country;
drop table if exists reward_type;
drop table if exists transaction_type;
drop table if exists reward;
drop table if exists transfer_detail;
drop table if exists transfer;
drop table if exists transfer_type;
drop table if exists unilevel;
drop table if exists purchase;
drop table if exists person;
drop table if exists club_member_enroll_type;
drop table if exists network_affiliate_type;
drop table if exists network_affiliate;
drop table if exists enrollment;
drop table if exists stub;
drop table if exists encashment;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists account_seq;
drop sequence if exists member_seq;
drop sequence if exists member_type_seq;
drop sequence if exists address_seq;
drop sequence if exists country_seq;
drop sequence if exists reward_type_seq;
drop sequence if exists transaction_type_seq;
drop sequence if exists reward_seq;
drop sequence if exists transfer_detail_seq;
drop sequence if exists transfer_seq;
drop sequence if exists transfer_type_seq;
drop sequence if exists purchase_seq;
drop sequence if exists person_seq;
drop sequence if exists club_member_enroll_type_seq;
drop sequence if exists network_affiliate_type_seq;
drop sequence if exists network_affiliate_seq;
drop sequence if exists enrollment_seq;
drop sequence if exists stub_seq;
drop sequence if exists encashment_seq;
