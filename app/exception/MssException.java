package exception;

public class MssException extends Exception {

	private static final long serialVersionUID = -4836712858531780591L;

	public MssException() {
		super();
	}

	public MssException(String message) {
		super(message);
	}
}
