import play.GlobalSettings;
import scheduler.EncashmentScheduler;
import scheduler.SponsorScheduler;
import scheduler.StubScheduler;

public class Global extends GlobalSettings {
	@Override
    public void onStart(play.Application application) {
		StubScheduler.callMultipleScheduler();
		EncashmentScheduler.callMultipleScheduler();
		SponsorScheduler.callMultipleScheduler();
	}
}
