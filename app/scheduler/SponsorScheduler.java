package scheduler;

import job.SponsorJob;

import org.quartz.CronScheduleBuilder;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;

import play.Logger;

public class SponsorScheduler {

    private static final String CLASSNAME = "SponsorScheduler";

    public static void callMultipleScheduler() {

        Logger.info(CLASSNAME + " callMultipleScheduler START");
        // Set the job definItion
    	Logger.info("User Home:" + System.getProperty("user.home"));
        JobDetailImpl sponsorJob = new JobDetailImpl();
        sponsorJob.setName("Sponsor Job");
        sponsorJob.setJobClass(SponsorJob.class);

        // Creating schedule time with trigger
        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity("sponsorTrigger", "groupSponsor")
                //For Test Run every 5 secs
//                .withSchedule(CronScheduleBuilder.cronSchedule("0/5 * * * * ?"))
                //For Test, Insert exact time and day
//                .withSchedule(CronScheduleBuilder.cronSchedule("0 00 04 ? * TUE *"))
                //For Prod, run every Saturday days at 11:59pm
                .withSchedule(CronScheduleBuilder.cronSchedule("0 59 23 ? * SAT *"))
                .build();

        //Start Job
        try {
            org.quartz.Scheduler schedulerMain = new StdSchedulerFactory()
            .getScheduler();
            schedulerMain.start();
            schedulerMain.scheduleJob(sponsorJob, trigger);

        } catch (SchedulerException e) {
            Logger.error(CLASSNAME + " callMultipleScheduler SchedulerException " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Logger.error(CLASSNAME + " callMultipleScheduler Exception " + e.getMessage());
        	e.printStackTrace();
        }
        Logger.info(CLASSNAME + " callMultipleScheduler END");
    }
}
