package scheduler;

import job.EncashmentJob;

import org.quartz.CronScheduleBuilder;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;

import play.Logger;

public class EncashmentScheduler {

    private static final String CLASSNAME = "EncashmentScheduler";

    public static void callMultipleScheduler() {
        Logger.info(CLASSNAME + " callMultipleScheduler START");

        // Set the job definItion
        JobDetailImpl encashJob = new JobDetailImpl();
        encashJob.setName("Encashment Job");
        encashJob.setJobClass(EncashmentJob.class);

        // Creating schedule time with trigger
        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity("encashTrigger", "groupEncashment")
                //For Test Run every 5 secs
                //.withSchedule(CronScheduleBuilder.cronSchedule("0/5 * * * * ?"))
                //For Test, Insert exact time and day
//                .withSchedule(CronScheduleBuilder.cronSchedule("0 55 2 ? * SUN *"))
                //For Prod, run every 15 days at 11:59pm
                .withSchedule(CronScheduleBuilder.cronSchedule("0 59 23 1/15 * ? *"))
                .build();

        //Start Job
        try {
            org.quartz.Scheduler schedulerMain = new StdSchedulerFactory()
            .getScheduler();
            schedulerMain.start();
            schedulerMain.scheduleJob(encashJob, trigger);

        } catch (SchedulerException e) {
            Logger.error(CLASSNAME + " callMultipleScheduler SchedulerException " + e.getMessage());
            e.printStackTrace();
        }

        Logger.info(CLASSNAME + " callMultipleScheduler END");
    }
}
