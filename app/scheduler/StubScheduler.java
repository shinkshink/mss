package scheduler;

import job.StubJob;

import org.quartz.CronScheduleBuilder;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;

import play.Logger;

public class StubScheduler {

    private static final String CLASSNAME = "StubScheduler";

    public static void callMultipleScheduler() {
        Logger.info(CLASSNAME + " callMultipleScheduler START");

        // Set the job definItion
        JobDetailImpl stubJob = new JobDetailImpl();
        stubJob.setName("First Job");
        stubJob.setJobClass(StubJob.class);

        // Creating schedule time with trigger
        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity("stubTrigger", "group1")
                //For Test Run every 5 secs
                //.withSchedule(CronScheduleBuilder.cronSchedule("0/5 * * * * ?"))
                //For Test, Insert exact time and day
//                .withSchedule(CronScheduleBuilder.cronSchedule("0 0 2 ? * MON *"))
                //For Prod, run every Saturday at 11:59pm
                .withSchedule(CronScheduleBuilder.cronSchedule("0 59 23 ? * SAT *"))
                .build();

        //Start Job
        try {
            org.quartz.Scheduler schedulerMain = new StdSchedulerFactory()
            .getScheduler();
            schedulerMain.start();
            schedulerMain.scheduleJob(stubJob, trigger);

        } catch (SchedulerException e) {
            Logger.error(CLASSNAME + " callMultipleScheduler SchedulerException " + e.getMessage());
            e.printStackTrace();
        }

        Logger.info(CLASSNAME + " callMultipleScheduler END");
    }
}
