package dao;

import models.BoardSlot;

import javax.persistence.Query;

import play.Logger;
import play.db.jpa.JPA;
import util.JpaResultHelper;

public class BoardSlotDao {
    private static final String CLASSNAME = "BoardSlotDao";

	public static void persist(BoardSlot boardSlot) {
	    Logger.info(CLASSNAME + " persist " + boardSlot);
    	JPA.em().persist(boardSlot);
    }

    public static BoardSlot merge(BoardSlot boardSlot) {
        Logger.info(CLASSNAME + " merge " + boardSlot);
    	return JPA.em().merge(boardSlot);
    }

    /**
     * Retrieve slot details of the given slot
     * @param slot number
     * @return counter
     */
    public static BoardSlot findBySlot(Long slot) {
        Logger.info(CLASSNAME + " findBySlot " + slot);

    	String queryString = "from BoardSlot where slot = ?";
    	Query query = JPA.em().createQuery(queryString);
    	query.setParameter(1, slot);
    	return (BoardSlot)JpaResultHelper.getSingleResultOrNull(query);
    }
}
