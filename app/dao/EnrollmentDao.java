package dao;

import models.Enrollment;
import play.Logger;
import play.db.jpa.JPA;

public class EnrollmentDao {

    private static final String CLASSNAME = "EnrollmentDao";
	public static void persist(Enrollment enroll) {
	    Logger.info(CLASSNAME + " persist " + enroll);
		JPA.em().persist(enroll);
	}

}
