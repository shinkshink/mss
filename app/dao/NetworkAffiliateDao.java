package dao;

import javax.persistence.Query;

import models.NetworkAffiliate;
import models.NetworkAffiliateType;
import play.Logger;
import play.db.jpa.JPA;
import util.JpaResultHelper;

public class NetworkAffiliateDao {

    private static final String CLASSNAME = "NetworkAffiliateDao";

	public static NetworkAffiliateType findNetworkAffiliateTypeByCode(String code) {
	    Logger.info(CLASSNAME + " findNetworkAffiliateTypeByCode " + code);

		String queryString = "from NetworkAffiliateType t where t.code = ?";
		Query query = JPA.em().createQuery(queryString);
		query.setParameter(1, code);
		return (NetworkAffiliateType) JpaResultHelper.getSingleResultOrNull(query);
	}

	public static void persist(NetworkAffiliate network) {
	    Logger.info(CLASSNAME + " persist " + network);

		JPA.em().persist(network);
	}

}
