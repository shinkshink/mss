package dao;

import models.Person;
import play.Logger;
import play.db.jpa.JPA;

public class PersonDao {
    private static final String CLASSNAME = "PersonDao";

	public static void persist(Person person) {
	    Logger.info(CLASSNAME + " persist " + person);
    	JPA.em().persist(person);
    }

    public static Person merge(Person person) {
        Logger.info(CLASSNAME + " merge  " + person);
    	return JPA.em().merge(person);
    }
}
