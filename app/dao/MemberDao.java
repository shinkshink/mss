package dao;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TemporalType;

import models.Member;
import models.MemberType;
import models.SearchResult;

import org.joda.time.DateTime;

import play.Logger;
import play.db.jpa.JPA;
import util.JpaResultHelper;
import util.PaginationControl;

public class MemberDao {
    private static final String CLASSNAME = "MemberDao";

	public static Member findById(Long id) {
	    Logger.info(CLASSNAME + " findById " + id);

        return JPA.em().find(Member.class, id);
    }

	public static Member findMemberByAccountWithLatestRegistrationDate(Long id) {
	    Logger.info(CLASSNAME + " findMemberByAccountWithLatestRegistrationDate " + id);

		String queryString = "from Member a where a.registrationDate = (select MAX(aa.registrationDate) from Member aa where aa.account.id = ?)";
		Query query = JPA.em().createQuery(queryString);
    	query.setParameter(1, id);
    	return (Member)JpaResultHelper.getSingleResultOrNull(query);
	}

	public static List<Member> findMemberByAccount(Long id) {
	    Logger.info(CLASSNAME + " findMemberByAccount " + id);

		String queryString = "from Member a where account_id= ?";
    	@SuppressWarnings("unchecked")
		List<Member> data = JPA.em()
                .createQuery(queryString)
                .setParameter(1, id)
                .getResultList();

    	return data;
	}

    public static Member findByUsername(String username) {
        Logger.info(CLASSNAME + " findByUsername " + username);

		String queryString = "from Member a where a.username = ?";
    	Query query = JPA.em().createQuery(queryString);
    	query.setParameter(1, username);
    	Logger.info(CLASSNAME + " findByUsername END");
    	return (Member)JpaResultHelper.getSingleResultOrNull(query);
	}

    public static Member findValidByUsername(String username) {
        Logger.info(CLASSNAME + " findValidByUsername " + username);

		String queryString = "from Member a where a.username = ? and a.approvedDate is not null";
    	Query query = JPA.em().createQuery(queryString);
    	query.setParameter(1, username);
    	return (Member)JpaResultHelper.getSingleResultOrNull(query);
	}

    public static Member findValidById(Long id) {
        Logger.info(CLASSNAME + " findValidById " + id);

		String queryString = "from Member a where a.id = ? and a.approvedDate is not null";
    	Query query = JPA.em().createQuery(queryString);
    	query.setParameter(1, id);
    	return (Member)JpaResultHelper.getSingleResultOrNull(query);
	}

    /**
     * Retrieve all users.
     */
    @SuppressWarnings("unchecked")
	public static List<Member> findAll() {
        Logger.info(CLASSNAME + " findAll");

    	return JPA.em()
    			.createQuery("from Member a")
    	        .getResultList();
    }

    /**
     * Retrieve a User from email.
     */
    public static Member findByEmail(String email) {
        Logger.info(CLASSNAME + " findByEmail " + email);

    	String queryString = "from Member a where a.account.email = ?";
    	Query query = JPA.em().createQuery(queryString);
    	query.setParameter(1, email);
    	return (Member)JpaResultHelper.getSingleResultOrNull(query);
    }
    public static void persist(Member member) {
        Logger.info(CLASSNAME + " persist " + member);

    	JPA.em().persist(member);
    }

    public static void merge(Member member) {
        Logger.info(CLASSNAME + " merge " + member);

    	JPA.em().merge(member);
    }

    public static MemberType findMemberTypeByCode(String code) {
        Logger.info(CLASSNAME + " findMemberTypeByCode " + code);

    	String queryString = "from MemberType m where m.code = ?";
    	Query query = JPA.em().createQuery(queryString);
    	query.setParameter(1, code);
    	return (MemberType)JpaResultHelper.getSingleResultOrNull(query);
    }

    /**
     * Return a page of associates
     *
     */
    public static SearchResult<Member> searchAssociates(PaginationControl params, Member sponsor) {
        Logger.info(CLASSNAME + " searchAssociates " + sponsor);

    	int page = params.getPage();
    	String filter = params.getFilter();
    	int pageSize = params.getPageSize();

        if(page < 1) page = 1;

        Long total = (Long)JPA.em()
            .createQuery("select count(m) from Member m where lower(m.account.person.firstName) like ? and m.sponsor = ? and m.approvedDate is not null")
            .setParameter(1, "%" + filter.toLowerCase() + "%")
            .setParameter(2, sponsor)
            .getSingleResult();

        @SuppressWarnings("unchecked")
		List<Member> data = JPA.em()
            .createQuery("from Member m where lower(m.account.person.firstName) like ? and m.sponsor = ? and m.approvedDate is not null order by m." +
            		params.getSortBy() + " " + params.getOrder())
            .setParameter(1, "%" + filter.toLowerCase() + "%")
            .setParameter(2, sponsor)
            .setFirstResult((page - 1) * pageSize)
            .setMaxResults(pageSize)
            .getResultList();
        return new SearchResult<Member>(data, total, page, pageSize);
    }


    /**
     * Return a page of associates
     *
     */
    public static List<Member> searchAssociates(Member sponsor) {
        Logger.info(CLASSNAME + " searchAssociates " + sponsor);

        @SuppressWarnings("unchecked")
		List<Member> data = JPA.em()
            .createQuery("from Member m where m.sponsor = ? and m.approvedDate is not null" )
            .setParameter(1, sponsor)
            .getResultList();
        return data;
    }

    /**
     * Return a page of associates
     *
     */
    public static List<Member> searchPendingAssociates(Member sponsor) {
        Logger.info(CLASSNAME + " searchPendingAssociates " + sponsor);

        @SuppressWarnings("unchecked")
		List<Member> data = JPA.em()
            .createQuery("from Member m where  m.sponsor = ? and m.approvedDate is null")
            .setParameter(1, sponsor)
            .getResultList();
        return data;
    }

    /**
     * Search active members with the given date range.
     * @param params PaginationControl
     * @param fromDate start date
     * @param toDate end date
     * @param admin admin member type code
     * @return SearchResult<Member>
     */
    public static SearchResult<Member> searchActiveMembersNotAdmin(PaginationControl params, DateTime fromDate, DateTime toDate, long admin) {
        Logger.info(CLASSNAME + " searchActiveMembersNotAdmin " + fromDate + " " + toDate);

    	int page = params.getPage();
    	String filter = params.getFilter();// member name
    	int pageSize = params.getPageSize();

    	DateTime startDate = null;
    	DateTime endDate = null;

    	if(fromDate == null) {
    		DateTime dt = new DateTime();
    		startDate = dt.withTimeAtStartOfDay();
    	}else {
    		startDate = fromDate.withTimeAtStartOfDay();
    	}

    	if(toDate == null) {
    		endDate = new DateTime();
    	}else {
    		DateTime current = new DateTime();
    		endDate = toDate.withTime(current.getHourOfDay(), current.getMinuteOfHour(), current.getSecondOfMinute(), current.getMillisOfSecond());
    	}

    	if(fromDate != null && toDate != null) {
        	if(endDate.isBefore(startDate)) {
        		startDate = null;
        		endDate = null;
        	}
    	}
        String count = "select count(m)";
        String columns = " from Member m where m.memberType.id != :adminParam and m.active = true and lower(m.account.person.firstName) like :firstNameParam";

        if(startDate != null) {
        	columns = columns + " and m.approvedDate >= :startDate";
        }

        if(endDate != null) {
        	columns = columns + " and m.approvedDate <= :endDate";
        }
        //ordering
        //count
        Query countQuery = JPA.em().createQuery(count + columns);
        countQuery.setParameter("adminParam", admin);
        String filterString = "";
        if(filter != null) {
        	filterString = filter.toLowerCase();
        }
        countQuery.setParameter("firstNameParam", "%" + filterString + "%");
        if(startDate != null) {
        	countQuery.setParameter("startDate", startDate.toDate(), TemporalType.TIMESTAMP);
        }
        if(endDate != null) {
        	countQuery.setParameter("endDate", endDate.toDate(), TemporalType.TIMESTAMP);
        }
        Long total = (Long) countQuery.getSingleResult();

        //sortby data
        String sortBy = "username";
        if(params.getSortBy().equals("username")) {
        	sortBy = "username";
        }
        if(params.getSortBy().equals("fullName")) {
        	sortBy = "account.person.firstName";
        }
        //actual data
        String select = "select m";
        String queryString = select + columns + " order by m." + sortBy + " " + params.getOrder();

        Query query = JPA.em().createQuery(queryString);
        query.setParameter("adminParam", admin);
        query.setParameter("firstNameParam", "%" + filterString + "%");
        if(startDate != null) {
        	query.setParameter("startDate", startDate.toDate(), TemporalType.TIMESTAMP);
        }
        if(endDate != null) {
        	query.setParameter("endDate", endDate.toDate(), TemporalType.TIMESTAMP);
        }

        //pagination
        query.setFirstResult(page);
        if (pageSize > 0) {
            query.setMaxResults(pageSize);
        }

        @SuppressWarnings("unchecked")
        List<Member> data = query.getResultList();
        return new SearchResult<Member>(data, total, page, data.size());

    }

    /**
     * Search active members
     * @param params PaginationControl
     * @param admin admin member type code
     * @return SearchResult<Member>
     */
    public static SearchResult<Member> searchMembersNotAdmin(PaginationControl params, long admin) {
        Logger.info(CLASSNAME + " searchMembersNotAdmin " + params.getPageSize());

    	int page = params.getPage();
    	String filter = params.getFilter();// member name
    	int pageSize = params.getPageSize();

        String count = "select count(m)";
        String columns = " from Member m where m.memberType.id != :adminParam and lower(m.account.person.firstName) like :firstNameParam";

        //ordering
        //count
        Query countQuery = JPA.em().createQuery(count + columns);
        countQuery.setParameter("adminParam", admin);
        String filterString = "";
        if(filter != null) {
        	filterString = filter.toLowerCase();
        }
        countQuery.setParameter("firstNameParam", "%" + filterString + "%");
        Long total = (Long) countQuery.getSingleResult();

        //sortby data
        String sortBy = "username";
        if(params.getSortBy().equals("username")) {
        	sortBy = "username";
        }
        if(params.getSortBy().equals("fullName")) {
        	sortBy = "account.person.firstName";
        }
        //actual data
        String select = "select m";
        String queryString = select + columns + " order by m." + sortBy + " " + params.getOrder();

        Query query = JPA.em().createQuery(queryString);
        query.setParameter("adminParam", admin);
        query.setParameter("firstNameParam", "%" + filterString + "%");

        //pagination
        query.setFirstResult(page);
        if (pageSize > 0) {
            query.setMaxResults(pageSize);
        }


        @SuppressWarnings("unchecked")
        List<Member> data = query.getResultList();
        return new SearchResult<Member>(data, total, page, data.size());

    }

    /**
     * Search active members
     * @param params PaginationControl
     * @param admin admin member type code
     * @return SearchResult<Member>
     */
    public static SearchResult<Member> searchFacilitators(PaginationControl params, long admin) {
        Logger.info(CLASSNAME + " searchFacilitators ");

        int page = params.getPage();
        String filter = params.getFilter();// member name
        int pageSize = params.getPageSize();

        String count = "select count(m)";
        String columns = " from Member m where m.memberType.id = :adminParam and lower(m.account.person.firstName) like :firstNameParam";

        //ordering
        //count
        Query countQuery = JPA.em().createQuery(count + columns);
        countQuery.setParameter("adminParam", admin);
        String filterString = "";
        if(filter != null) {
            filterString = filter.toLowerCase();
        }
        countQuery.setParameter("firstNameParam", "%" + filterString + "%");
        Long total = (Long) countQuery.getSingleResult();

        //sortby data
        String sortBy = "username";
        if(params.getSortBy().equals("username")) {
            sortBy = "username";
        }
        if(params.getSortBy().equals("fullName")) {
            sortBy = "account.person.firstName";
        }
        //actual data
        String select = "select m";
        String queryString = select + columns + " order by m." + sortBy + " " + params.getOrder();

        Query query = JPA.em().createQuery(queryString);
        query.setParameter("adminParam", admin);
        query.setParameter("firstNameParam", "%" + filterString + "%");

        //pagination
        query.setFirstResult(page);
        if (pageSize > 0) {
            query.setMaxResults(pageSize);
        }

        @SuppressWarnings("unchecked")
        List<Member> data = query.getResultList();
        return new SearchResult<Member>(data, total, page, data.size());

    }
}
