package dao;

import models.Stub;
import play.Logger;
import play.db.jpa.JPA;

public class StubDao {

    private static final String CLASSNAME = "MemberController";

	public static void persist(Stub stub) {
	    Logger.info(CLASSNAME + " persist " + stub);

    	JPA.em().persist(stub);
    	JPA.em().flush();
    }
}
