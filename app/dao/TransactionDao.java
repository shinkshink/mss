package dao;

import javax.persistence.Query;

import models.TransactionType;
import play.Logger;
import play.db.jpa.JPA;
import util.JpaResultHelper;

public class TransactionDao {
    private static final String CLASSNAME = "TransactionDao";

	public static TransactionType findTransactionTypeByCode(String code) {
	    Logger.info(CLASSNAME + " findTransactionTypeByCode " + code);

		String queryString = "from TransactionType t where t.code = ?";
		Query query = JPA.em().createQuery(queryString);
		query.setParameter(1, code);
		return (TransactionType) JpaResultHelper.getSingleResultOrNull(query);
	}
}
