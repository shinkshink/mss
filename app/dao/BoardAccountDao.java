package dao;

import java.util.List;

import javax.persistence.Query;

import models.BoardAccount;
import models.dto.BoardDto;
import play.Logger;
import play.db.jpa.JPA;
import util.JpaResultHelper;

public class BoardAccountDao {

    private static final String CLASSNAME = "BoardAccountDao";

	public static void persist(BoardAccount boardAccount) {
		Logger.info(CLASSNAME + " persist " + boardAccount);
    	JPA.em().persist(boardAccount);
    }

    public static BoardAccount merge(BoardAccount boardAccount) {
    	Logger.info(CLASSNAME + " merge " + boardAccount);
    	return JPA.em().merge(boardAccount);
    }

    /**
     * Find board details using id
     * @param id board_account_id
     * @return BoardAccount
     */
    public static BoardAccount findByBoardAccountId(long id) {
    	Logger.info(CLASSNAME + " findByBoardAccountId = " + id);
    	String queryString = "from BoardAccount where id = ?";
    	Query query = JPA.em().createQuery(queryString);
    	query.setParameter(1, id);
    	return (BoardAccount)JpaResultHelper.getSingleResultOrNull(query);
    }

	/**
	 * Search for members in the given board number
	 * @param board board number
	 * @return list of board details
	 */
	public static List<BoardAccount> searchByBoardNumber(long board) {
		Logger.info(CLASSNAME + " searchByBoardNumber = " + board);

		String queryString = "from BoardAccount ba where ba.boardNumber = ? order by ba.id";
    	@SuppressWarnings("unchecked")
		List<BoardAccount> data = JPA.em().createQuery(queryString).setParameter(1, board).getResultList();

    	return data;
	}

	/**
	 * Find the next available parent
	 * @return BoardAccount
	 */
	public static BoardAccount findNextAvailableParent() {
		Logger.info(CLASSNAME + " findNextAvailableParent START");

		String queryString = "from BoardAccount ba where ba.id = (select min(id) from BoardAccount where leftSponsee is null or rightSponsee is null)";
		Query query = JPA.em().createQuery(queryString);
		Logger.info(CLASSNAME + " findNextAvailableParent END");
		return (BoardAccount) JpaResultHelper.getSingleResultOrNull(query);
	}

	/**
	 * Return true if the next board of the given board already has existing users
	 * @param board board number
	 * @return boolean
	 */
	public static boolean isNextBoardExists(long board) {
	    Logger.info(CLASSNAME + " isNextBoardExists = " + board);

		String count = "select count(*) from BoardAccount where boardNumber = ?";
        Query countQuery = JPA.em().createQuery(count);
        countQuery.setParameter(1, board + 1);

        Long total = (Long) countQuery.getSingleResult();
        if (total > 0) {
        	return true;
        }
        return false;
	}

	/**
	 * Get all parent board accounts for the given board number
	 * @param board board number
	 * @return List<BoardAccount>
	 */
	public static List<BoardAccount> getParentBoardAccount(long board) {
	    Logger.info(CLASSNAME + " getParentBoardAccount = " + board);

		String queryString = "from BoardAccount ba  where (ba.parentSponsor.id not in " +
				"(select p.id from BoardAccount p where p.boardNumber = ?) or ba.parentSponsor.id is null) and ba.boardNumber = ? order by ba.id";
		Query query = JPA.em().createQuery(queryString);
		query.setParameter(1, board);
		query.setParameter(2, board);
		@SuppressWarnings("unchecked")
		List<BoardAccount> data = query.getResultList();
		return data;
	}

    /**
     * Search for members with the given parent and board number
     * @param id parent account id
     * @param board board number
     * @return list of board details
     */
	@SuppressWarnings("unchecked")
	public static List<BoardDto> searchByParentAndBoard(long id, long board) {
	    Logger.info(CLASSNAME + " searchByParentAndBoard = " + id + ", " + board);
		Query query = JPA.em().createNamedQuery("boardDataSql");
		query.setParameter(1, id);
		query.setParameter(2, board);
		query.setParameter(3, board);
		return query.getResultList();
	}
}
