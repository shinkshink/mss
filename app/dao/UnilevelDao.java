package dao;

import java.util.List;

import javax.persistence.Query;

import models.Unilevel;
import play.Logger;
import play.db.jpa.JPA;
import util.JpaResultHelper;

public class UnilevelDao {
    
    private static final String CLASSNAME = "UnilevelDao";
    
	public static Unilevel findUnilevelById(Integer levelNumber) {
	    Logger.info(CLASSNAME + " findUnilevelById " + levelNumber);
	    
		String queryString = "from Unilevel l where l.rank = ?";
		Query query = JPA.em().createQuery(queryString);
		query.setParameter(1, levelNumber);
		return (Unilevel) JpaResultHelper.getSingleResultOrNull(query);
	}

	@SuppressWarnings("unchecked")
	public static List<Unilevel> findAll() {
	    Logger.info(CLASSNAME + " findAll");
	   
		return (List<Unilevel>) JPA.em().createQuery("from Unilevel a")
				.getResultList();
	}
}
