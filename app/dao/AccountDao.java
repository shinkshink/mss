package dao;

import models.Account;
import play.Logger;
import play.db.jpa.JPA;

public class AccountDao {

    private static final String CLASSNAME = "AccountDao";

	public static Account findById(Long id) {
		Logger.info(CLASSNAME + " findById " + id);
        return JPA.em().find(Account.class, id);
    }

	public static void persist(Account account) {
		Logger.info(CLASSNAME + " persist " + account);
    	JPA.em().persist(account);
    }

    public static Account merge(Account account) {
    	Logger.info(CLASSNAME + " merge " + account);
    	return JPA.em().merge(account);
    }
}
