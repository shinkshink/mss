package dao;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TemporalType;

import models.Reward;
import models.RewardType;
import play.Logger;
import play.db.jpa.JPA;
import util.JpaResultHelper;

public class RewardDao {

    private static final String CLASSNAME = "RewardDao";

	public static RewardType findRewardTypeByCode(String code) {
	    Logger.info(CLASSNAME + " findRewardTypeByCode " + code);

		String queryString = "from RewardType r where r.code = ?";
		Query query = JPA.em().createQuery(queryString);
		query.setParameter(1, code);
		return (RewardType) JpaResultHelper.getSingleResultOrNull(query);
	}

	public static void persist(Reward reward) {
	    Logger.info(CLASSNAME + " persist " + reward);

		JPA.em().persist(reward);
	}

	/**
	 * Search rewards by reward type id.
	 * @param id reward type id
	 * @return List of Rewards
	 */
	public static List<Reward> searchRewardsByRewardType(long id) {
	    Logger.info(CLASSNAME + " searchRewardsByRewardType " + id);

		String queryString = "from Reward r where r.rewardType.id = ?";
    	@SuppressWarnings("unchecked")
		List<Reward> data = JPA.em().createQuery(queryString).setParameter(1, id).getResultList();

    	return data;
	}

	public static Reward findRewardAmount(long rewardType, long memberId) {
	    Logger.info(CLASSNAME + " findRewardAmount " + rewardType + " " + memberId);

	    String queryString = "from Reward r where r.rewardType.id = ? and r.member.id = ?";
	    Query query = JPA.em().createQuery(queryString);
	    query.setParameter(1, rewardType);
	    query.setParameter(2, memberId);
	    return (Reward) JpaResultHelper.getSingleResultOrNull(query);
	}

	/**
	 * Search rewards by reward type id.
	 * @param id reward type id
	 * @return List of Rewards
	 */
	public static List<Reward> searchRewardsByRewardTypeAndDate(long id) {
	    Logger.info(CLASSNAME + " searchRewardsByRewardTypeAndDate " + id);

		Calendar calendar = new GregorianCalendar();
        Date today = new GregorianCalendar().getTime();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, - 6);//substract the number of days to look back
        Date dateToLookBackAfter = calendar.getTime();

		String queryString = "from Reward r where r.rewardType.id = ? and r.amount > 0 and r.lastUpdate between ? and ?";
    	@SuppressWarnings("unchecked")
		List<Reward> data = JPA.em()
		    .createQuery(queryString)
		    .setParameter(1, id)
		    .setParameter(2, dateToLookBackAfter, TemporalType.TIMESTAMP)
		    .setParameter(3, today, TemporalType.TIMESTAMP)
		    .getResultList();

    	return data;
	}

	public static int resetRewardAmount(long id) {
	    Logger.info(CLASSNAME + " resetRewardAmount " + id);

		Calendar calendar = new GregorianCalendar();
        Date today = new GregorianCalendar().getTime();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, - 6);//substract the number of days to look back
        Date dateToLookBackAfter = calendar.getTime();

        String queryString = "update Reward r set r.amount = 0 where r.rewardType.id = ? and r.amount > 0 and r.lastUpdate between ? and ?";
    	@SuppressWarnings("unchecked")
		int count = JPA.em()
		    .createQuery(queryString)
		    .setParameter(1, id)
		    .setParameter(2, dateToLookBackAfter, TemporalType.TIMESTAMP)
		    .setParameter(3, today, TemporalType.TIMESTAMP)
		    .executeUpdate();

      return count;
	}

}
