package dao;

import javax.persistence.Query;

import models.Country;
import play.Logger;
import play.db.jpa.JPA;
import util.JpaResultHelper;

public class CountryDao {
    private static final String CLASSNAME = "CountryDao";

	public static Country findById(Long id) {
	    Logger.info(CLASSNAME + " findByid " + id);

		String queryString = "from Country c where c.id = ?";
    	Query query = JPA.em().createQuery(queryString);
    	query.setParameter(1, id);
    	return (Country)JpaResultHelper.getSingleResultOrNull(query);
	}
}
