package dao;

import javax.persistence.Query;

import models.ClubMemberEnrollType;
import play.Logger;
import play.db.jpa.JPA;
import util.JpaResultHelper;

public class ClubMemberEnrollDao {

    private static final String CLASSNAME = "ClubMemberEnrollDao";

	public static ClubMemberEnrollType findClubMemberEnrollTypeByCode(String code) {
	    Logger.info(CLASSNAME + " findClubMemberEnrollTypeByCode " + code);

		String queryString = "from ClubMemberEnrollType t where t.code = ?";
		Query query = JPA.em().createQuery(queryString);
		query.setParameter(1, code);
		return (ClubMemberEnrollType) JpaResultHelper.getSingleResultOrNull(query);
	}

}
