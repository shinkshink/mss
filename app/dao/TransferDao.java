package dao;

import javax.persistence.Query;

import models.TransferType;
import play.Logger;
import play.db.jpa.JPA;
import util.JpaResultHelper;

public class TransferDao {

    private static final String CLASSNAME = "TransferDao";

	public static TransferType findTransferTypeByCode(String code) {
	    Logger.info(CLASSNAME + " findTransferTypeByCode " + code);

		String queryString = "from TransferType t where t.code = ?";
		Query query = JPA.em().createQuery(queryString);
		query.setParameter(1, code);
		return (TransferType) JpaResultHelper.getSingleResultOrNull(query);
	}

}
