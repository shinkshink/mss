package dao;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TemporalType;

import models.Member;
import models.Purchase;
import models.SearchResult;
import models.dto.SalesEntityDto;

import org.joda.time.DateTime;

import play.Logger;
import play.db.jpa.JPA;
import util.JpaResultHelper;
import util.PaginationControl;

public class PurchaseDao {

    private static final String CLASSNAME = "PurchaseDao";

	public static Purchase findById(Long purchaseId) {
	    Logger.info(CLASSNAME + " findById " + purchaseId);

		String queryString = "from Purchase p where p.id = ?";
		Query query = JPA.em().createQuery(queryString);
		query.setParameter(1, purchaseId);
		return (Purchase) JpaResultHelper.getSingleResultOrNull(query);
	}

	public static void persist(Purchase purchase) {
	    Logger.info(CLASSNAME + " persist " + purchase);

		JPA.em().persist(purchase);
	}

	public static SearchResult<Purchase> searchPurchases(PaginationControl params, Long merchantId, DateTime fromDate, DateTime toDate) {
	    Logger.info(CLASSNAME + " searchPurchases " + merchantId);

    	int page = params.getPage();
    	String filter = params.getFilter();// member name
    	int pageSize = params.getPageSize();

    	DateTime startDate = null;
    	DateTime endDate = null;

    	if(fromDate == null) {
    		DateTime dt = new DateTime();
    		startDate = dt.withTimeAtStartOfDay();
    	}else {
    		startDate = fromDate.withTimeAtStartOfDay();
    	}

    	if(toDate == null) {
    		endDate = new DateTime();
    	}else {
    		DateTime current = new DateTime();
    		endDate = toDate.withTime(current.getHourOfDay(), current.getMinuteOfHour(), current.getSecondOfMinute(), current.getMillisOfSecond());
    	}

    	if(fromDate != null && toDate != null) {
        	if(endDate.isBefore(startDate)) {
        		startDate = null;
        		endDate = null;
        	}
    	}

//        if(page < 1) page = 1;
        String count = "select count(p)";
        String columns = " from Purchase p where p.merchant = :merchantParam and lower(p.buyer.account.person.firstName) like :firstNameParam";

        if(startDate != null) {
        	columns = columns + " and p.date >= :startDate";
        }

        if(endDate != null) {
        	columns = columns + " and p.date <= :endDate";
        }
        //ordering

        //count
        Member merchant = MemberDao.findById(merchantId);
        Query countQuery = JPA.em().createQuery(count + columns);
        countQuery.setParameter("merchantParam", merchant);
        String filterString = "";
        if(filter != null) {
        	filterString = filter.toLowerCase();
        }

        countQuery.setParameter("firstNameParam", "%" + filterString + "%");
        if(startDate != null) {
        	countQuery.setParameter("startDate", startDate.toDate(), TemporalType.TIMESTAMP);
        }
        if(endDate != null) {
        	countQuery.setParameter("endDate", endDate.toDate(), TemporalType.TIMESTAMP);
        }
        Long total = (Long) countQuery.getSingleResult();

        //sortby data
        String sortBy = "date";
        if(params.getSortBy().equals("username")) {
        	sortBy = "buyer.account.person.firstName";
        }
        if(params.getSortBy().equals("fullName")) {
        	sortBy = "buyer.account.person.firstName";
        }
        if(params.getSortBy().equals("amount")) {
        	sortBy = "amount";
        }

        //actual data
        String select = "select p";
        String queryString = select + columns + " order by p." + sortBy + " " + params.getOrder();
        Query query = JPA.em().createQuery(queryString);
        query.setParameter("merchantParam", merchant);
        query.setParameter("firstNameParam", "%" + filterString + "%");
        if(startDate != null) {
        	query.setParameter("startDate", startDate.toDate(), TemporalType.TIMESTAMP);
        }
        if(endDate != null) {
        	query.setParameter("endDate", endDate.toDate(), TemporalType.TIMESTAMP);
        }

        //pagination
        query.setFirstResult(page);
        if (pageSize > 0) {
            query.setMaxResults(pageSize);
        }

        @SuppressWarnings("unchecked")
		List<Purchase> data = query.getResultList();
        return new SearchResult<Purchase>(data, total, page, data.size());
    }

	/**
	 * Search Sales Report per Merchant.
	 * @param params
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public static SearchResult<SalesEntityDto> searchPurchasesByMerchant(PaginationControl params, DateTime fromDate, DateTime toDate) {
	    Logger.info(CLASSNAME + " searchPurchasesByMerchant " + fromDate + " " + toDate);

	    int page = params.getPage();
    	String filter = params.getFilter();// member name
    	int pageSize = params.getPageSize();

    	DateTime startDate = null;
    	DateTime endDate = null;

    	if(fromDate == null) {
    		DateTime dt = new DateTime();
    		startDate = dt.withTimeAtStartOfDay();;
    	}else {
    		startDate = fromDate.withTimeAtStartOfDay();
    	}

    	if(toDate == null) {
    		endDate = new DateTime();
    	}else {
    		DateTime current = new DateTime();
    		endDate = toDate.withTime(current.getHourOfDay(), current.getMinuteOfHour(), current.getSecondOfMinute(), current.getMillisOfSecond());
    	}

    	if(fromDate != null && toDate != null) {
        	if(endDate.isBefore(startDate)) {
        		startDate = null;
        		endDate = null;
        	}
    	}

        StringBuilder columns = new StringBuilder();
        columns.append("select username, first_name || ' ' || last_name as full_name, round(sum(p.amount), 2) as amount from Purchase p " +
        		" inner join Member m on m.member_id = p.merchant_account_id " +
        		" inner join Account a on a.account_id = m.account_id " +
        		" inner join Person ps on ps.person_id = a.person_id " +

        		" where lower(first_name) like :firstNameParam");

        if(startDate != null) {
        	columns.append(" and p.purchase_date >= :startDate");
        }

        if(endDate != null) {
        	columns.append(" and p.purchase_date <= :endDate");
        }

        columns.append("  group by username, full_name");
        //ordering

        //sortby data
        String sortBy = "amount";
        if(params.getSortBy().equals("username")) {
        	sortBy = "username";
        }
        if(params.getSortBy().equals("fullName")) {
        	sortBy = "full_name";
        }
        if(params.getSortBy().equals("amount")) {
        	sortBy = "amount";
        }

        Query countQuery = JPA.em().createNativeQuery(columns.toString() + " order by " + sortBy + " " + params.getOrder(), SalesEntityDto.class);
        String filterString = "";
        if(filter != null) {
        	filterString = filter.toLowerCase();
        }

        //count data
        countQuery.setParameter("firstNameParam", "%" + filterString + "%");
        if(startDate != null) {
        	countQuery.setParameter("startDate", startDate.toDate(), TemporalType.TIMESTAMP);
        }
        if(endDate != null) {
        	countQuery.setParameter("endDate", endDate.toDate(), TemporalType.TIMESTAMP);
        }

        @SuppressWarnings("unchecked")
        List<SalesEntityDto> entityList = countQuery.getResultList();
        Long total = Long.valueOf(entityList.size());

        //actual data
        Query query = JPA.em().createNativeQuery(columns.toString() + " order by " + sortBy + " " + params.getOrder(), SalesEntityDto.class);

        query.setParameter("firstNameParam", "%" + filterString + "%");
        if(startDate != null) {
        	query.setParameter("startDate", startDate.toDate(), TemporalType.TIMESTAMP);
        }
        if(endDate != null) {
        	query.setParameter("endDate", endDate.toDate(), TemporalType.TIMESTAMP);
        }

        //pagination
        query.setFirstResult(page);
        if (pageSize > 0) {
            query.setMaxResults(pageSize);
        }

        @SuppressWarnings("unchecked")
        List<SalesEntityDto> data = countQuery.getResultList();

        return new SearchResult<SalesEntityDto>(data, total, page, data.size());
    }
}
