package dao;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.TemporalType;

import models.Encashment;
import play.Logger;
import play.db.jpa.JPA;

public class EncashmentDao {

    private static final String CLASSNAME = "EncashmentDao";

	public static Encashment findById(Long id) {
	    Logger.info(CLASSNAME + " findById " + id);
        return JPA.em().find(Encashment.class, id);
    }

	public static void persist(Encashment encash) {
	    Logger.info(CLASSNAME + " persist " + encash);
    	JPA.em().persist(encash);
    }

    public static Encashment merge(Encashment encash) {
        Logger.info(CLASSNAME + " merge " + encash);
    	return JPA.em().merge(encash);
    }

    /**
     * Retrieve all users.
     */
    @SuppressWarnings("unchecked")
    public static List<Encashment> findEncashmentByDate() {
        Logger.info(CLASSNAME + " findEncashmentByDate");

        Calendar calendar = new GregorianCalendar();
        Date today = new GregorianCalendar().getTime();
//        calendar.setTimeZone(TimeZone.getTimeZone("UTC+1"));//Munich time
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, - 14);//substract the number of days to look back
        Date dateToLookBackAfter = calendar.getTime();

        return JPA.em()
                .createQuery("from Encashment e where e.encashmentDate between ? and ?")
                .setParameter(1, dateToLookBackAfter, TemporalType.TIMESTAMP)
                .setParameter(2, today, TemporalType.TIMESTAMP)
                .getResultList();
    }

}
