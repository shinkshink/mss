package util;

import java.util.Properties;

import models.Account;
import models.Address;
import models.Country;
import models.Member;
import models.MemberType;
import models.Person;
import models.Purchase;
import models.Reward;
import models.RewardType;
import models.Stub;
import models.TransactionType;
import models.Transfer;
import models.TransferDetail;
import models.TransferType;
import models.Unilevel;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;

import play.api.Play;

/**
 * small program that creates hibernate Configuration object and then dumps the
 * update script on the console. You should include the sql in evolutions/N.sql
 *
 * @author daniel
 *
 */
public class ExportSchema {

	public static void main(String[] args) {
		// TODO: configure log4j so that it doesnt't log hibernates info
		// messages
		// BasicConfigurator.configure();
	    Configuration conf = new Configuration();
		conf.addAnnotatedClass(Account.class);
		conf.addAnnotatedClass(Address.class);
		conf.addAnnotatedClass(Country.class);
		conf.addAnnotatedClass(Member.class);
		conf.addAnnotatedClass(MemberType.class);
		conf.addAnnotatedClass(Person.class);
		conf.addAnnotatedClass(Purchase.class);
		conf.addAnnotatedClass(Reward.class);
		conf.addAnnotatedClass(RewardType.class);
		conf.addAnnotatedClass(TransactionType.class);
		conf.addAnnotatedClass(Transfer.class);
		conf.addAnnotatedClass(TransferDetail.class);
		conf.addAnnotatedClass(TransferType.class);
		conf.addAnnotatedClass(Unilevel.class);
		conf.addAnnotatedClass(Stub.class);

		Properties hibernateProperties = new Properties();
		// TODO: read some of these properties from application.conf
		hibernateProperties.put("hibernate.hbm2ddl.auto", "");
		hibernateProperties.put("hibernate.show_sql", "false");
		hibernateProperties.put("hibernate.dialect",
				"org.hibernate.dialect.H2Dialect");
		hibernateProperties.put("hibernate.connection.driver_class",
				"org.h2.Driver");
		hibernateProperties.put("hibernate.connection.url", "jdbc:h2:mem:play");
		hibernateProperties.put("hibernate.connection.username", "");
		hibernateProperties.put("hibernate.connection.password", "");
		conf.setProperties(hibernateProperties);

		SchemaUpdate update = new SchemaUpdate(conf);
		// to system.out, but not to the database
		update.execute(true, false);
	}
}
