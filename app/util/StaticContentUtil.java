package util;

import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;

public class StaticContentUtil {
	
	public static final BigDecimal CLASSIC_ECREDITS_APPROVAL_AMOUNT = new BigDecimal("25");
	public static final BigDecimal PREMIUM_ECREDITS_APPROVAL_AMOUNT = new BigDecimal("75");
	public static final BigDecimal PREMIUM_ENROLL_ADD_REWARD = new BigDecimal("1");
	public static final String DEFAULT_RANK = "consultant";
	public static final BigDecimal PREMIUM_SPONSOR_AMOUNT = new BigDecimal("12.5");
	public static final DecimalFormat DEFAULT_DECIMAL_FORMAT = new DecimalFormat("0.0000");
	public static final DecimalFormat TWO_DECIMAL_FORMAT = new DecimalFormat("0.00");
	public static final BigDecimal MINIMUM_AMOUNT_TO_ENCASH = new BigDecimal("7.5");
	public static final double SERVICE_CHARGE = 1;
	//For Prod
	public static final String HOME_DIR = File.separator + "home" + File.separator + "mss_prod" 
	    + File.separator + "report";
	//For Dev
	//public static final String HOME_DIR = "/home/mss_prod/report";

}