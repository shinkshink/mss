package util;

import java.math.BigDecimal;

public enum MssConversionEnum {
	PESO_PER_RP("2000.00"),
	PESO_PER_DOLLAR("40.00"),
	PURCHASE_PER_ECREDIT("800.00"),
	PESO_PER_WP("5000.00");
	
	private BigDecimal value;
	
	private MssConversionEnum(String value) {
		this.value  = new BigDecimal(value);
	}
	
	public BigDecimal getValue() {
		return value;
	}
}