package util;

/**
 * @param page
 *            Page to display
 * @param pageSize
 *            Number of items per page
 * @param sortBy
 *            property used for sorting
 * @param order
 *            Sort order (either or asc or desc)
 * @param filter
 *            Filter applied on the name column
 *            
 * @author Dantot
 * 
 */
public class PaginationControl {
	private int page;
	private int pageSize;
	private String sortBy;
	private String order;
	private String filter;

	public PaginationControl(int page, int pageSize, String sortBy, String order,
			String filter) {
		super();
		this.page = page;
		this.pageSize = pageSize;
		this.sortBy = sortBy;
		this.order = order;
		this.filter = filter;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}
}
