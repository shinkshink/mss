package util;

import java.math.BigDecimal;

import models.Member;
import dao.MemberDao;

public class MemberUtil {

	public static boolean isMembemExist(String username){
			Member member = MemberDao.findByUsername(username);
		return (member != null) ? true : false;
	}

	public static String counterAddAccountUsername(Member member){
		Member latestMemberAccount = MemberDao.findMemberByAccountWithLatestRegistrationDate(member.account.id);
		String userName = latestMemberAccount.username;

		String appendZero = "";
		String lastThreeChar = userName.substring((userName.length() - 3), userName.length());
		BigDecimal count = null;

		if(lastThreeChar.matches("^\\d\\d\\d$")){
			count = new BigDecimal(lastThreeChar);
			BigDecimal newCount = count.add(new BigDecimal(1));

			String addedString = newCount.toString();
			Integer zeros = 3 - addedString.length();

			String addedZero = "";
			for(int x = zeros; x >0; x--){
				addedZero += "0";
			}
			appendZero = addedZero + addedString;
			userName = userName.substring(0, (userName.length() - 3));
		} else{
			appendZero = "001";
		}

		return userName += appendZero;
	}

}