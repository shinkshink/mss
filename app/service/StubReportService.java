package service;

import java.util.List;

import models.Account;
import models.NetworkAffiliateType;
import models.Stub;
import models.dto.StubScheduleDto;
import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import report.StubSchedulerReport;
import dao.StubDao;

public class StubReportService {

    private static final String CLASSNAME = "StubReportService";

	@SuppressWarnings("unchecked")
	@Transactional
	public static void generateStubReport() {
	    Logger.info(CLASSNAME + " generateStubReport START");

		//Run the query
		List<StubScheduleDto> stubReportList = JPA.em().createNamedQuery("stubReportSql")
				.getResultList();

		//Insert to Stub Table
		for (StubScheduleDto stubDto : stubReportList) {
			insertStubData(stubDto);
		}

		try {
			//Generate Excel Report
			createStubReportExcel(stubReportList);

			//Reset NetworkAffiliate table
			JPA.em().createQuery("update NetworkAffiliate set count_reward = 0").executeUpdate();
		} catch (Exception e) {
		    Logger.error(CLASSNAME + " generateStubReport Exception " + e.getMessage());
			e.printStackTrace();
		}
		Logger.info(CLASSNAME + " generateStubReport END");
	}

	@Transactional
	private static void insertStubData(StubScheduleDto stubDto) {
	    Logger.info(CLASSNAME + " insertStubData START");

		try {
			Stub stub = new Stub();
			Account claimant = new Account();
			stub.claimant = claimant;
			stub.claimant.id = stubDto.getAccountId();
			stub.generationDate = stubDto.getDateGen();
			stub.stubCnt = stubDto.getStubCnt();
			NetworkAffiliateType net = new NetworkAffiliateType();
			stub.networkAffiliateType = net;
			stub.networkAffiliateType.id = stubDto.getNetAffTypId();
			StubDao.persist(stub);
		} catch (Exception e) {
		    Logger.error(CLASSNAME + " insertStubData Exception " + e.getMessage());
			e.printStackTrace();
		}
		Logger.info(CLASSNAME + " insertStubData END");
	}

	private static void createStubReportExcel(List<StubScheduleDto> stubDtoList) {
	    Logger.info(CLASSNAME + " createStubReportExcel START");
		StubSchedulerReport stubReport = new StubSchedulerReport();
		stubReport.writeWeeklyStubReport(stubDtoList);
		Logger.info(CLASSNAME + " createStubReportExcel END");
	}
}
