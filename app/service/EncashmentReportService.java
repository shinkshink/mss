package service;

import java.util.List;

import models.Encashment;
import play.Logger;
import play.db.jpa.Transactional;
import report.EncashmentSchedulerReport;
import dao.EncashmentDao;

public class EncashmentReportService {

    private static final String CLASSNAME = "EncashmentReportService";

	@SuppressWarnings("unchecked")
	@Transactional
	public static void generateEncashmentReport() {

	    Logger.info(CLASSNAME + " generateEncashmentReport");

		//Run the query
		List<Encashment> encashmentReportList = EncashmentDao.findEncashmentByDate();

//		for (Encashment encashment : encashmentReportList) {
//		    System.out.println("Name: " + encashment.account.person.firstName + " " + encashment.account.person.lastName + "\n");
//		    System.out.println("Amount: " + encashment.amount + "\n");
//		    System.out.println("Date: " + encashment.encashmentDate + "\n");
//		}

		//Generate Excel Report
		createEncashmentReportExcel(encashmentReportList);

	}

	private static void createEncashmentReportExcel(List<Encashment> encashmentReportList) {
	    Logger.info(CLASSNAME + " createEncashmentReportExcel");

	    EncashmentSchedulerReport encashmentReport = new EncashmentSchedulerReport();
	    encashmentReport.writeEncashmentReport(encashmentReportList);
	}
}
