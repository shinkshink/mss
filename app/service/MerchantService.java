package service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import models.Member;
import models.MemberTypeEnum;
import models.Purchase;
import models.Reward;
import models.RewardType;
import models.RewardTypeEnum;
import models.SearchResult;
import models.TransactionType;
import models.TransactionTypeEnum;
import models.Transfer;
import models.TransferDetail;
import models.TransferType;
import models.TransferTypeEnum;
import models.dto.SalesDto;
import models.dto.SalesEntityDto;

import org.joda.time.DateTime;

import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import util.MssConversionEnum;
import util.PaginationControl;
import dao.MemberDao;
import dao.PurchaseDao;
import dao.RewardDao;
import dao.TransactionDao;
import dao.TransferDao;
import dao.UnilevelDao;
import exception.MssException;

public class MerchantService {
    private static final String CLASSNAME = "MerchantService";
	private static final DecimalFormat df = new DecimalFormat("0.00");
	private static final SimpleDateFormat DATE_FORMAT= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aaa");

	@Transactional
	public static void purchase(Long merchantId, Long customerId , BigDecimal amount) throws MssException  {
	    Logger.info(CLASSNAME + " purchase START");

		Member merchant = MemberDao.findById(merchantId);
		Member current = MemberDao.findById(customerId);
		BigDecimal rpValue = amount.divide(MssConversionEnum.PESO_PER_RP.getValue());
		BigDecimal wpValue = amount.divide(MssConversionEnum.PESO_PER_WP.getValue());
		int level = 1;

		if(current.memberType.code.equals(MemberTypeEnum.CLUB_MEMBER.getCode()) && merchant !=null
				&& merchant.memberType.code.equals(MemberTypeEnum.MERCHANT.getCode())) {
			Date today = new GregorianCalendar().getTime();

			/**
			 * check if has enough e-credit otherwise not permitted to transact purchase
			 * Throw MssException
			 */
			//START OF E-CREDIT SUBTRACT
			BigDecimal eCreditValue = amount.divide(MssConversionEnum.PURCHASE_PER_ECREDIT.getValue());

			Reward merchatReward = merchant.getEcreditReward();
			BigDecimal merchantPreviousAmount = merchatReward.amount;
			if(merchantPreviousAmount.compareTo(eCreditValue) < 0) {
				throw new MssException("Purchase amout is less than the debited E-Credit amount");
			}
			BigDecimal debitedEcreditRewardAmount = merchantPreviousAmount.subtract(eCreditValue);
			merchatReward.amount = debitedEcreditRewardAmount;
			merchatReward.lastUpdate = today;
			RewardDao.persist(merchatReward);
			Logger.info(CLASSNAME + " purchase subtract e-credit");
			// END OF E-CREDIT SUBTRACT

			//Start Record transaction of subtracted e-credit
			//record transaction
			Transfer purchaseTransfer = new Transfer();
			purchaseTransfer.amount = eCreditValue;
			purchaseTransfer.receiver = current;
			RewardType eCreditRewardType = RewardDao.findRewardTypeByCode(RewardTypeEnum.E_CREDIT.getCode());
			purchaseTransfer.rewardType = eCreditRewardType;
			TransferType transferType = TransferDao.findTransferTypeByCode(TransferTypeEnum.PURCHASE.getCode());
			purchaseTransfer.transferType = transferType;
			purchaseTransfer.sender = merchant;
			purchaseTransfer.date = today;
			JPA.em().persist(purchaseTransfer);
			//update member
			merchant.debitTransfers.add(purchaseTransfer);
			Logger.info(CLASSNAME + " purchase record transaction");

			/**
			 * NOTE:
			 * No credit transfer due if purchase
			 * current.creditTransfers.add(purchaseTransfer);

			 */
			//End Record transaction of subracted e-creidt

			//start record subtracted tranfer details e-credit
			/**
			 * Record Transfer Details
			 */
			TransferDetail purchaseTf = new TransferDetail();
			purchaseTf.transfer = purchaseTransfer;
			TransactionType debit = TransactionDao.findTransactionTypeByCode(TransactionTypeEnum.DEBIT.getCode());
			purchaseTf.transactionType = debit;
			purchaseTf.previousAmount = merchantPreviousAmount;
			purchaseTf.availableAmount = debitedEcreditRewardAmount;
			purchaseTf.member = merchant;
			JPA.em().persist(purchaseTf);
			//update transfer
			purchaseTransfer.transferDetails.add(purchaseTf);
			JPA.em().persist(purchaseTransfer);
			//update member
			merchant.transferDetails.add(purchaseTf);
			MemberDao.persist(merchant);
			Logger.info(CLASSNAME + " purchase record subtracted transfer");
			//end recored subtracted tranfer details e-credit

			Purchase purchase1 = new Purchase();
			purchase1.amount = amount;
			purchase1.buyer = current;
			purchase1.merchant = merchant;
			purchase1.date = today;
			Purchase purchase = JPA.em().merge(purchase1);
			current.purchasesAsMember.add(purchase);
			merchant.purchasesAsMerchant.add(purchase);
			//Personal Shopping Cash Reward
			BigDecimal bd = new BigDecimal(".3");
			BigDecimal personalCashReward = rpValue.multiply(bd);
			//Personal Wealth Points Reward
			BigDecimal personalWealthPointsReward = wpValue.multiply(bd);
			//updated cash reward wallet
			Reward personalCashRewardRecord = current.getCashReward();
			BigDecimal previousCashRewardAmount = personalCashRewardRecord.amount;
			BigDecimal newPersonalCashReward = previousCashRewardAmount.add(personalCashReward);
			personalCashRewardRecord.amount = newPersonalCashReward;
			personalCashRewardRecord.lastUpdate = today;
			RewardDao.persist(personalCashRewardRecord);
			Logger.info(CLASSNAME + " purchase update cash reward wallet");

			//updated wealth reward wallet
			Reward personalWealthRewardRecord = current.getWealthReward();
			BigDecimal previousWealthRewardAmount = personalWealthRewardRecord.amount;
			BigDecimal newPersonalWealthReward = previousWealthRewardAmount.add(personalWealthPointsReward);
			personalWealthRewardRecord.amount = newPersonalWealthReward;
			personalWealthRewardRecord.lastUpdate = today;
			RewardDao.persist(personalWealthRewardRecord);
			//record cash transaction
			Transfer ptf1 = new Transfer();
			ptf1.amount = personalCashReward;
			ptf1.receiver = current;
			RewardType cashRewardType = RewardDao.findRewardTypeByCode(RewardTypeEnum.CASH.getCode());
			ptf1.rewardType = cashRewardType;
			ptf1.sender = merchant;
			ptf1.date = today;
			ptf1.purchase = purchase;
			ptf1.transferType = transferType;
			Transfer ptf = JPA.em().merge(ptf1);
			current.creditTransfers.add(ptf);
			Logger.info(CLASSNAME + " purchase wealth reward wallet");

			//credit transfer details
			TransferDetail ptfd1 = new TransferDetail();
			ptfd1.transfer = ptf;
			//credit only, since merchants have no points to be debited
			TransactionType credit = TransactionDao.findTransactionTypeByCode(TransactionTypeEnum.CREDIT.getCode());
			ptfd1.transactionType = credit;
			ptfd1.previousAmount = current.getCashReward().amount;
			ptfd1.availableAmount = current.getCashReward().amount.add(personalCashReward);
			ptfd1.member = current;
			TransferDetail ptfd = JPA.em().merge(ptfd1);
			//update transfer
			ptf.transferDetails.add(ptfd);
			//update member
			current.transferDetails.add(ptfd);
			//update purchase
			purchase.transfers.add(ptf);
			Logger.info(CLASSNAME + " purchase update purchase");

			//record wealth transaction
			Transfer ptfw1 = new Transfer();
			ptfw1.amount = personalWealthPointsReward;
			ptfw1.receiver = current;
			RewardType wealthRewardType = RewardDao.findRewardTypeByCode(RewardTypeEnum.WEALTH_POINT.getCode());
			ptfw1.rewardType = wealthRewardType;
			ptfw1.sender = merchant;
			ptfw1.date = today;
			ptfw1.purchase = purchase;
			ptfw1.transferType = transferType;
			Transfer ptfw = JPA.em().merge(ptfw1);
			current.creditTransfers.add(ptfw);
			//credit transfer details
			TransferDetail ptfdw1 = new TransferDetail();
			ptfdw1.transfer = ptfw;
			//credit only, since merchants have no points to be debited
//			TransactionType credit = TransactionDao.findTransactionTypeByCode(TransactionTypeEnum.CREDIT.getCode());
			ptfdw1.transactionType = credit;
			ptfdw1.previousAmount = current.getWealthReward().amount;
			ptfdw1.availableAmount = current.getWealthReward().amount.add(personalWealthPointsReward);
			ptfdw1.member = current;
			TransferDetail ptfdw = JPA.em().merge(ptfdw1);
			//update transfer
			ptfw.transferDetails.add(ptfdw);
			Logger.info(CLASSNAME + " purchase record wealth transactions");

			//update member
			current.transferDetails.add(ptfdw);

			MemberDao.persist(current);
			//update purchase
			purchase.transfers.add(ptfw);

			//percentages to sponsors
			while(current.placement != null && level <= 12) {
				current = current.placement;
				if(!current.memberType.code.equals(MemberTypeEnum.FACILITATOR.getCode())) {
					if(current.active) {
						BigDecimal percentage = UnilevelDao.findUnilevelById(level).percent;

						//Process Transaction Cash Reward Unilevel
						BigDecimal calculatedCashRewardAmount = rpValue.multiply(percentage);
						//updated main wallet
						Reward sponsorCashReward = current.getCashReward();
						BigDecimal previousAmount = sponsorCashReward.amount;
						BigDecimal addedCashRewardAmount = calculatedCashRewardAmount.add(previousAmount);
						sponsorCashReward.amount = addedCashRewardAmount;
						sponsorCashReward.lastUpdate = today;
						RewardDao.persist(sponsorCashReward);
						//record cash transaction
						Transfer trfcash = new Transfer();
						trfcash.amount = calculatedCashRewardAmount;
						trfcash.receiver = current;
						trfcash.rewardType = cashRewardType;
						trfcash.sender = merchant;
						trfcash.date = today;
						trfcash.purchase = purchase;//purchase
						trfcash.transferType = transferType;
						Transfer transferCash = JPA.em().merge(trfcash);
						//update member
						current.creditTransfers.add(transferCash);
						//credit transfer details
						TransferDetail tfcash = new TransferDetail();
						tfcash.transfer = transferCash;
						//credit only, since merchants have no points to be debited
						tfcash.transactionType = credit;
						tfcash.previousAmount = previousAmount;
						tfcash.availableAmount = addedCashRewardAmount;
						tfcash.member = current;
						TransferDetail savedTf = JPA.em().merge(tfcash);
						//update transfer
						transferCash.transferDetails.add(savedTf);
						//update member
						current.transferDetails.add(savedTf);
						//update purchase
						purchase.transfers.add(transferCash);

						Logger.info(CLASSNAME + " purchase Process Transaction Cash Reward Unilevel");

						//Process Transaction Wealth Reward Unilevel
						BigDecimal calculatedWealthRewardAmount = wpValue.multiply(percentage);
						//updated main wallet
						Reward sponsorWealthReward = current.getWealthReward();
						BigDecimal previousWealthAmount = sponsorWealthReward.amount;
						BigDecimal addedWealthRewardAmount = calculatedWealthRewardAmount.add(previousWealthAmount);
						sponsorWealthReward.amount = addedWealthRewardAmount;
						sponsorWealthReward.lastUpdate = today;
						RewardDao.persist(sponsorWealthReward);
						//record cash transaction
						Transfer trfWealth = new Transfer();
						trfWealth.amount = calculatedWealthRewardAmount;
						trfWealth.receiver = current;
						trfWealth.rewardType = wealthRewardType;
						trfWealth.sender = merchant;
						trfWealth.date = today;
						trfWealth.purchase = purchase;//purchase
						trfWealth.transferType = transferType;
						Transfer transferWealth = JPA.em().merge(trfWealth);
						//update member
						current.creditTransfers.add(transferWealth);
						//credit transfer details
						TransferDetail tfWealth = new TransferDetail();
						tfWealth.transfer = transferWealth;
						//credit only, since merchants have no points to be debited
						tfWealth.transactionType = credit;
						tfWealth.previousAmount = previousWealthAmount;
						tfWealth.availableAmount = addedWealthRewardAmount;
						tfWealth.member = current;
						TransferDetail savedTfWealth = JPA.em().merge(tfWealth);
						//update transfer
						transferWealth.transferDetails.add(savedTfWealth);
						//update member
						current.transferDetails.add(savedTfWealth);
						//update purchase
						purchase.transfers.add(transferWealth);
						Logger.info(CLASSNAME + " purchase Process Transaction Wealth Reward Unilevel");

						MemberDao.merge(current);
					}
				}
				level++;
			}
		}
		Logger.info(CLASSNAME + " purchase END");
	}

	@Transactional
	public static SearchResult<SalesDto> searchPurchases(
			PaginationControl params, Long merchantId, DateTime fromDate, DateTime toDate) {
		SearchResult<Purchase> result = PurchaseDao.searchPurchases(params, merchantId, fromDate, toDate);
		Logger.info(CLASSNAME + " searchPurchases START");
		List<SalesDto> sales = new ArrayList<SalesDto>();
		for (Purchase p : result.getList()) {
			Member buyer = p.buyer;
			SalesDto dto = new SalesDto(
					p.buyer.username,
					buyer.account.person.firstName + " " + buyer.account.person.lastName,
					df.format(p.amount), DATE_FORMAT.format(p.date));
			sales.add(dto);
		}
		Logger.info(CLASSNAME + " searchPurchases END");
		return new SearchResult<SalesDto>(sales, result.getTotalRowCount(), result.getPageIndex(), result.getPageSize());
	}

	/**
	 * Search sales report per merchant.
	 * @param params Pagination Control
	 * @param fromDate start date
	 * @param toDate end date
	 * @return SearchResult<SalesEntityDto>
	 */
	public static SearchResult<SalesEntityDto> searchSalesReport(PaginationControl params, DateTime fromDate, DateTime toDate) {
	    Logger.info(CLASSNAME + " searchSalesReport");
		SearchResult<SalesEntityDto> result = PurchaseDao.searchPurchasesByMerchant(params, fromDate, toDate);
		int pageSize = result.getPageSize();
		if (pageSize < 0) {
		    pageSize = result.getList().size();
		}

		return new SearchResult<SalesEntityDto>(result.getList(), result.getTotalRowCount(), result.getPageIndex(), pageSize);
	}

}
