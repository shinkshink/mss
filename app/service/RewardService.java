package service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;

import models.Encashment;
import models.EncashmentDetail;
import models.Member;
import models.Reward;
import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import util.StaticContentUtil;
import dao.EncashmentDao;
import dao.MemberDao;
import dao.RewardDao;
import exception.MssException;

public class RewardService {

    private static final String CLASSNAME = "RewardService";

	@Transactional
	public static void processCashEncashment(Long id, BigDecimal amount) throws MssException {
	    Logger.info(CLASSNAME + " processCashEncashment START");

		Date today = new GregorianCalendar().getTime();
		Member member = MemberDao.findValidById(id);
		//check wallet
		Reward cashReward = member.getCashReward();
		BigDecimal cashRewardAmount = cashReward.amount;
		if (cashRewardAmount.compareTo(amount) < 0) {
		    Logger.error(CLASSNAME + " processCashEncashment Current amount is less than the debited amount");
			throw new MssException("Current amount is less than the debited amount");
			 }

		BigDecimal minimumAmountAllowed = new BigDecimal(
				StaticContentUtil.MINIMUM_AMOUNT_TO_ENCASH
						.toString());
		// updated main wallet
		// check if sponsor has sufficient e-credits
		 if (amount.compareTo(minimumAmountAllowed) < 0) {
		    Logger.error(CLASSNAME + " processCashEncashment Minimum allowed encashment is " + minimumAmountAllowed.toString());
			throw new MssException("Miminum allowed encashment is "+minimumAmountAllowed.toString());
		}

		Encashment encash = new Encashment();
		encash.account = member.account;
		encash.member =  member;
		encash.amount = amount;
		encash.encashmentDate = today;
		EncashmentDao.persist(encash);

		BigDecimal previousAmount = cashRewardAmount;
		BigDecimal debitedCashRewardAmount = previousAmount.subtract(amount);

		EncashmentDetail encashDetail = new EncashmentDetail();
		encashDetail.encashment = encash;
		encashDetail.previousAmount = previousAmount;
		encashDetail.availableAmount = debitedCashRewardAmount;
		encashDetail.member = member;
		encashDetail.date = today;
		JPA.em().persist(encashDetail);

		cashReward.amount = debitedCashRewardAmount;
		cashReward.lastUpdate = today;
		RewardDao.persist(cashReward);
		Logger.info(CLASSNAME + " processCashEncashment END");

	}

}
