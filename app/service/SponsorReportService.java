package service;

import java.util.List;

import models.Reward;
import play.Logger;
import play.db.jpa.Transactional;
import report.SponsorSchedulerReport;
import dao.RewardDao;

public class SponsorReportService {

    private static final String CLASSNAME = "SponsorReportService";
	/**
	 * Generate Sponsor Report.
	 */
	@Transactional
	public static void generateSponsorReport() {
	    Logger.info(CLASSNAME + " generateSponsorReport START");
		//Run the query
		List<Reward> rewardList = RewardDao.searchRewardsByRewardTypeAndDate(4L);

		int count = 0;

		try {
		    Logger.info("Count: " + rewardList.size());
			//Generate Excel Report
			createRewardReportExcel(rewardList);

			count = RewardDao.resetRewardAmount(4L);
		} catch (Exception e) {
		    Logger.error(CLASSNAME + " generateSponsorReport Exception " + e.getMessage());
			e.printStackTrace();
		}

		Logger.info("No. of updated records: " + count);
		Logger.info(CLASSNAME + " generateSponsorReport END");
	}

	/**
	 * Create Excel File.
	 * @param rewardList
	 */
	private static void createRewardReportExcel(List<Reward> rewardList) {
	    Logger.info(CLASSNAME + " createRewardReportExcel START");
		SponsorSchedulerReport sponsorReport = new SponsorSchedulerReport();
		sponsorReport.writeWeeklySponsorReport(rewardList);
		Logger.info(CLASSNAME + " createRewardReportExcel END");
	}
}
