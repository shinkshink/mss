package service;

import models.Member;
import play.Logger;
import dao.MemberDao;
import exception.MssException;

public class AdminService {

    private static final String CLASSNAME = "AdminService";
    /**
     * Update member.
     * @param member Member
     * @throws MssException
     */
	public static void updateMember(Member member) throws MssException {
	    Logger.info(CLASSNAME + " updateMember");
	    MemberDao.merge(member);
	}

}