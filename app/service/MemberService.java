package service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ListIterator;

import models.Account;
import models.BoardAccount;
import models.BoardSlot;
import models.ClubMemberEnrollType;
import models.ClubMemberEnrollTypeEnum;
import models.Enrollment;
import models.Member;
import models.MemberType;
import models.MemberTypeEnum;
import models.NetworkAffiliate;
import models.NetworkAffiliateType;
import models.NetworkAffiliateTypeEnum;
import models.Person;
import models.Reward;
import models.RewardType;
import models.RewardTypeEnum;
import models.SearchResult;
import models.TransactionType;
import models.TransactionTypeEnum;
import models.Transfer;
import models.TransferDetail;
import models.TransferType;
import models.TransferTypeEnum;
import models.dto.BoardDto;
import models.dto.MemberDto;

import org.joda.time.DateTime;

import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import security.BCrypt;
import util.MemberUtil;
import util.PaginationControl;
import util.StaticContentUtil;
import dao.AccountDao;
import dao.BoardAccountDao;
import dao.BoardSlotDao;
import dao.ClubMemberEnrollDao;
import dao.EnrollmentDao;
import dao.MemberDao;
import dao.NetworkAffiliateDao;
import dao.PersonDao;
import dao.RewardDao;
import dao.TransactionDao;
import dao.TransferDao;
import exception.MssException;

public class MemberService {

    private static final String CLASSNAME = "MemberService";

	/**
	 * Authenticate a User.
	 */
	public static Member authenticate(String username, String password, boolean isAdmin) {
	    Logger.info(CLASSNAME + " authenticate START");

		Member member = MemberDao.findByUsername(username);
		if (member != null) {
			String generatedSecuredPasswordHash = member.account.password;
			boolean matched = BCrypt.checkpw(password,
					generatedSecuredPasswordHash);
			if (matched && (member.approvedDate != null)) {
			    Logger.info(CLASSNAME + " authenticate matched");
			    if (isAdmin) {
		            if ("admin".equals(member.memberType.code)) {
		                return member;
			        }
			    } else {
			        if (!"admin".equals(member.memberType.code)) {
                        return member;
                    }
			    }
			}
		}
		Logger.info(CLASSNAME + " authenticate END");
		return null;
	}

	public static Member signUpMember(MemberDto memberDto) {
	    Logger.info(CLASSNAME + " signUpMember START");

		Person person = new Person(memberDto.firstName, memberDto.middleName,
				memberDto.lastName);
		PersonDao.persist(person);
		Account account = new Account(memberDto.email, BCrypt.hashpw(
				memberDto.password, BCrypt.gensalt(12)), person);

		AccountDao.persist(account);

		Date today = new GregorianCalendar().getTime();
		// create initial Organo Gold Affiliate reward counter
		NetworkAffiliateType organoGoldNetworkAffiliateType = NetworkAffiliateDao
				.findNetworkAffiliateTypeByCode(NetworkAffiliateTypeEnum.ORGANO_GOLD
						.getCode());
		NetworkAffiliate networkAffiliate1 = new NetworkAffiliate(account,
				organoGoldNetworkAffiliateType, new BigDecimal(0L), today);
		NetworkAffiliateDao.persist(networkAffiliate1);

		// create initial Vantage Affiliate reward counter
		NetworkAffiliateType vantageNetworkAffiliateType = NetworkAffiliateDao
				.findNetworkAffiliateTypeByCode(NetworkAffiliateTypeEnum.VANTAGE
						.getCode());
		NetworkAffiliate networkAffiliate2 = new NetworkAffiliate(account,
				vantageNetworkAffiliateType, new BigDecimal(0L), today);
		NetworkAffiliateDao.persist(networkAffiliate2);

		// create initial AIM Gold Affiliate reward counter
		NetworkAffiliateType aimNetworkAffiliateType = NetworkAffiliateDao
				.findNetworkAffiliateTypeByCode(NetworkAffiliateTypeEnum.AIM
						.getCode());
		NetworkAffiliate networkAffiliate3 = new NetworkAffiliate(account,
				aimNetworkAffiliateType, new BigDecimal(0L), today);
		NetworkAffiliateDao.persist(networkAffiliate3);

		account.networkAffiliates.add(networkAffiliate1);
		account.networkAffiliates.add(networkAffiliate2);
		account.networkAffiliates.add(networkAffiliate3);

		AccountDao.persist(account);

		Member member = new Member();
		member.username = memberDto.username;
		member.account = account;
		Member sponsorMember = MemberDao
				.findByUsername(memberDto.sponsorUsername);
		member.sponsor = sponsorMember;
		member.placement = sponsorMember;
		member.registrationDate = new GregorianCalendar().getTime();
		DateTime dt = new DateTime(member.registrationDate.getTime());
		DateTime expiryDateTime = dt.plusDays(5);
		member.expiryDate = expiryDateTime.toDate();
		member.rank = memberDto.rank;
		MemberDao.persist(member);

		sponsorMember.associates.add(member);
		MemberDao.persist(sponsorMember);

		Logger.info(CLASSNAME + " signUpMember END");
		return member;
	}

	public static Member approveAddAccount(String givenUserName,
			String placementUser, Member parentMember, BigDecimal amountToDebit)
			throws MssException {
	    Logger.info(CLASSNAME + " approveAddAccount START");
		Date today = new GregorianCalendar().getTime();

		Member placementMember = MemberDao.findByUsername(placementUser);

		// updated main wallet
		Reward reward = parentMember.getEcreditReward();
		BigDecimal previousCashRewardAmount = reward.amount;
		// check if sponsor has sufficient e-credits
		if (previousCashRewardAmount.compareTo(amountToDebit) < 0) {
		    Logger.error(CLASSNAME + " approveAddAccount You need " + amountToDebit.toString()
                    + " to get approval for your own account!");
			throw new MssException("You need " + amountToDebit.toString()
					+ " to get approval for your own account!");
		}

		// debit on the sponsor
		if (parentMember.memberType.code.equals("club_member")) {

			BigDecimal newEcreditReward = reward.amount.subtract(amountToDebit);
			reward.amount = newEcreditReward;
			reward.lastUpdate = today;
			RewardDao.persist(reward);
			Logger.info(CLASSNAME + " approveAddAccount add reward");

			Account acc = AccountDao.findById(parentMember.account.id);
			MemberType memberType = MemberDao
					.findMemberTypeByCode(MemberTypeEnum.CLUB_MEMBER.getCode());

			// Register Member
			Member member = new Member();
			member.username = givenUserName;
			member.account = acc;
			member.memberType = memberType;
			member.sponsor = parentMember.sponsor;
			member.placement = placementMember;
			member.registrationDate = new GregorianCalendar().getTime();
			member.rank = StaticContentUtil.DEFAULT_RANK;
			member.approvedDate = today;
			member.active = true;
			MemberDao.persist(member);

			Logger.info(CLASSNAME + " approveAddAccount register member");
			// adding member to you list of own member account
			acc.members.add(member);
			AccountDao.persist(acc);

			Logger.info(CLASSNAME + " approveAddAccount add account");
			// adding own account to list of associate
			parentMember.associates.add(member);
			MemberDao.persist(parentMember);
			Logger.info(CLASSNAME + " approveAddAccount add parent member");

			Member newOwnAccount = MemberDao
					.findMemberByAccountWithLatestRegistrationDate(acc.id);

			// create initial cash rewards (zero amount)
			RewardType cashRewardType = RewardDao
					.findRewardTypeByCode(RewardTypeEnum.CASH.getCode());
			Reward reward1 = new Reward(newOwnAccount, cashRewardType,
					new BigDecimal(0L), today);
			RewardDao.persist(reward1);
			Logger.info(CLASSNAME + " approveAddAccount initial cash reward");

			// create initial E-credit(zero amount)
			RewardType eCreditRewardType = RewardDao
					.findRewardTypeByCode(RewardTypeEnum.E_CREDIT.getCode());
			Reward reward2 = new Reward(newOwnAccount, eCreditRewardType,
					new BigDecimal(0L), today);
			RewardDao.persist(reward2);
			Logger.info(CLASSNAME + " approveAddAccount initial e credit");

			// create initial Wealth Points (zero amount)
			RewardType wealthRewardType = RewardDao
					.findRewardTypeByCode(RewardTypeEnum.WEALTH_POINT.getCode());
			Reward reward3 = new Reward(newOwnAccount, wealthRewardType,
					new BigDecimal(0L), today);
			RewardDao.persist(reward3);
			Logger.info(CLASSNAME + " approveAddAccount initial wealth points");

			// create initial Sponsor Points (zero amount)
			RewardType sponsorRewardType = RewardDao
					.findRewardTypeByCode(RewardTypeEnum.SPONSOR.getCode());
			Reward reward4 = new Reward(newOwnAccount, sponsorRewardType,
					new BigDecimal(0L), today);
			RewardDao.persist(reward4);
			Logger.info(CLASSNAME + " approveAddAccount initial sponsor points");

			newOwnAccount.rewards.add(reward1);
			newOwnAccount.rewards.add(reward2);
			newOwnAccount.rewards.add(reward3);
			newOwnAccount.rewards.add(reward4);

			MemberDao.persist(newOwnAccount);

			parentMember.associates.add(newOwnAccount);
			MemberDao.persist(parentMember);
			Logger.info(CLASSNAME + " approveAddAccount add member details");

			// Record Transaction
			Member newAddAccountMember = MemberDao
					.findByUsername(givenUserName);
			// record transaction
			Transfer transfer = new Transfer();
			transfer.amount = new BigDecimal(
					StaticContentUtil.CLASSIC_ECREDITS_APPROVAL_AMOUNT
							.toString());
			transfer.receiver = newAddAccountMember;
			transfer.rewardType = eCreditRewardType;
			TransferType transferType = TransferDao
					.findTransferTypeByCode(TransferTypeEnum.ENROLL.getCode());
			transfer.transferType = transferType;
			transfer.sender = parentMember;
			transfer.date = today;
			JPA.em().persist(transfer);
			// update member
			parentMember.debitTransfers.add(transfer);
			Logger.info(CLASSNAME + " approveAddAccount parent member");

			/**
			 * Record Transfer Details
			 */
			TransferDetail tf = new TransferDetail();
			tf.transfer = transfer;
			TransactionType debit = TransactionDao
					.findTransactionTypeByCode(TransactionTypeEnum.DEBIT
							.getCode());
			tf.transactionType = debit;
			tf.previousAmount = previousCashRewardAmount;
			tf.availableAmount = newEcreditReward;
			tf.member = parentMember;
			JPA.em().persist(tf);
			// update transfer
			transfer.transferDetails.add(tf);
			JPA.em().persist(transfer);
			// update member
			parentMember.transferDetails.add(tf);
			MemberDao.persist(parentMember);
			Logger.info(CLASSNAME + " approveAddAccount record transfer details");

		} else {
		    Logger.error(CLASSNAME + " approveAddAccount Adding account is for club member only");
			throw new MssException("Adding account is for club member only");
		}

		Member newMemberApproved = MemberDao.findByUsername(givenUserName);
		Logger.info(CLASSNAME + " approveAddAccount END");
		return newMemberApproved;
	}

	@Transactional
	public static void approveMember(Long newMemberIdForApproval,
			MemberTypeEnum memberTypeEnum, Member sponsorMember,
			ClubMemberEnrollTypeEnum clubMemberEnrolltypeEnum,
			NetworkAffiliateTypeEnum networkAffiliateTypeEnum)
			throws MssException, Exception {
	    Logger.info(CLASSNAME + " approveMember START");

		Member newMemberForApproval = MemberDao
				.findById(newMemberIdForApproval);
		Date today = new GregorianCalendar().getTime();
		// member type
		MemberType memberType = MemberDao.findMemberTypeByCode(memberTypeEnum
				.getCode());
		newMemberForApproval.memberType = memberType;

		if (clubMemberEnrolltypeEnum != null
				&& memberTypeEnum.equals(MemberTypeEnum.CLUB_MEMBER)) {
			// only Club Member has a clubmembertype

			if (clubMemberEnrolltypeEnum
					.equals(ClubMemberEnrollTypeEnum.CLASSIC)) {
			    Logger.info(CLASSNAME + " approveMember classic");

				// debit on the sponsor
				BigDecimal amountToDebit = new BigDecimal(
						StaticContentUtil.CLASSIC_ECREDITS_APPROVAL_AMOUNT
								.toString());
				// updated main wallet
				Reward reward = sponsorMember.getEcreditReward();
				BigDecimal previousCashRewardAmount = reward.amount;
				// check if sponsor has sufficient e-credits
				if (previousCashRewardAmount.compareTo(amountToDebit) < 0) {
				    Logger.error(CLASSNAME + " approveMember You need "
                            + amountToDebit.toString() + " to get approval.");
					throw new MssException("You need "
							+ amountToDebit.toString() + " to get approval.");
				}
				createClubMember(newMemberIdForApproval, newMemberForApproval,
						sponsorMember, today, amountToDebit);

				Member approvedMember = MemberDao
						.findById(newMemberIdForApproval);
				Account sponsorAccount = AccountDao
						.findById(sponsorMember.account.id);
				Enrollment enrollment = new Enrollment();
				enrollment.sponsor = sponsorAccount;
				enrollment.sponsee = approvedMember.account;
				enrollment.approvedDate = today;
				ClubMemberEnrollType clubMemberEnrollType = ClubMemberEnrollDao
						.findClubMemberEnrollTypeByCode(clubMemberEnrolltypeEnum
								.getCode());
				enrollment.clubMemberEnrollType = clubMemberEnrollType;
				sponsorAccount.enrollAsSponsor.add(enrollment);
				EnrollmentDao.persist(enrollment);
				AccountDao.persist(sponsorAccount);
				Logger.info(CLASSNAME + " approveMember enrollment");

			} else {
			    Logger.info(CLASSNAME + " approveMember not classic");

				// debit on the sponsor
				BigDecimal amountToDebit = new BigDecimal(
						StaticContentUtil.PREMIUM_ECREDITS_APPROVAL_AMOUNT
								.toString());
				// updated main wallet
				Reward reward = sponsorMember.getEcreditReward();
				BigDecimal previousCashRewardAmount = reward.amount;
				// check if sponsor has sufficient e-credits
				if (previousCashRewardAmount.compareTo(amountToDebit) < 0) {
				    Logger.error(CLASSNAME + " approveMember You need "
                            + amountToDebit.toString() + " to get approval.");
					throw new MssException("You need "
							+ amountToDebit.toString() + " to get approval.");
				}
				/*
				 * Note Premium membership is equivalent to 3 club member
				 * accounts
				 */
				// Create first account
				createClubMember(newMemberIdForApproval, newMemberForApproval,
						sponsorMember, today, amountToDebit);
				Member approvedMember = MemberDao
						.findById(newMemberIdForApproval);
				// Get new auto generated username
				String nextUserName = MemberUtil
						.counterAddAccountUsername(approvedMember);
				// Create additional two account
				String placementUserName = approvedMember.username;
				for (int x = 1; x < 3; x++) {
					Member premiumApprovedMember = addAccount(nextUserName,
							placementUserName, approvedMember);
					placementUserName = premiumApprovedMember.username;
					nextUserName = MemberUtil
							.counterAddAccountUsername(premiumApprovedMember);
				}
				Logger.info(CLASSNAME + " approveMember create first account");

				// updated sponsor's sponsor reward wallet
				Reward sponsorReward = sponsorMember.getSponsorReward();
//				Logger.info(CLASSNAME + " approveMember update sponsor1 " + sponsorReward);
				BigDecimal previousSponsorRewardAmount = sponsorReward.amount;
//				Logger.info(CLASSNAME + " approveMember update sponsor2");
				BigDecimal newPersonalSponsorReward = previousSponsorRewardAmount
						.add(StaticContentUtil.PREMIUM_SPONSOR_AMOUNT);
//				Logger.info(CLASSNAME + " approveMember update sponsor3");
				sponsorReward.amount = newPersonalSponsorReward;
//				Logger.info(CLASSNAME + " approveMember update sponsor4");
				sponsorReward.lastUpdate = today;
//				Logger.info(CLASSNAME + " approveMember update sponsor5");
				RewardDao.persist(sponsorReward);
				Logger.info(CLASSNAME + " approveMember update sponsor");

				// Record Enrollment
				Account sponsorAccount = AccountDao
						.findById(sponsorMember.account.id);
				Enrollment enrollment = new Enrollment();
				enrollment.sponsor = sponsorAccount;
				enrollment.sponsee = approvedMember.account;
				NetworkAffiliateType networkAffiliateType = NetworkAffiliateDao
						.findNetworkAffiliateTypeByCode(networkAffiliateTypeEnum
								.getCode());
				enrollment.networkAffiliateType = networkAffiliateType;
				enrollment.approvedDate = today;
				ClubMemberEnrollType clubMemberEnrollType = ClubMemberEnrollDao
						.findClubMemberEnrollTypeByCode(clubMemberEnrolltypeEnum
								.getCode());
				enrollment.clubMemberEnrollType = clubMemberEnrollType;

				sponsorAccount.enrollAsSponsor.add(enrollment);

				EnrollmentDao.persist(enrollment);
				AccountDao.persist(sponsorAccount);
				Logger.info(CLASSNAME + " approveMember enrollment");

				NetworkAffiliate networkAffiliate = null;

				if (networkAffiliateTypeEnum
						.equals(NetworkAffiliateTypeEnum.ORGANO_GOLD)) {
					networkAffiliate = sponsorAccount
							.getOrganoGoldNetworkAffiliate();
				} else if (networkAffiliateTypeEnum
						.equals(NetworkAffiliateTypeEnum.VANTAGE)) {
					networkAffiliate = sponsorAccount
							.getVantageNetworkAffiliate();
				} else {
					networkAffiliate = sponsorAccount.getAimNetworkAffiliate();
				}

				// Record Network Reward
				BigDecimal currentCount = networkAffiliate.count;
				networkAffiliate.count = currentCount
						.add(StaticContentUtil.PREMIUM_ENROLL_ADD_REWARD);
				networkAffiliate.lastUpdate = today;

				NetworkAffiliateDao.persist(networkAffiliate);

				try {
					//Record in Board 1
					BoardAccount boardAccount = new BoardAccount(sponsorAccount, approvedMember.account, today);

					//add parent
					boardAccount.parentSponsor = BoardAccountDao.findNextAvailableParent();

					BoardAccountDao.persist(boardAccount);
					Logger.info(CLASSNAME + " approveMember board account saved");
					//update parent's left or right child
					if (boardAccount.parentSponsor != null) {
						if (boardAccount.parentSponsor.leftSponsee == null) {
//						    Logger.info(CLASSNAME + " approveMember parentSponsor.leftSponsee");
							boardAccount.parentSponsor.leftSponsee = boardAccount;
						} else {
//						    Logger.info(CLASSNAME + " approveMember parentSponsor.rightSponsee");
							boardAccount.parentSponsor.rightSponsee = boardAccount;
						}
						BoardAccountDao.merge(boardAccount.parentSponsor);
					}

					Logger.info(CLASSNAME + " approveMember find slot");
					BoardSlot slot = BoardSlotDao.findBySlot(1L);
					if (slot == null ) {
						slot = new BoardSlot();
						slot.slot = 1L;
						slot.counter = 0;
					}
					slot.counter++;
					BoardSlotDao.merge(slot);
					Logger.info(CLASSNAME + " approveMember update slot");
					//recalculate board allocation
					for (int id = 1; id < 13; id++) {
						MemberService.recalculateBoard(id);
					}
					Logger.info(CLASSNAME + " approveMember add board details");
				} catch (Exception e) {
				    Logger.error(CLASSNAME + " approveMember Exception " + e.getMessage());
				    e.printStackTrace();
					throw new Exception("Board details error.");
//				} catch (Throwable e) {
//				    Logger.error(CLASSNAME + " approveMember Throwable " + e.getMessage());
//				    e.printStackTrace();
//                    throw new Exception("Board details error.");
				}
			}

		} else {

			// if membertype is MERCHANT No need to create reward, wealth
			// if reach here club member should be merchant only
			if (memberTypeEnum.equals(MemberTypeEnum.MERCHANT)) { // extra
																	// checking
																	// if there
																	// is
																	// another
																	// membertype
																	// in future

			    Logger.info(CLASSNAME + " approveMember merchant");

				// merchant requires ecredit to be debited every purchase
				newMemberForApproval.approvedDate = today;
				newMemberForApproval.active = true;
				RewardType eCreditRewardType = RewardDao
						.findRewardTypeByCode(RewardTypeEnum.E_CREDIT.getCode());
				Reward reward = new Reward(newMemberForApproval,
						eCreditRewardType, new BigDecimal(0L), today);
				RewardDao.persist(reward);
				newMemberForApproval.rewards.add(reward);
				MemberDao.persist(newMemberForApproval);
			}
		}
		Logger.info(CLASSNAME + " approveMember END");
	}

	@Transactional
	public static void transferCredit(Long senderId, Long receiverId,
			BigDecimal amount) throws MssException {
	    Logger.info(CLASSNAME + " transferCredit START");

		// 0 equals, 1 greater than, -1 less than
		if (amount.compareTo(new BigDecimal("0")) <= 0) {
		    Logger.error(CLASSNAME + " transferCredit Current amount is less zero or less than.");
			throw new MssException("Current amount is less zero or less than.");
		}
		Member senderMember = MemberDao.findValidById(senderId);
		Member receiverMember = MemberDao.findValidById(receiverId);
		if (receiverMember.memberType.code.equals(MemberTypeEnum.FACILITATOR
				.getCode())) {
		    Logger.error(CLASSNAME + " transferCredit Facilitator can't receive a e-credits");
			throw new MssException(
					"Facililator can't receive a e-credits");
		}

		Date today = new GregorianCalendar().getTime();
		// updated main wallet
		Reward senderReward = senderMember.getEcreditReward();
		BigDecimal senderPreviousAmount = senderReward.amount;
		if (senderPreviousAmount.compareTo(amount) < 0) {
		    Logger.error(CLASSNAME + " viewDownloadReportCurrent amount is less than the debited amount");
			throw new MssException(
					"Current amount is less than the debited amount");
		}
		BigDecimal debitedCashRewardAmount = senderPreviousAmount
				.subtract(amount);
		senderReward.amount = debitedCashRewardAmount;
		senderReward.lastUpdate = today;
		RewardDao.persist(senderReward);
		Reward receiverReward = receiverMember.getEcreditReward();
		BigDecimal receiverPreviousAmount = receiverReward.amount;
		BigDecimal sumCashRewardAmount = receiverPreviousAmount.add(amount);
		receiverReward.amount = sumCashRewardAmount;
		receiverReward.lastUpdate = today;
		RewardDao.persist(receiverReward);
		Logger.info(CLASSNAME + " transferCredit main wallet");

		// record transaction
		Transfer transfer = new Transfer();
		transfer.amount = amount;
		transfer.receiver = receiverMember;
		RewardType cashRewardType = RewardDao
				.findRewardTypeByCode(RewardTypeEnum.E_CREDIT.getCode());
		transfer.rewardType = cashRewardType;
		TransferType transferType = TransferDao
				.findTransferTypeByCode(TransferTypeEnum.TRANSFER.getCode());
		transfer.transferType = transferType;
		transfer.sender = senderMember;
		transfer.date = today;
		JPA.em().persist(transfer);
		Logger.info(CLASSNAME + " transferCredit record transaction");

		// update member
		senderMember.debitTransfers.add(transfer);
		receiverMember.creditTransfers.add(transfer);
		// credit transfer details to receiver
		TransferDetail tf = new TransferDetail();
		tf.transfer = transfer;
		TransactionType credit = TransactionDao
				.findTransactionTypeByCode(TransactionTypeEnum.CREDIT.getCode());
		tf.transactionType = credit;
		tf.previousAmount = receiverPreviousAmount;
		tf.availableAmount = sumCashRewardAmount;
		tf.member = receiverMember;
		JPA.em().persist(tf);
		Logger.info(CLASSNAME + " transferCredit update member");

		// update transfer
		transfer.transferDetails.add(tf);
		// update member
		receiverMember.transferDetails.add(tf);
		MemberDao.persist(receiverMember);
		Logger.info(CLASSNAME + " transferCredit update transfer");

		// debit
		TransferDetail tf2 = new TransferDetail();
		tf2.transfer = transfer;
		TransactionType debit = TransactionDao
				.findTransactionTypeByCode(TransactionTypeEnum.DEBIT.getCode());
		tf2.transactionType = debit;
		tf2.previousAmount = senderPreviousAmount;
		tf2.availableAmount = debitedCashRewardAmount;
		tf2.member = senderMember;
		JPA.em().persist(tf2);
		Logger.info(CLASSNAME + " transferCredit debit");

		// update transfer
		transfer.transferDetails.add(tf2);
		JPA.em().persist(transfer);
		// update member
		senderMember.transferDetails.add(tf2);
		MemberDao.persist(senderMember);

		Logger.info(CLASSNAME + " transferCredit END");
	}

	private static Member createClubMember(Long memberId, Member member,
			Member sponsorMember, Date today, BigDecimal amountToDebit)
			throws MssException {
	    Logger.info(CLASSNAME + " createClubMember START");

		Reward reward = sponsorMember.getEcreditReward();
		BigDecimal previousCashRewardAmount = reward.amount;

		member.approvedDate = today;
		member.active = true;

		BigDecimal newEcreditReward = reward.amount.subtract(amountToDebit);
		reward.amount = newEcreditReward;
		reward.lastUpdate = today;
		RewardDao.persist(reward);
		Logger.info(CLASSNAME + " createClubMember reward");

		// create initial rewards (zero amount)
		RewardType cashRewardType = RewardDao
				.findRewardTypeByCode(RewardTypeEnum.CASH.getCode());
		Reward reward1 = new Reward(member, cashRewardType, new BigDecimal(0L),
				today);
		RewardDao.persist(reward1);

		RewardType eCreditRewardType = RewardDao
				.findRewardTypeByCode(RewardTypeEnum.E_CREDIT.getCode());
		Reward reward2 = new Reward(member, eCreditRewardType, new BigDecimal(
				0L), today);
		RewardDao.persist(reward2);

		RewardType wealthRewardType = RewardDao
				.findRewardTypeByCode(RewardTypeEnum.WEALTH_POINT.getCode());
		Reward reward3 = new Reward(member, wealthRewardType,
				new BigDecimal(0L), today);
		RewardDao.persist(reward3);

		RewardType sponsorRewardType = RewardDao
				.findRewardTypeByCode(RewardTypeEnum.SPONSOR.getCode());
		Reward reward4 = new Reward(member, sponsorRewardType, new BigDecimal(
				0L), today);
		RewardDao.persist(reward4);

		member.rewards.add(reward1);
		member.rewards.add(reward2);
		member.rewards.add(reward3);
		member.rewards.add(reward4);

		MemberDao.persist(member);
		Logger.info(CLASSNAME + " createClubMember persist rewards");

		// find again to get updated entity
		Member newMember = MemberDao.findById(memberId);
		// record transaction
		Transfer transfer = new Transfer();
		transfer.amount = amountToDebit;
		transfer.receiver = newMember;
		transfer.rewardType = eCreditRewardType;
		TransferType transferType = TransferDao
				.findTransferTypeByCode(TransferTypeEnum.ENROLL.getCode());
		transfer.transferType = transferType;
		transfer.sender = sponsorMember;
		transfer.date = today;
		JPA.em().persist(transfer);
		Logger.info(CLASSNAME + " createClubMember record transaction");

		// update member
		sponsorMember.debitTransfers.add(transfer);

		TransferDetail tf = new TransferDetail();
		tf.transfer = transfer;
		TransactionType debit = TransactionDao
				.findTransactionTypeByCode(TransactionTypeEnum.DEBIT.getCode());
		tf.transactionType = debit;
		tf.previousAmount = previousCashRewardAmount;
		tf.availableAmount = newEcreditReward;
		tf.member = sponsorMember;
		JPA.em().persist(tf);
		// update transfer
		transfer.transferDetails.add(tf);
		JPA.em().persist(transfer);
		// update member
		sponsorMember.transferDetails.add(tf);
		MemberDao.persist(sponsorMember);

		Logger.info(CLASSNAME + " createClubMember END");
		return newMember;
	}

	public static Member addAccount(String givenUserName, String placementUser,
			Member parentMember) throws MssException {
	    Logger.info(CLASSNAME + " addAccount START");

		Date today = new GregorianCalendar().getTime();
		Member placementMember = MemberDao.findByUsername(placementUser);
		Member sponsorMember = MemberDao.findByUsername(parentMember.username);
		MemberType memberType = MemberDao
				.findMemberTypeByCode(MemberTypeEnum.CLUB_MEMBER.getCode());

		// Register Member
		Member member = new Member();
		member.username = givenUserName;
		member.account = sponsorMember.account;
		member.memberType = memberType;
		member.sponsor = sponsorMember;
		member.placement = placementMember;
		member.registrationDate = new GregorianCalendar().getTime();
		member.rank = StaticContentUtil.DEFAULT_RANK;
		member.approvedDate = today;
		member.active = true;
		MemberDao.persist(member);
		Logger.info(CLASSNAME + " addAccount register member");

		Member newMemberApproved = MemberDao.findByUsername(givenUserName);
		// create initial cash rewards (zero amount)
		RewardType cashRewardType = RewardDao
				.findRewardTypeByCode(RewardTypeEnum.CASH.getCode());
		Reward reward1 = new Reward(member, cashRewardType, new BigDecimal(0L),
				today);
		RewardDao.persist(reward1);
		Logger.info(CLASSNAME + " addAccount initial cash reward");

		// create initial E-credit(zero amount)
		RewardType eCreditRewardType = RewardDao
				.findRewardTypeByCode(RewardTypeEnum.E_CREDIT.getCode());
		Reward reward2 = new Reward(member, eCreditRewardType, new BigDecimal(
				0L), today);
		RewardDao.persist(reward2);
		Logger.info(CLASSNAME + " addAccount initial e credit");

		// create initial Wealth Points (zero amount)
		RewardType wealthRewardType = RewardDao
				.findRewardTypeByCode(RewardTypeEnum.WEALTH_POINT.getCode());
		Reward reward3 = new Reward(member, wealthRewardType,
				new BigDecimal(0L), today);
		RewardDao.persist(reward3);
		Logger.info(CLASSNAME + " addAccount initial wealth points");

		// create initial Sponsor Points (zero amount)
		RewardType sponsorRewardType = RewardDao
				.findRewardTypeByCode(RewardTypeEnum.SPONSOR.getCode());
		Reward reward4 = new Reward(member, sponsorRewardType, new BigDecimal(
				0L), today);
		RewardDao.persist(reward4);
		Logger.info(CLASSNAME + " addAccount END");
		return newMemberApproved;
	}

	/**
	 * Change password of member.
	 * @param memberDto MemberDto
	 * @return member object
	 * @throws MssException
	 */
	public static Member changePassword(MemberDto memberDto)  throws MssException {
	    Logger.info(CLASSNAME + " changePassword START");

	    Member member = MemberDao.findByUsername(memberDto.username);

	    Account account = AccountDao.findById(member.account.id);
	    account.password = BCrypt.hashpw(
                memberDto.password, BCrypt.gensalt(12));

        AccountDao.persist(account);
        Logger.info(CLASSNAME + " changePassword END");

        return member;
    }

	/**
	 * Searches for a member using username.
	 * @param username member's user name
	 * @return member object
	 */
	public static Member findMember(String username) {
	    Logger.info(CLASSNAME + " findMember");
	    return MemberDao.findByUsername(username);

	}

	/**
	 * Search member type by code.
	 * @param code member type code
	 * @return MemberType object
	 */
	public static MemberType findMemberTypeByCode(String code) {
	    Logger.info(CLASSNAME + " findMemberTypeByCode");
	    return MemberDao.findMemberTypeByCode(code);
	}

	/**
	 * Searches for active members.
	 * @return list of members
	 */
	public static SearchResult<MemberDto> searchMembers(PaginationControl params) {
	    Logger.info(CLASSNAME + " searchMembers START");
		MemberType type = MemberService.findMemberTypeByCode("admin");

		SearchResult<Member> result = MemberDao.searchMembersNotAdmin(params, type.id);
		List<MemberDto> list = new ArrayList<MemberDto>();
		for (Member m : result.getList()) {
			MemberDto member = new MemberDto(
                    m.username,
                    m.account.person.firstName + " " +
                    m.account.person.lastName,
                    m.memberType.description,
                    m.active);
			list.add(member);
		}
		Logger.info(CLASSNAME + " searchMembers END");
		return new SearchResult<MemberDto>(list, result.getTotalRowCount(), result.getPageIndex(), result.getPageSize());
	}
	/**
	 * Searches for active members.
	 * @return list of members
	 */
	public static SearchResult<MemberDto> searchActiveMembers(PaginationControl params, DateTime fromDate, DateTime toDate) {
	    Logger.info(CLASSNAME + " searchActiveMembers START");
	    MemberType type = MemberService.findMemberTypeByCode("admin");

		SearchResult<Member> result = MemberDao.searchActiveMembersNotAdmin(params, fromDate, toDate, type.id);
		List<MemberDto> list = new ArrayList<MemberDto>();
		for (Member m : result.getList()) {
			MemberDto member = new MemberDto();
			member.username = m.username;
			member.fullName = m.account.person.firstName + " " + m.account.person.lastName;
			try {
				if (m.sponsor != null) {
					member.sponsorFullName = m.sponsor.account.person.firstName + " " + m.account.person.lastName;
				}
			} catch (NullPointerException e) {
			    Logger.error(CLASSNAME + " searchActiveMembers NullPointerException");
			}

			list.add(member);
		}
		Logger.info(CLASSNAME + " searchActiveMembers END");
		return new SearchResult<MemberDto>(list, result.getTotalRowCount(), result.getPageIndex(), result.getPageSize());
	}

    /**
     * Update member.
     * @param member Member
     * @throws MssException
     */
	public static void updateMember(Member member) throws MssException {
	    Logger.info(CLASSNAME + " updateMember");
	    MemberDao.merge(member);
	}

	/**
	 * Searches for members that are in the same board with the given parent account id and board number
	 * @param id parent account id
	 * @param board board number
	 * @return list of board details
	 */
	public static List<BoardDto> searchBoard(long id, long board) {
	    Logger.info(CLASSNAME + " searchBoard START");
		List<BoardDto> result = new ArrayList<BoardDto>();

		BoardDto dto;
		int size = 0;
		int origSize = 0;
		String empty = "[EMPTY]";
		String company = "[COMPANY]";
		boolean hasNextBoard;

		//get tree for the specified parent
		List<BoardDto> tempList = BoardAccountDao.searchByParentAndBoard(id, board);
	    hasNextBoard = BoardAccountDao.isNextBoardExists(board);
	    size = tempList.size();
	    origSize = size;
		//add company slots
		if (!hasNextBoard) {
			if (board > 1 && size < 9) {
				for (int i = 0; i < 7; i++) {
					dto = new BoardDto();
					dto.id = 0l;
					dto.username = company;
					dto.name = "";
					dto.email = "";
					dto.boardNumber = board;
					result.add(dto);
					size++;
				}
			} else if (board > 1 && size > 8) {
				for (int i = 0; i < 15 - origSize; i++) {
					dto = new BoardDto();
					dto.id = 0l;
					dto.username = company;
					dto.name = "";
					dto.email = "";
					dto.boardNumber = board;
					result.add(dto);
					size++;
				}
			}
		}

		//add existing slots
		ListIterator<BoardDto> iterator = tempList.listIterator();
		while (iterator.hasNext()) {
			result.add(iterator.next());
		}

		//add empty slots

		for (int i = size++; i < 15; i++) {
			dto = new BoardDto();
			dto.id = 0l;
			dto.username = empty;
			dto.name = "";
			dto.email = "";
			dto.boardNumber = board;
			result.add(dto);
		}
		Logger.info(CLASSNAME + " searchBoard END");
		return result;

	}

	/**
	 * Get parent board accounts for the given board number
	 * @param board board number
	 * @return list of parent board accounts
	 */
	public static List<BoardDto> getParentBoardAccount(long board) {
	    Logger.info(CLASSNAME + " getParentBoardAccount START");
		List<BoardDto> result = new ArrayList<BoardDto>();
		BoardAccount parent;
		BoardDto parentDto;

		List<BoardAccount> parentList = BoardAccountDao.getParentBoardAccount(board);
		ListIterator<BoardAccount> parentIterator = parentList.listIterator();
		while (parentIterator.hasNext()) {

			parent = parentIterator.next();
			parentDto = new BoardDto();
			parentDto.id = parent.id;
			result.add(parentDto);
		}
		Logger.info(CLASSNAME + " getParentBoardAccount END");
		return result;
	}

	/**
	 * Retrieve board details for the given board
	 * @param board board number
	 * @return list of board details
	 */
	public static List<List<BoardDto>> searchParentBoard(long board) {
	    Logger.info(CLASSNAME + " searchParentBoard START");

		List<List<BoardDto>> result = new ArrayList<List<BoardDto>>();
		List<BoardDto> parentList = MemberService.getParentBoardAccount(board);
		ListIterator<BoardDto> iterator = parentList.listIterator();
		List<BoardDto> childList;
		BoardDto parent;
		boolean hasData = false;

		while (iterator.hasNext()) {
			hasData = true;
			parent = iterator.next();
			childList = MemberService.searchBoard(parent.id, board);
			result.add(childList);
		}
		if (!hasData) {
			childList = MemberService.searchBoard(0, board);
			result.add(childList);
		}

		Logger.info(CLASSNAME + " searchParentBoard END");

		return result;
	}

	/**
	 * Recalculate board number of the accounts for the given board number
	 * @param board board number
	 * @throws MssException update db exception
	 */
	public static void recalculateBoard(long board) throws MssException {
	    Logger.info(CLASSNAME + " recalculateBoard START");

		List<BoardAccount> list = BoardAccountDao.searchByBoardNumber(board);
		boolean hasNextBoard = BoardAccountDao.isNextBoardExists(board);
		BoardSlot slot = BoardSlotDao.findBySlot(board);
		if (slot == null) {
			slot = new BoardSlot();
			slot.slot = board;
			slot.counter = 0;
		}
		int size = list.size();
		int initialLimit = 14;
		int splitLimit = 7;
		int remIndex = 0;

		if (hasNextBoard) {
			if (slot.counter > splitLimit) {
				remIndex = size % splitLimit;
			}
		} else {
			if (slot.counter > initialLimit) {
				remIndex = size - initialLimit;
			}
		}

		BoardAccount boardAccount;
		BoardSlot boardSlot;
		for (int index = 0; index < remIndex; index++) {
			boardAccount = list.get(index);
			boardAccount.boardNumber++;
			BoardAccountDao.merge(boardAccount);

			//reset counter
			slot.counter = 0;
			BoardSlotDao.merge(slot);

			//increment next slot counter
			boardSlot = BoardSlotDao.findBySlot(boardAccount.boardNumber);
			boardSlot.counter++;
			BoardSlotDao.merge(boardSlot);
		}
		Logger.info(CLASSNAME + " recalculateBoard END");
	}
}
