package service;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import play.Logger;
import util.StaticContentUtil;
import models.dto.ReportDto;


public class ReportService {

    private static final String CLASSNAME = "ReportService";

	/**
	 * Generate Downloadable Report List.
	 * @param rewardList
	 */
	public static List<ReportDto> searchReports() {
	    Logger.info(CLASSNAME + " searchReports START");

		File folder = new File(System.getProperty("user.home") + StaticContentUtil.HOME_DIR);
		File [] files = folder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".xlsx");
			}
		});
		List<ReportDto> reportList = new ArrayList<ReportDto>();

	    for (int i = 0; i < files.length; i++) {
	    	if (files[i].isFile()) {
	    		try {
		    		ReportDto report = new ReportDto(files[i].getName(), files[i].getCanonicalPath());
		    		reportList.add(report);
	    		} catch (IOException e) {
	    			Logger.error(CLASSNAME + " searchReports IOException " + e.getMessage());
	    		}

	    	}
	    }
	    Logger.info(CLASSNAME + " searchReports END");
	    return reportList;
	}
}
