package service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import play.Logger;
import models.Account;
import models.Member;
import models.MemberType;
import models.MemberTypeEnum;
import models.Person;
import models.Reward;
import models.RewardType;
import models.RewardTypeEnum;
import models.SearchResult;
import models.dto.FacilitatorDto;
import models.dto.FacilitatorProfileDto;
import security.BCrypt;
import util.PaginationControl;
import util.StaticContentUtil;
import dao.AccountDao;
import dao.MemberDao;
import dao.PersonDao;
import dao.RewardDao;
import exception.MssException;

public class FacilitatorService {

    private static final String CLASSNAME = "FacilitatorService";

    /**
     * Add a new facilitator
     * @param fpdDto
     * @return
     */
    public static Member addFacilitator(FacilitatorProfileDto fpdDto) {
        Logger.info(CLASSNAME + " addFacilitator START");

        Date today = new GregorianCalendar().getTime();
        Person person = new Person(fpdDto.firstName, fpdDto.middleName,
                fpdDto.lastName);
        person.gender = fpdDto.gender;
        person.mobileNumber = fpdDto.mobileNumber;
        PersonDao.persist(person);

        Account account = new Account(fpdDto.email, BCrypt.hashpw(
                fpdDto.userName, BCrypt.gensalt(12)), person);
        AccountDao.persist(account);

        MemberType memberType = MemberDao
                .findMemberTypeByCode(MemberTypeEnum.FACILITATOR.getCode());
        Member member = new Member();
        member.username = fpdDto.userName;
        member.account = account;
        member.registrationDate = today;
        member.approvedDate = today;
        member.rank = "facilitator";
        member.memberType = memberType;
        MemberDao.persist(member);

        RewardType rewardType = RewardDao.findRewardTypeByCode(RewardTypeEnum.E_CREDIT.getCode());
        Reward reward = new Reward(member, rewardType, new BigDecimal(0), today);
        RewardDao.persist(reward);

        Logger.info(CLASSNAME + " addFacilitator END");
        return member;
    }

    /**
     * Search for all facilitators
     * @param params
     * @return list of Facilitators
     */
    public static SearchResult<FacilitatorDto> searchFacilitators(PaginationControl params) {
        Logger.info(CLASSNAME + " searchFacilitators START");

        SearchResult<Member> result = null;
        List<FacilitatorDto> list = new ArrayList<FacilitatorDto>();
        try {
            MemberType type = FacilitatorService.findMemberTypeByCode("facilitator");

            result = MemberDao.searchFacilitators(params, type.id);
            list = new ArrayList<FacilitatorDto>();
            for (Member m : result.getList()) {
                FacilitatorDto faciDto = new FacilitatorDto(m.username, m.account.person.firstName + " "
                        + m.account.person.lastName);

                Reward reward = RewardDao.findRewardAmount(2, m.id);
                faciDto.amount = reward.amount;
                list.add(faciDto);
            }
        } catch (Exception e) {
            Logger.error(CLASSNAME + " searchFacilitators Exception " + e.getMessage());
            e.printStackTrace();
        }

        Logger.info(CLASSNAME + " searchFacilitators END");
        return new SearchResult<FacilitatorDto>(list, result.getTotalRowCount(), result.getPageIndex(),
                result.getPageSize());
    }

    /**
     * Search member type by code.
     * @param code member type code
     * @return MemberType object
     */
    public static MemberType findMemberTypeByCode(String code) {
        Logger.info(CLASSNAME + " findMemberTypeByCode " + code);
        return MemberDao.findMemberTypeByCode(code);
    }

    /**
     * Add ECredit to existing Facilitators
     * @param userName
     * @param addedECredits
     * @return
     * @throws MssException
     */
    public static String addECredits(String userName, String addedECredits) throws MssException {
        Logger.info(CLASSNAME + " addECredits START");
        Member recipient = MemberDao.findByUsername(userName);
        String newECredit = "0";
        if (recipient != null) {

            try {
                int eCredit = Integer.parseInt(addedECredits);

                if (eCredit <= 0) {
                    Logger.error(CLASSNAME + " addECredits Please enter correct amount.");
                    throw new MssException("Please enter correct amount.");
                } else {
                    if (recipient.rewards != null) {
                        Reward reward = recipient.getEcreditReward();
                        BigDecimal amount = reward.amount.setScale(2, BigDecimal.ROUND_DOWN);
                        reward.amount = amount.add(new BigDecimal(eCredit));
                        RewardDao.persist(reward);
                        newECredit = StaticContentUtil.DEFAULT_DECIMAL_FORMAT.format(reward.amount);
                    }

                }
            } catch (NumberFormatException e) {
                Logger.error(CLASSNAME + " addECredits NumberFormatException " + e.getMessage());
                e.printStackTrace();
                throw new MssException("Please enter correct amount.");
            }
        } else {
            Logger.error(CLASSNAME + " addECredits Username does not exist");
            throw new MssException("Username does not exist.");
        }
        Logger.info(CLASSNAME + " addECredits addECredits END");
        return newECredit;
    }

}
