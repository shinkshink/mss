package controllers;

import static play.data.Form.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import models.Address;
import models.Country;
import models.Member;
import models.dto.MemberProfileDto;
import play.Logger;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.BCrypt;
import views.html.member.myProfile;

import com.fasterxml.jackson.databind.node.ObjectNode;

import dao.CountryDao;
import dao.MemberDao;

@Security.Authenticated(Secured.class)
public class MemberProfileController extends Controller {
    private static final String CLASSNAME = "MemberProfileController";

	/**
	 * Defines a form wrapping the MemberProfileDto class.
	 */
	final static Form<MemberProfileDto> profileForm = form(MemberProfileDto.class);

	/**
	 * Handle the form submission.
	 */
	@Transactional
	public static Result display() {
		Logger.info(CLASSNAME + " display START ");

		Member member = MemberDao.findByUsername(session("username"));
		MemberProfileDto dto = new MemberProfileDto();
		dto.id = member.id;
		dto.username = member.username;
		if(member.sponsor != null) {
			dto.sponsorName = member.sponsor.username;
		}
		dto.firstName = member.account.person.firstName;
		dto.middleName = member.account.person.middleName;
		dto.lastName = member.account.person.lastName;
		dto.gender = member.account.person.gender;
		dto.mobileNumber = member.account.person.mobileNumber;
		dto.email = member.account.email;
		Set<Address> addresses = member.account.person.addresses;
		if(addresses.size() > 0) {
			Address currentAddress = addresses.iterator().next();
			if(currentAddress.country != null) {
				dto.countryId = addresses.iterator().next().country.id;
			}
 			dto.street = currentAddress.street;
 			dto.state = currentAddress.state;
 			dto.cityProvince = currentAddress.cityProvince;
 			dto.zipCode = currentAddress.zipCode;
		}
		Logger.info(CLASSNAME + " display END ");
		return ok(myProfile.render(profileForm.fill(dto), member));
	}

	@Transactional
	public static Result submit() {
		Logger.info(CLASSNAME + " submit START ");

		Form<MemberProfileDto> filledForm = profileForm.bindFromRequest();
		Member member = MemberDao.findByUsername(session("username"));
		boolean passwordChanged = false;
		//check if password is changed
		if((!filledForm.field("password").valueOr("").isEmpty()) || (!filledForm.field("repeatedPassword").valueOr("").isEmpty())) {
			if (!filledForm.field("password").value().equals(filledForm.field("repeatPassword").value())) {
				filledForm.reject("repeatPassword", "Password don't match");
			}else {
				if(filledForm.field("oldPassword").valueOr("").isEmpty()) {
					filledForm.reject("oldPassword", "Empty password");
				}else {
					//check old password
					String enteredPassword = filledForm.field("oldPassword").value();
		        	boolean matched = BCrypt.checkpw(enteredPassword,
		        			member.account.password);
		        	if(!matched) {
		        		filledForm.reject("oldPassword", "Invalid password");
		        	}else {
		        		if(filledForm.field("password").value().length() < 6) {
		        			filledForm.reject("password", "Minimum length is 6");
		        		}else {
		        			passwordChanged = true;
		        		}
		        	}
				}
			}
		}

		boolean addressExists = false;

		if(!filledForm.field("street").valueOr("").isEmpty()) {
			addressExists = true;
		}

		if(!filledForm.field("state").valueOr("").isEmpty()) {
			addressExists = true;
		}

		if(!filledForm.field("cityProvince").valueOr("").isEmpty()) {
			addressExists = true;
		}

		if(!filledForm.field("zipCode").valueOr("").isEmpty()) {
			addressExists = true;
		}

		if(!filledForm.field("countryId").valueOr("").isEmpty()) {
			addressExists = true;
		}

		if (filledForm.hasErrors()) {
			return badRequest(myProfile.render(filledForm, member));
		} else {
			MemberProfileDto dto = filledForm.get();
			member.account.person.firstName = dto.firstName;
			member.account.person.middleName = dto.middleName;
			member.account.person.lastName = dto.lastName;
			member.account.person.gender = dto.gender;
			member.account.person.mobileNumber = dto.mobileNumber;
			member.account.email = dto.email;
			if(passwordChanged) {
				member.account.password = BCrypt.hashpw(dto.password,
						BCrypt.gensalt(12));
			}
			if(addressExists) {
				updateAddress(member, dto);
			}

			MemberDao.merge(member);
			Logger.info(CLASSNAME + " submit END");

			return redirect(routes.MemberProfileController.display());
		}
	}

	private static void updateAddress(Member member, MemberProfileDto dto) {
		Logger.info(CLASSNAME + " updateAddress START");

		Address address = null;
		Set<Address> addresses = member.account.person.addresses;
		if(addresses.size() > 0) {
			address = addresses.iterator().next();
		}else {
			address = new Address();
			address.person = member.account.person;
		}

		address.street = dto.street;
		address.state = dto.state;
		address.cityProvince = dto.cityProvince;
		address.zipCode = dto.zipCode;
		if(dto.countryId != null) {
			Country country = CountryDao.findById(dto.countryId);
			address.country = country;
		}
		if(addresses.size() == 0) {
			addresses.add(address);
		}

		JPA.em().persist(address);
		Logger.info(CLASSNAME + " updateAddress END");
	}

	@Transactional
	@BodyParser.Of(BodyParser.Json.class)
	public static Result getCountries() {
		Logger.info(CLASSNAME + " getCountries START");
		@SuppressWarnings("unchecked")
		List<Country> countriesResult = JPA.em().createQuery("from Country order by name").getResultList();
		List<Map<String,Object>> countries = new ArrayList<Map<String,Object>>();

		for (Country c : countriesResult) {
			Map<String,Object> countryMap = new HashMap<String, Object>();
			countryMap.put("id", c.id.toString());
			countryMap.put("name", c.properName);
			countries.add(countryMap);
		}
		ObjectNode result = Json.newObject();

		result.put("status", "OK");
		result.put("countries", Json.toJson(countries));
		Logger.info(CLASSNAME + " getCountries END");
		return ok(result);
	}
}
