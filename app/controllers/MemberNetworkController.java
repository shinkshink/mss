package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Member;
import models.dto.BoardDto;
import models.dto.MemberUnilevelDto;
import play.Logger;
import play.db.jpa.Transactional;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import service.MemberService;
import views.html.member.boardNetwork;
import views.html.member.unilevelNetwork;

import com.google.gson.Gson;

import dao.MemberDao;

@Security.Authenticated(Secured.class)
public class MemberNetworkController extends Controller {

    private static final String CLASSNAME = "MemberNetworkController";

	/**
	 * Unilevel Network
	 *
	 */
	@Transactional(readOnly = true)
	public static Result indexUnilevel(String userName) {
	    Logger.info(CLASSNAME + " indexUnilevel " + userName);
		Result result = null;

        //display downline regardless if part of the network or not
	    Member member = MemberDao.findByUsername(session("username"));
	    result = ok(unilevelNetwork.render(member));

//		Member logMember = MemberDao.findByUsername(session("username"));
//		Member logMember = MemberDao.findByUsername(userName);
//		List<Member> listMember = MemberDao.searchAssociates(logMember);

		//Check if member is part of network of the log member
//		if(listMember.contains(member) || logMember.username.equals(userName)){
//			result = ok(unilevelNetwork.render(member));
//		}else {
//			result = redirect(routes.LoginController.logout());
//        }

		return result;
	}

	@Transactional(readOnly = true)
	public static Result listAssociates(String userName, int level) {
		Logger.info(CLASSNAME + " listAssociates " + userName + "===" + level);
		response().setContentType("application/json");

		//recursive: call all associates on the given level
		Member parent = MemberDao.findByUsername(userName);
		List<Member> parentMemberList = new ArrayList<Member>();
		List<Member> childMemberList = new ArrayList<Member>();
		parentMemberList.add(parent);

        for (int i = 0; i < level; i++) {

            childMemberList = new ArrayList<Member>();

            for (Member temp : parentMemberList) {
                Logger.info("PARENT=================" + temp.username);
                List<Member> tempList = MemberDao.searchAssociates(temp);
                childMemberList.addAll(tempList);
            }
            parentMemberList.clear();
            parentMemberList.addAll(childMemberList);

		}

		List<MemberUnilevelDto> dtoList = new ArrayList<MemberUnilevelDto> ();
//		List<Member> listMember = MemberDao.searchAssociates(member);

		MemberUnilevelDto memberUnilevelDto;
		for(Member memberRaw : childMemberList) {

		    Logger.info("CHILD =================" + memberRaw.username);

			memberUnilevelDto = new MemberUnilevelDto(
					memberRaw.account.person.firstName + " "+ memberRaw.account.person.lastName,
					memberRaw.username, memberRaw.rank,
					memberRaw.registrationDate,
					new Long(memberRaw.associates.size()), memberRaw.active);
			dtoList.add(memberUnilevelDto);
		}

		Gson gson = new Gson();
		String json = gson.toJson(dtoList);
		return ok(json);
	}
	
	/**
	 *
	 * Board Scheme
	 */
	@Transactional(readOnly = true)
	public static Result indexBoard(String userName) {
	    Logger.info(CLASSNAME + " indexBoard START");
		Result result = null;

		Member logMember = MemberDao.findByUsername(session("username"));
		List<Member> listMember = MemberDao.searchAssociates(logMember);
		Member member = MemberDao.findByUsername(userName);

		//Check if member is part of network of the log member
		if(listMember.contains(member) || logMember.username.equals(userName)){
			result = ok(boardNetwork.render(member));
		}else {
			result = redirect(routes.LoginController.logout());
        }
		Logger.info(CLASSNAME + " indexBoard END");
		return result;
	}

	/**
	 * Display the boards for the given board number
	 * @param board board number
	 * @return list of board details
	 */
	@Transactional(readOnly = true)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result displayParentBoard(long board) {
		Logger.info(CLASSNAME + " displayParentBoard");
		response().setContentType("application/json");
		List<List<BoardDto>> dtoList = MemberService.searchParentBoard(board);
		Gson gson = new Gson();
		String json = gson.toJson(dtoList);
		return ok(json);
	}

	/**
	 * Retrieve the parent/root accounts for the given board
	 * @param board board number
	 * @return list of board details of the parent/root accounts
	 */
	@Transactional(readOnly = true)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result getParentBoardAccount(long board) {
		Logger.info(CLASSNAME + " getParentBoardAccount");
		response().setContentType("application/json");
		List<BoardDto> dtoList = MemberService.getParentBoardAccount(board);
		Gson gson = new Gson();
		String json = gson.toJson(dtoList);
		return ok(json);
	}
}