package controllers;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import models.Member;
import models.MemberTypeEnum;
import models.Reward;
import models.RewardTypeEnum;
import models.SearchResult;
import models.dto.SalesDto;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import service.MerchantService;
import util.PaginationControl;
import util.StaticContentUtil;
import views.html.member.merchantDash;
import views.html.member.merchantSalesHistory;

import com.fasterxml.jackson.databind.node.ObjectNode;

import dao.MemberDao;
import exception.MssException;

@Security.Authenticated(Secured.class)
public class MerchantController extends Controller {
    private static final String CLASSNAME = "MerchantController";

	private static final SimpleDateFormat DATE_FORMAT= new SimpleDateFormat("MMMM d, yyyy");

	@Transactional(readOnly=true)
	public static Result dashboard() {
		Member currentUser = MemberDao.findByUsername(session("username"));
		String ecredit = "0";
		if (currentUser.rewards != null) {
			for (Reward reward : currentUser.rewards) {
				BigDecimal value = reward.amount.setScale(2,
						BigDecimal.ROUND_DOWN);
				if (reward.rewardType.code.equals(RewardTypeEnum.E_CREDIT
						.getCode())) {
					ecredit = StaticContentUtil.DEFAULT_DECIMAL_FORMAT.format(value);
				}

			}
		}

		return ok(merchantDash.render(currentUser, ecredit));
	}

	@Transactional
	public static Result submitPurchase(String recipientName, String amount) {
		Logger.info(CLASSNAME + " submitPurchase START " + recipientName);

		boolean hasError = false;
		String errorMsg1 = "";
		String errorMsg2 = "";
		String ecredit = "0";
		if(recipientName.isEmpty() || recipientName == null ) {
			hasError = true;
			errorMsg1 = "Please enter member name.";
		}
		if(amount.isEmpty() || amount == null ) {
			hasError = true;
			errorMsg2 = "Please enter correct amount.";
		}

		Member recipient = MemberDao.findByUsername(recipientName);
		if(recipient != null && !hasError && (recipient.memberType.code.equals(MemberTypeEnum.CLUB_MEMBER.getCode()))) {
			Member member = MemberDao.findByUsername(session("username"));
			try {
				int d = Integer.parseInt(amount);
				if(d <= 0) {
					hasError = true;
					errorMsg2 = "Please enter correct amount.";
				}else {
					try {
						MerchantService.purchase(member.id, recipient.id, new BigDecimal(d));

						//Get new updated member
						Member updatedMerchantMember = MemberDao.findByUsername(session("username"));
						if (updatedMerchantMember.rewards != null) {
							for (Reward reward : updatedMerchantMember.rewards) {
								BigDecimal value = reward.amount.setScale(2,
										BigDecimal.ROUND_DOWN);
								if (reward.rewardType.code.equals(RewardTypeEnum.E_CREDIT
										.getCode())) {
									ecredit = StaticContentUtil.DEFAULT_DECIMAL_FORMAT.format(value);
								}

							}
						}

					} catch (MssException e) {
						Logger.error(CLASSNAME + " submitPurchase MssException " + errorMsg2);
						errorMsg2 = e.getMessage().toString();
						hasError = true;
						e.printStackTrace();

					}
				}
			} catch (NumberFormatException nfe) {
			    Logger.error(CLASSNAME + " submitPurchase NumberFormatException " + nfe.getMessage());
				hasError = true;
				errorMsg2 = "Please enter correct amount.";
				nfe.printStackTrace();
			}
		}else  {
			hasError = true;
			errorMsg1 = "Recipient does not exist, please enter valid recipient username.";
		}
		ObjectNode result = Json.newObject();

		result.put("ecredit", ecredit);
		result.put("status", hasError ? "ERROR" : "OK");
		result.put("error1", errorMsg1);
		result.put("error2", errorMsg2);

		if (hasError) {
			Logger.error(CLASSNAME + " submitPurchase " + errorMsg1);
			Logger.error(CLASSNAME + " submitPurchase " + errorMsg2);
		}
		Logger.info(CLASSNAME + " submitPurchase END");
		return ok(result);
	}

	@Transactional(readOnly=true)
	public static Result salesHistory() {
		Member currentUser = MemberDao.findByUsername(session("username"));
		return ok(merchantSalesHistory.render(currentUser));
	}

	@Transactional(readOnly=true)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result viewSales() {
		Logger.info(CLASSNAME + " viewSales START");

		response().setContentType("application/json");
		Member currentUser = MemberDao.findByUsername(session("username"));

		String[] cols = { "username", "fullName", "amount", "date"};

		int amount = 0;
	    int start = 0;
	    int echo = 0;
	    int col = 0;

	    String username = "";
	    String fullname = "";
	    String priceAmount = "";
	    String date = "";

	    String dir = "asc";
	    String sStart = request().getQueryString("iDisplayStart");
	    String sAmount = request().getQueryString("iDisplayLength");
	    String sEcho = request().getQueryString("sEcho");
	    String sCol = request().getQueryString("iSortCol_0");
	    String sdir = request().getQueryString("sSortDir_0");
	    String fromDateString = request().getQueryString("fromDate");
	    String toDateString = request().getQueryString("toDate");

	    username = request().getQueryString("sSearch_0");

	    if (sStart != null) {
	        start = Integer.parseInt(sStart);
	        if (start < 0)
	            start = 0;
	    }
	    if (sAmount != null) {
	        amount = Integer.parseInt(sAmount);
	        if (amount < 5 || amount > 30)
	            amount = 5;
	    }
	    if (sEcho != null) {
	        echo = Integer.parseInt(sEcho);
	    }
	    if (sCol != null) {
	        col = Integer.parseInt(sCol);
	        if (col < 0 || col > 3)
	            col = 0;
	    }
	    if (sdir != null) {
	        if (!sdir.equals("asc"))
	            dir = "desc";
	    }
	    String colName = cols[col];
	    //create pagination input
	    PaginationControl params = new PaginationControl(start, amount, colName, dir, username);

	    DateTime fromDate = null;

	    DateTime toDate = null;
	    try {
	    	if(!StringUtils.isEmpty(fromDateString) || fromDateString != null) {
	 	    	fromDate = new DateTime(DATE_FORMAT.parse(fromDateString).getTime());
	 	    }
	    	if(!StringUtils.isEmpty(toDateString) || toDateString != null) {
				toDate = new DateTime(DATE_FORMAT.parse(toDateString).getTime());
		    }
	    } catch (ParseException e) {
			//do nothing
	        Logger.error(CLASSNAME + " salesHistory ParseException " + e.getMessage());
		}
	    //search
	    SearchResult<SalesDto> sales = MerchantService.searchPurchases(params, currentUser.id, fromDate, toDate);
	    //convert to json
	    ObjectNode result = Json.newObject();
		result.put("iTotalRecords", sales.getTotalRowCount());
		result.put("iTotalDisplayRecords", sales.getTotalRowCount());
		result.put("aaData", Json.toJson(sales.getList()));
		result.put("sEcho", echo);
		Logger.info(CLASSNAME + " viewSales END");
		return ok(result);
	}

}