package controllers;

import java.math.BigDecimal;
import java.util.List;

import models.Member;
import models.Reward;
import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import service.RewardService;
import views.html.member.encashmentReward;

import com.fasterxml.jackson.databind.node.ObjectNode;

import dao.MemberDao;
import dao.RewardDao;
import exception.MssException;

@Security.Authenticated(Secured.class)
public class RewardController extends Controller {

    private static final String CLASSNAME = "RewardController";
	@Transactional(readOnly = true)
	public static Result indexEncashment() {
		Result result = null;

		Member member = MemberDao.findByUsername(session("username"));

		BigDecimal cashReward = member.getCashReward().amount;

		return ok(encashmentReward.render(member, new scala.math.BigDecimal(cashReward)));
	}


	@Transactional
	public static Result submitCashEncashment(String amount) {
		Logger.info(CLASSNAME + " submitCashEncashment START");

		boolean hasError = false;
		String errorMsg1 = "";
		String errorMsg2 = "";

		if(amount.isEmpty() || amount == null ) {
			hasError = true;
			errorMsg2 = "Please enter correct amount.";
		}

		int integerPlaces = amount.indexOf(".");

		if(!(integerPlaces == -1)){
			int decimalPlaces = amount.length() - integerPlaces - 1;
			if(decimalPlaces > 2){
				hasError = true;
				errorMsg2 = "Two Decimal is allowed only.";
			}
		}

			Member member = MemberDao.findByUsername(session("username"));
			try {
				RewardService.processCashEncashment(member.id, new BigDecimal(amount));
			} catch (MssException e) {
			    Logger.error(CLASSNAME + " submitCashEncashment MssException " + e.getMessage());
				hasError = true;
				errorMsg2 = e.getMessage();
			}

		ObjectNode result = Json.newObject();

		result.put("status", hasError ? "ERROR" : "OK");
		result.put("error1", errorMsg1);
		result.put("error2", errorMsg2);
		if (hasError) {
			Logger.error(CLASSNAME + " submitCashEncashment " + errorMsg1);
			Logger.error(CLASSNAME + " submitCashEncashment " + errorMsg2);
		}
		Logger.info(CLASSNAME + " submitCashEncashment END");
		return ok(result);

	}

	/**
	 * Search rewards by reward type id.
	 * @param id reward type id
	 * @return List of Rewards
	 */
	public static List<Reward> searchRewardsByRewardType(int id) {
		return RewardDao.searchRewardsByRewardType(id);
	}
}
