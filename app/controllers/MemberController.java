package controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import models.Account;
import models.ClubMemberEnrollTypeEnum;
import models.Member;
import models.MemberTypeEnum;
import models.NetworkAffiliate;
import models.NetworkAffiliateTypeEnum;
import models.Reward;
import models.RewardTypeEnum;
import models.dto.MemberPendingDto;
import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import service.MemberService;
import util.MemberUtil;
import util.StaticContentUtil;
import views.html.member.addOwnAccount;
import views.html.member.dashboard;
import views.html.member.pendingAssociates;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;

import dao.AccountDao;
import dao.MemberDao;
import exception.MssException;
@Security.Authenticated(Secured.class)
public class MemberController extends Controller {

    private static final String CLASSNAME = "MemberController";
	/**
	 * Dashboard
	 *
	 */
	@Transactional(readOnly = true)
	public static Result index() {
		Logger.info(CLASSNAME + " index START");
		Member member = MemberDao.findByUsername(request().username());
		Account account = AccountDao.findById(member.account.id);
		BigDecimal cash =  new BigDecimal("0");
		BigDecimal ecredit = new BigDecimal("0");
		BigDecimal wealthpoints = new BigDecimal("0");
		BigDecimal sponsor = new BigDecimal("0");

		BigDecimal totalCash =  new BigDecimal("0");
		BigDecimal totalEcredit = new BigDecimal("0");
		BigDecimal totalWealthpoints = new BigDecimal("0");
		BigDecimal totalSponsor = new BigDecimal("0");

		BigDecimal ogCounter = new BigDecimal("0");
		BigDecimal vantageCounter = new BigDecimal("0");
		BigDecimal aimCounter = new BigDecimal("0");

		for(Member individualMember: member.account.members){
			if (individualMember.rewards != null) {
				for (Reward reward : individualMember.rewards) {
					BigDecimal value = reward.amount.setScale(5,
							BigDecimal.ROUND_DOWN);
					if (reward.rewardType.code
							.equals(RewardTypeEnum.CASH.getCode())) {
						cash = value;
					}
					if (reward.rewardType.code.equals(RewardTypeEnum.SPONSOR
							.getCode())) {
						sponsor = value;
					}
					if (reward.rewardType.code.equals(RewardTypeEnum.E_CREDIT
							.getCode())) {
						ecredit = value;
					}
					if (reward.rewardType.code.equals(RewardTypeEnum.WEALTH_POINT
							.getCode())) {
						wealthpoints = value;
					}
				}
			}
			totalCash = totalCash.add(cash);
			totalSponsor = totalSponsor.add(sponsor.setScale(2, BigDecimal.ROUND_DOWN));
			totalEcredit = totalEcredit.add(ecredit.setScale(2, BigDecimal.ROUND_DOWN));
			totalWealthpoints = totalWealthpoints.add(wealthpoints);
		}
		Set<Member> associates = member.associates;
		int pendingCount = 0;
		for (Member member2 : associates) {
			if(member2.approvedDate == null) {
				pendingCount++;
			}
		}

		for(NetworkAffiliate networkAffiliate :  account.networkAffiliates){
			BigDecimal counter = networkAffiliate.count;

			if(networkAffiliate.networkAffiliateType.code.equals(NetworkAffiliateTypeEnum.ORGANO_GOLD.getCode())){
				ogCounter =counter;
			}
			if(networkAffiliate.networkAffiliateType.code.equals(NetworkAffiliateTypeEnum.VANTAGE.getCode())){
				vantageCounter =counter;
			}
			if(networkAffiliate.networkAffiliateType.code.equals(NetworkAffiliateTypeEnum.AIM.getCode())){
				aimCounter =counter;
			}
		}

		List<Member> allMemberAccount = MemberDao.findMemberByAccount(member.account.id);
		Logger.info(CLASSNAME + " index END");
		return ok(dashboard.render(member, totalCash.toString(), totalSponsor.toString(), totalEcredit.toString(), pendingCount,
				allMemberAccount, allMemberAccount, totalWealthpoints.toString(), ogCounter.toString(), vantageCounter.toString(), aimCounter.toString()));
	}

	@Transactional(readOnly = true)
	public static Result indexPendingAssociates() {
		Member member = MemberDao.findByUsername(session("username"));
		return ok(pendingAssociates.render(member));
	}

	@Transactional(readOnly = true)
	public static Result listPendingAssociates() {
		Logger.info(CLASSNAME + " listPendingAssociates");
		response().setContentType("application/json");
		Member currentUser = MemberDao.findByUsername(session("username"));
		List<MemberPendingDto> dtoList = new ArrayList<MemberPendingDto> ();

		List<Member> listMember = MemberDao.searchPendingAssociates(currentUser);

		MemberPendingDto memberPendingDto;
		for(Member memberRaw : listMember){
			memberPendingDto = new MemberPendingDto(
					memberRaw.id ,
					memberRaw.account.person.firstName + " "+ memberRaw.account.person.lastName ,
					memberRaw.username,
					memberRaw.registrationDate,
					memberRaw.expiryDate);
			dtoList.add(memberPendingDto);
		}

		Gson gson = new Gson();
		String json = gson.toJson(dtoList);
		return ok(json);
	}

	@Transactional
	public static Result approveMember(Long newMemberId, String memberTypeCode, String clubMemberTypeCode, String networkAffiliateTypeCode) {
		Logger.info(CLASSNAME + " approveMember START");
		boolean hasError = false;
		String errorMsg1 = "";
		Member sponsorMember = MemberDao.findByUsername(session("username"));

		ClubMemberEnrollTypeEnum clubMemberType = (clubMemberTypeCode.equals("classic")) ? ClubMemberEnrollTypeEnum.CLASSIC : ClubMemberEnrollTypeEnum.PREMIUM;
		NetworkAffiliateTypeEnum networkAffiliateType = null;

        if(networkAffiliateTypeCode.equals("organo_gold")){
        		networkAffiliateType = NetworkAffiliateTypeEnum.ORGANO_GOLD;
        }else if(networkAffiliateTypeCode.equals("vantage")){
        	networkAffiliateType = NetworkAffiliateTypeEnum.VANTAGE;
        } else if(networkAffiliateTypeCode.equals("aim")){
        	networkAffiliateType = NetworkAffiliateTypeEnum.AIM;
        }

		if(!clubMemberTypeCode.equals("null") && memberTypeCode.equals("merchant")){
			Logger.error(CLASSNAME + " approveMember Data has tampered, invalid input!");
			return ok("error1", "Data has tampered, invalid input!");
		}

		try{
			Logger.info(CLASSNAME + " approveMember memberTypeCode=" + memberTypeCode);
			if (memberTypeCode.equals("club_member")){
				MemberService.approveMember(newMemberId, MemberTypeEnum.CLUB_MEMBER, sponsorMember, clubMemberType, networkAffiliateType);
			}
			if (memberTypeCode.equals("merchant")){
				MemberService.approveMember(newMemberId, MemberTypeEnum.MERCHANT, sponsorMember,  null, null);
			}

		} catch (MssException e) {
			hasError = true;
			errorMsg1 = " Insufficient E-Credits. " + e.getMessage();
		} catch (Exception e) {
			hasError = true;
			errorMsg1 = e.getMessage();
		}catch(Throwable e) {
			hasError = true;
			errorMsg1 = "ERROR "+ e.getMessage();
		}

		ObjectNode errorMessage = Json.newObject();
		errorMessage.put("status", hasError ? "ERROR" : "OK");
		errorMessage.put("error1", errorMsg1);
		if (hasError) {
			Logger.error(CLASSNAME + " approveMember " + errorMsg1);
		}
		Logger.info(CLASSNAME + " approveMember END");
		return ok(errorMessage);
	}

	@Transactional(readOnly = true)
	public static Result indexAddOwnAccount() {
		Member member = MemberDao.findByUsername(session("username"));
		String userName = MemberUtil.counterAddAccountUsername(member);
		return ok(addOwnAccount.render(member, userName));
	}

	@Transactional
	public static Result submitAddOwnAccount(String userName, String placementUsername){
		Logger.info(CLASSNAME + " submitAddOwnAccount START ");
		Member member = MemberDao.findByUsername(session("username"));
		boolean hasError = false;
		String errorMsg1 = "";

		//Additional account username name must not exist
		   if(MemberUtil.isMembemExist(userName)){
			   hasError = true;
			   errorMsg1 = "Username already exist!";
		   } else if(!MemberUtil.isMembemExist(placementUsername)){
			   hasError = true;
			   errorMsg1 = "Placement username does not exist!";
		   } else{
			   try{
					BigDecimal amountToDebit = new BigDecimal(StaticContentUtil.CLASSIC_ECREDITS_APPROVAL_AMOUNT.toString());
					MemberService.approveAddAccount(userName, placementUsername, member, amountToDebit);
				} catch (MssException e) {
					hasError = true;
					errorMsg1 = e.getMessage();
				}
		   }

		String nextUserName = MemberUtil.counterAddAccountUsername(member);
		ObjectNode errorMessage = Json.newObject();
		errorMessage.put("status", hasError ? "ERROR" : "OK");
		errorMessage.put("username", nextUserName);
		errorMessage.put("error1", errorMsg1);
		if (hasError) {
			Logger.error(CLASSNAME + " submitAddOwnAccount " + errorMsg1);
		}
		Logger.info(CLASSNAME + " submitAddOwnAccount END");
		return ok(errorMessage);
	}

	@Transactional
	public static Result displayAccountMemberRewards(String username) {
		Logger.info(CLASSNAME + " displayAccountMemberRewards START ");
		Member member = MemberDao.findByUsername(username);
		boolean hasError = false;
		String errorMsg1 = "";
		String cash = "0";
		String sponsor = "0";
		String ecredit = "0";
		String wealthpoints = "0";

		if (member.rewards != null) {
			for (Reward reward : member.rewards) {
				BigDecimal value = reward.amount.setScale(5,
						BigDecimal.ROUND_DOWN);
				if (reward.rewardType.code
						.equals(RewardTypeEnum.CASH.getCode())) {
					cash = StaticContentUtil.DEFAULT_DECIMAL_FORMAT.format(value);
				}
				if (reward.rewardType.code
						.equals(RewardTypeEnum.SPONSOR.getCode())) {
					sponsor = StaticContentUtil.TWO_DECIMAL_FORMAT.format(value);
				}
				if (reward.rewardType.code.equals(RewardTypeEnum.E_CREDIT
						.getCode())) {
					ecredit = StaticContentUtil.TWO_DECIMAL_FORMAT.format(value);
				}
				if (reward.rewardType.code.equals(RewardTypeEnum.WEALTH_POINT
						.getCode())) {
					wealthpoints = StaticContentUtil.DEFAULT_DECIMAL_FORMAT.format(value);
				}
			}
		}

		ObjectNode errorMessage = Json.newObject();
		errorMessage.put("status", hasError ? "ERROR" : "OK");
		errorMessage.put("usernameSelected", username);
		errorMessage.put("cash", cash);
		errorMessage.put("sponsor", sponsor);
		errorMessage.put("unilevel", ecredit);
		errorMessage.put("wealth", wealthpoints);
		errorMessage.put("error1", errorMsg1);
		if (hasError) {
			Logger.error(CLASSNAME + " displayAccountMemberRewards " + errorMsg1);
		}
		Logger.info(CLASSNAME + " displayAccountMemberRewards END");
		return ok(errorMessage);
	}

    @Transactional(readOnly = true)
    public static Result getMemberByUsername(String userName) {
        Logger.info(CLASSNAME + " getMemberByUsername");

        response().setContentType("application/json");
        Member member = MemberDao.findByUsername(userName);
        ObjectNode json = Json.newObject();
        json.put("name", member.account.person.firstName + " " + member.account.person.lastName);
        return ok(json);
    }
}