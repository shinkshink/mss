package controllers;

import static play.data.Form.form;
import models.Member;
import models.SearchResult;
import models.dto.FacilitatorDto;
import models.dto.FacilitatorProfileDto;
import models.dto.MemberDto;
import play.Logger;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import service.FacilitatorService;
import service.MemberService;
import util.MemberUtil;
import util.PaginationControl;

import com.fasterxml.jackson.databind.node.ObjectNode;

import dao.MemberDao;
import exception.MssException;

@Security.Authenticated(Secured.class)
public class AdminController extends Controller {

    private static final String CLASSNAME = "AdminController";

    /**
     * Defines a form wrapping the MemberProfileDto class.
     */
    final static Form<FacilitatorProfileDto> profileForm = form(FacilitatorProfileDto.class);

    public static class MemberForm {

        public String username;
    }
    /**
     * Dashboard.
     *
     */
    @Transactional(readOnly = true)
    public static Result index() {
        Member member = MemberService.findMember(request().username());
        return ok(views.html.administrator.adminDashboard.render(member));
    }

    /**
     * List members for Activate/Deactive Members screen.
     * @return Result
     */
    @Transactional(readOnly = true)
    public static Result indexListMembers() {
        Member member = MemberService.findMember(request().username());
        return ok(views.html.administrator.listMembers.render(member));

    }

    /**
     * Change Password screen.
     * @return Result
     */
    @Transactional(readOnly = true)
    public static Result indexChangePassword() {
        Member member = MemberService.findMember(request().username());
        return ok(views.html.administrator.password.render(member));
    }

	/**
	 *
	 * @return
	 */
	@Transactional(readOnly=true)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result viewMembers() {
		Logger.info(CLASSNAME + " viewMembers");
		response().setContentType("application/json");

		String[] cols = { "username", "fullName", "code", "active"};

		int amount = 0;
	    int start = 0;
	    int echo = 0;
	    int col = 0;

	    String name = "";
	    String dir = "asc";
	    String sStart = request().getQueryString("iDisplayStart");
	    String sAmount = request().getQueryString("iDisplayLength");
	    String sEcho = request().getQueryString("sEcho");
	    String sCol = request().getQueryString("iSortCol_0");
	    String sdir = request().getQueryString("sSortDir_0");

	    name = request().getQueryString("sSearch");

	    if (sStart != null) {
	        start = Integer.parseInt(sStart);
	        if (start < 0)
	            start = 0;
	    }
	    if (sAmount != null) {
	        amount = Integer.parseInt(sAmount);
//	        if (amount < 10 || amount > 10)
//	            amount = 10;
	    }
	    if (sEcho != null) {
	        echo = Integer.parseInt(sEcho);
	    }
	    if (sCol != null) {
	        col = Integer.parseInt(sCol);
	        if (col < 0 || col > 2)
	            col = 0;
	    }
	    if (sdir != null) {
	        if (!sdir.equals("asc"))
	            dir = "desc";
	    }
	    String colName = cols[col];
	    //create pagination input
	    PaginationControl params = new PaginationControl(start, amount, colName, dir, name);

	    //search
	    SearchResult<MemberDto> list = MemberService.searchMembers(params);
	    //convert to json
	    ObjectNode result = Json.newObject();
		result.put("iTotalRecords", list.getTotalRowCount());
		result.put("iTotalDisplayRecords", list.getTotalRowCount());
		result.put("aaData", Json.toJson(list.getList()));
		result.put("sEcho", echo);
		Logger.info(CLASSNAME + " viewMembers END ");
		return ok(result);
	}

    /**
     * Deactivate member by username.
     * @param username user name
     * @return Result
     */
    @Transactional
    public static Result deactivate(String username) {
        Logger.info(CLASSNAME + " deactivate");
        ObjectNode errorMessage = AdminController.activateMember(username, false);
        return ok(errorMessage);
    }

    /**
     * Activate member by username.
     * @param username user name
     * @return Result
     */
    @Transactional
    public static Result activate(String username) {
        Logger.info(CLASSNAME + " activate");
        ObjectNode errorMessage = AdminController.activateMember(username, true);
        return ok(errorMessage);

    }

    /**
     * Search for member user username.
     * @param username
     * @param status true if activate, else false
     * @return Member
     */
    private static ObjectNode activateMember(String username, boolean status) {
        Logger.info(CLASSNAME + " activateMember START status=" + status);
        Member member = MemberService.findMember(username);
        boolean hasError = false;
        String errorMsg1 = "";

        try {
            //username exists
            if (MemberUtil.isMembemExist(username)) {
                member.active = status;
                MemberService.updateMember(member);
            } else {
                hasError = true;
                errorMsg1 = "Username does not exist!";
                Logger.error(CLASSNAME + " activateMember - " + errorMsg1);
            }
        } catch (Exception e) {
            hasError = true;
            errorMsg1 = "Error occurred! Member was not activated/deactivated.";
            Logger.error(CLASSNAME + " activateMember - " + errorMsg1);
        }

        ObjectNode errorMessage = Json.newObject();
        errorMessage.put("status", hasError ? "ERROR" : "OK");
        errorMessage.put("error1", errorMsg1);
        Logger.info(CLASSNAME + " activateMember END");
        return errorMessage;
    }

    /**
     * Change password of a given user.
     * @param username user name
     * @param password new password
     * @param repeatPassword repeat new password
     * @return Result
     */
    @Transactional
    public static Result changePassword(String username, String password, String repeatPassword) {
    	Logger.info(CLASSNAME + " changePassword START");
        boolean hasError = false;
        String errorMsg = "";

        if(password.isEmpty()) {
            hasError = true;
            errorMsg = "Please enter password";
        }
        // Check repeated password
        if (!hasError) {
            if (!password.equals(repeatPassword)) {
                hasError = true;
                errorMsg = "Password don't match";
            }
        }

        if (!hasError) {
            @SuppressWarnings("unused")
            Member member = MemberDao.findByUsername(username);

            //username exists
            if (!MemberUtil.isMembemExist(username)) {
                hasError = true;
                errorMsg = "Username does not exist!";
            }
        }

        if (!hasError) {
            MemberDto memberDto = new MemberDto(username, password);
            try {
                MemberService.changePassword(memberDto);
            } catch (Exception e) {
                hasError = true;
                errorMsg = "Error occurred! Member\'s password was not changed.";
            }

        }

        ObjectNode result = Json.newObject();
        result.put("status", hasError ? "ERROR" : "OK");
        result.put("error", errorMsg);
        if (hasError) {
        	Logger.error(CLASSNAME + " changePassword - " + errorMsg);
        }

        Logger.info(CLASSNAME + " changePassword END");
        return ok(result);
    }


    /*
     * Activate/Deactivate Members screen.
     * @return Result
     */
    @Transactional(readOnly = true)
    public static Result indexActiveMembers() {
        Logger.info(CLASSNAME + " indexActiveMembers");
        Member member = MemberDao.findByUsername(request().username());
        return ok(views.html.administrator.activeMembers.render(member));

    }

    @Transactional(readOnly = true)
    public static Result indexAddFacilitator() {
        Logger.info(CLASSNAME + " indexAddFacilitator");
        Member member = MemberService.findMember(request().username());

        FacilitatorProfileDto dto = new FacilitatorProfileDto();
//        return null;

        return ok(views.html.administrator.addFacilitator.render(profileForm.fill(dto), member));
    }

    @Transactional
    public static Result addFacilitator() {
    	Logger.info(CLASSNAME + " addFacilitator START");
        Member member = MemberService.findMember(request().username());
        Form<FacilitatorProfileDto> filledForm = profileForm.bindFromRequest();


        if (filledForm.field("userName").valueOr("").isEmpty()) {
            filledForm.reject("userName", "Empty Username");
        }

        if (filledForm.field("firstName").valueOr("").isEmpty()) {
            filledForm.reject("firstName", "Empty First Name");
        }

        if (filledForm.field("middleName").valueOr("").isEmpty()) {
            filledForm.reject("middleName", "Empty Middle Name");
        }

        if (filledForm.field("middleName").valueOr("").isEmpty()) {
            filledForm.reject("middleName", "Empty Middle Name");
        }

        if (filledForm.field("lastName").valueOr("").isEmpty()) {
            filledForm.reject("lastName", "Empty Last Name");
        }

        if (filledForm.field("email").valueOr("").isEmpty()) {
            filledForm.reject("email", "Empty Email");
        }

        if (filledForm.hasErrors()) {
        	Logger.info(CLASSNAME + " addFacilitator badRequest END");
        	return badRequest(views.html.administrator.addFacilitator.render(filledForm, member));
        } else {
            FacilitatorProfileDto fpd = filledForm.get();
            member = FacilitatorService.addFacilitator(fpd);
            Logger.info(CLASSNAME + " addFacilitator success END");
            return redirect(routes.AdminController.indexAddFacilitator());
        }

    }

    /**
     * Change Password screen.
     * @return Result
     */
    @Transactional(readOnly = true)
    public static Result indexListFacilitators() {
        Logger.info(CLASSNAME + " indexListFacilitators");
        Member member = MemberService.findMember(request().username());
        return ok(views.html.administrator.listFacilitators.render(member));
    }

    /**
    *
    * @return
    */
   @Transactional(readOnly=true)
   @BodyParser.Of(BodyParser.Json.class)
   public static Result viewFacilitators() {
	   Logger.info(CLASSNAME + " viewFacilitators START");
       response().setContentType("application/json");

       String[] cols = { "username", "fullName", "eCredits", "action"};

       int amount = 0;
       int start = 0;
       int echo = 0;
       int col = 0;

       String name = "";
       String dir = "asc";
       String sStart = request().getQueryString("iDisplayStart");
       String sAmount = request().getQueryString("iDisplayLength");
       String sEcho = request().getQueryString("sEcho");
       String sCol = request().getQueryString("iSortCol_0");
       String sdir = request().getQueryString("sSortDir_0");

       name = request().getQueryString("sSearch");

       if (sStart != null) {
           start = Integer.parseInt(sStart);
           if (start < 0)
               start = 0;
       }
       if (sAmount != null) {
           amount = Integer.parseInt(sAmount);
       }
       if (sEcho != null) {
           echo = Integer.parseInt(sEcho);
       }
       if (sCol != null) {
           col = Integer.parseInt(sCol);
           if (col < 0 || col > 2)
               col = 0;
       }
       if (sdir != null) {
           if (!sdir.equals("asc"))
               dir = "desc";
       }
       String colName = cols[col];
       //create pagination input
       PaginationControl params = new PaginationControl(start, amount, colName, dir, name);

       //search
       SearchResult<FacilitatorDto> list = FacilitatorService.searchFacilitators(params);
       //convert to json
       ObjectNode result = Json.newObject();
       result.put("iTotalRecords", list.getTotalRowCount());
       result.put("iTotalDisplayRecords", list.getTotalRowCount());
       result.put("aaData", Json.toJson(list.getList()));
       result.put("sEcho", echo);

       Logger.info(CLASSNAME + " viewFacilitators END");
       return ok(result);
   }

   @Transactional
   public static Result addECredits(String username, String addecredit) {
	   Logger.info(CLASSNAME + " addECredits START");
       boolean hasError = false;
       String errorMsg = "";

       if(addecredit.isEmpty() || addecredit == null ) {
           hasError = true;
           errorMsg = "Please enter correct amount.";
       }

       try {
           FacilitatorService.addECredits(username, addecredit);
       } catch (MssException e) {
           errorMsg = e.getMessage();
           hasError = true;
       }

       ObjectNode result = Json.newObject();
       result.put("status", hasError ? "ERROR" : "OK");
       result.put("error", errorMsg);
       if (hasError) {
           Logger.error(CLASSNAME + " addECredits " + errorMsg);
       }
       Logger.info(CLASSNAME + " addECredits END");
       return ok(result);

   }
}
