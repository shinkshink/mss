package controllers;

import play.Routes;
import play.mvc.Controller;
import play.mvc.Result;

public class Application extends Controller {

    public static Result index() {
    	return redirect(routes.LoginController.login());
    }

    public static Result jsRoutes() {
        response().setContentType("text/javascript");
        return ok(Routes.javascriptRouter("appRoutes", //appRoutes will be the JS object available in our view,
        		routes.javascript.MemberController.approveMember(),
        		routes.javascript.MemberProfileController.getCountries(),
        		routes.javascript.MemberNetworkController.listAssociates(),
        		routes.javascript.MemberTransferController.submitTransferCredit(),
        		routes.javascript.SignUpController.submitAjax(),
        		routes.javascript.MerchantController.submitPurchase(),
        		routes.javascript.MerchantController.viewSales(),
        		routes.javascript.MemberController.submitAddOwnAccount(),
        		routes.javascript.MemberController.displayAccountMemberRewards(),
        		routes.javascript.MemberController.listPendingAssociates(),
        		routes.javascript.AdminController.viewMembers(),
        		routes.javascript.AdminController.deactivate(),
        		routes.javascript.AdminController.activate(),
        		routes.javascript.AdminController.changePassword(),
        		routes.javascript.ReportController.viewSalesReport(),
        		routes.javascript.ReportController.viewDownloadReport(),
        		routes.javascript.ReportController.viewActiveMembers(),
        		routes.javascript.ReportController.downloadReport(),
			    routes.javascript.AdminController.viewFacilitators(),
			    routes.javascript.RewardController.submitCashEncashment(),
        		routes.javascript.AdminController.addECredits(),
        		routes.javascript.MemberNetworkController.getParentBoardAccount(),
        		routes.javascript.MemberNetworkController.displayParentBoard(),
        		routes.javascript.MemberController.getMemberByUsername()));
    }

}
