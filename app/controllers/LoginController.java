package controllers;

import static play.data.Form.form;
import models.MemberTypeEnum;
import play.Logger;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import service.MemberService;
import views.html.admin;
import views.html.login;

public class LoginController extends Controller {

    private static final String CLASSNAME = "LoginController";
	// Authentication
	public static class Login {

		public String username;
		public String password;
		public boolean isAdmin;

		public String validate() {
			Logger.info(CLASSNAME + " validate START");
			if (MemberService.authenticate(username, password, isAdmin) == null) {
				Logger.error(CLASSNAME + " validate Invalid user or password");
				return "Invalid user or password";
			}
			Logger.info(CLASSNAME + " validate END");
			return null;
		}

	}

	/**
	 * Login page.
	 */
	public static Result login() {
		return ok(login.render(form(Login.class)));
	}

	/**
     * Admin Login page.
     */
    public static Result adminLogin() {
        return ok(admin.render(form(Login.class)));
    }

	/**
	 * Handle login form submission.
	 */
	@Transactional(readOnly = true)
	public static Result authenticate() {
	    return LoginController.chechAuthentication(false);
	}

	/**
	 * Validates login authentication.
	 * @param isAdmin true if from admin screen.
	 * @return Result
	 */
	private static Result chechAuthentication(boolean isAdmin) {
	   Logger.info(CLASSNAME + " chechAuthentication START");
       Result redirect = null;
       Form<Login> loginForm = form(Login.class).bindFromRequest();

       if (loginForm.hasErrors()) {
           if (isAdmin) {
               redirect = badRequest(admin.render(loginForm));
            } else {
                redirect = badRequest(login.render(loginForm));
            }

        } else {
            String username = loginForm.get().username;
            String userRole = MemberService.findMember(username).memberType.code;
            session("username", loginForm.get().username);
            session("usr_role", userRole);
            if (userRole.equals(MemberTypeEnum.MERCHANT.getCode())) {
                redirect = redirect(routes.MerchantController.dashboard());
            } else if (userRole.equals(MemberTypeEnum.ADMIN.getCode())) {
                redirect = redirect(routes.AdminController.index());
            } else {
                redirect = redirect(routes.MemberController.index());
            }
            Logger.info(CLASSNAME + " User role:" + userRole);
        }

        Logger.info(CLASSNAME + " chechAuthentication END");
        return redirect;
	}

	/**
	 * Handles admin login form submission.
	 *
	 * @return Result
	 */
	@Transactional(readOnly = true)
	public static Result adminAuthenticate() {
	    return LoginController.chechAuthentication(true);
	}

	/**
	 * Logout and clean the session.
	 */
	public static Result logout() {
		Logger.info(CLASSNAME + " logout");
		session().clear();
		flash("success", "You've been logged out");
		return redirect(routes.LoginController.login());
	}

	   /**
     * Logout and clean the session.
     */
    public static Result adminLogout() {
    	Logger.info(CLASSNAME + " adminLogout");
        session().clear();
        flash("success", "You've been logged out");
        return redirect(routes.LoginController.adminLogin());
    }
}
