package controllers;

import java.math.BigDecimal;

import models.Member;
import models.MemberTypeEnum;
import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import service.MemberService;
import views.html.member.transferECredits;

import com.fasterxml.jackson.databind.node.ObjectNode;

import dao.MemberDao;
import exception.MssException;

@Security.Authenticated(Secured.class)
public class MemberTransferController extends Controller {

    private static final String CLASSNAME = "MemberTransferController";

	@Transactional
	public static Result displayTransferCredit() {
		Member member = MemberDao.findByUsername(session("username"));
    	return ok(transferECredits.render(member));
    }

	@Transactional
	public static Result submitTransferCredit(String recipientName, String amount) {
		Logger.info(CLASSNAME + " submitTransferCredit START");

		boolean hasError = false;
		String errorMsg1 = "";
		String errorMsg2 = "";
		if(recipientName.isEmpty() || recipientName == null ) {
			hasError = true;
			errorMsg1 = "Please enter member name.";
		}
		if(amount.isEmpty() || amount == null ) {
			hasError = true;
			errorMsg2 = "Please enter correct amount.";
		}

		Member recipient = MemberDao.findByUsername(recipientName);

		if(recipient == null ) {
			hasError = true;
			errorMsg1 = "Please enter correct amount.";
		}

		int integerPlaces = amount.indexOf(".");

		if(!(integerPlaces == -1)){
			int decimalPlaces = amount.length() - integerPlaces - 1;
			if(decimalPlaces > 2){
				hasError = true;
				errorMsg2 = "Two Decimal is allowed only.";
			}
		}

		if(recipient != null && !hasError && (recipient.memberType.code.equals(MemberTypeEnum.CLUB_MEMBER.getCode()) ||
				recipient.memberType.code.equals(MemberTypeEnum.MERCHANT.getCode()))) {
			Member member = MemberDao.findByUsername(session("username"));
			try {
					MemberService.transferCredit(member.id, recipient.id, new BigDecimal(amount));
			} catch (MssException e) {
				hasError = true;
			 	errorMsg2 = "Insufficient E-Credits.";
			} catch (NumberFormatException nfe) {
				hasError = true;
				errorMsg2 = "Please enter correct amount.";
			}
		}

		ObjectNode result = Json.newObject();

		result.put("status", hasError ? "ERROR" : "OK");
		result.put("error1", errorMsg1);
		result.put("error2", errorMsg2);
		if (hasError) {
			Logger.error(CLASSNAME + " submitTransferCredit " + errorMsg1);
			Logger.error(CLASSNAME + " submitTransferCredit " + errorMsg2);
		}
		Logger.info(CLASSNAME + " submitTransferCredit END");
		return ok(result);
	}
}