package controllers;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import models.Member;
import models.SearchResult;
import models.dto.MemberDto;
import models.dto.ReportDto;
import models.dto.SalesEntityDto;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import service.MemberService;
import service.MerchantService;
import service.ReportService;
import util.PaginationControl;
import util.StaticContentUtil;

import com.fasterxml.jackson.databind.node.ObjectNode;

import dao.MemberDao;

@Security.Authenticated(Secured.class)
public class ReportController extends Controller {
    private static final String CLASSNAME = "ReportController";

	private static final SimpleDateFormat DATE_FORMAT= new SimpleDateFormat("MMMM d, yyyy");

	/**
	 *
	 * @return
	 */
	@Transactional(readOnly=true)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result viewActiveMembers() {
		Logger.info(CLASSNAME + " viewActiveMembers START");

		response().setContentType("application/json");

		String[] cols = { "fullName", "username"};

		int amount = 0;
	    int start = 0;
	    int echo = 0;
	    int col = 0;

	    String name = "";
	    String dir = "asc";
	    String sStart = request().getQueryString("iDisplayStart");
	    String sAmount = request().getQueryString("iDisplayLength");
	    String sEcho = request().getQueryString("sEcho");
	    String sCol = request().getQueryString("iSortCol_0");
	    String sdir = request().getQueryString("sSortDir_0");
	    String fromDateString = request().getQueryString("fromDate");
	    String toDateString = request().getQueryString("toDate");

	    name = request().getQueryString("sSearch");

	    if (sStart != null) {
	        start = Integer.parseInt(sStart);
	        if (start < 0)
	            start = 0;
	    }
	    if (sAmount != null) {
	        amount = Integer.parseInt(sAmount);
//	        if (amount < 10 || amount > 10)
//	            amount = 10;
	    }
	    if (sEcho != null) {
	        echo = Integer.parseInt(sEcho);
	    }
	    if (sCol != null) {
	        col = Integer.parseInt(sCol);
	        if (col < 0 || col > 1)
	            col = 0;
	    }
	    if (sdir != null) {
	        if (!sdir.equals("asc"))
	            dir = "desc";
	    }
	    String colName = cols[col];
	    //create pagination input
	    PaginationControl params = new PaginationControl(start, amount, colName, dir, name);

	    DateTime fromDate = null;

	    DateTime toDate = null;
	    try {
	    	if(!StringUtils.isEmpty(fromDateString) || fromDateString != null) {
	 	    	fromDate = new DateTime(DATE_FORMAT.parse(fromDateString).getTime());
	 	    }
	    	if(!StringUtils.isEmpty(toDateString) || toDateString != null) {
				toDate = new DateTime(DATE_FORMAT.parse(toDateString).getTime());
		    }
	    } catch (ParseException e) {
			//do nothing
	        Logger.error(CLASSNAME + " viewActiveMembers ParseException" + e.getMessage());
		}
	    Logger.info(CLASSNAME + " viewActiveMembers start date = " + fromDateString + "; end date =" + toDateString);

	    //search
	    SearchResult<MemberDto> list = MemberService.searchActiveMembers(params, fromDate, toDate);
	    //convert to json
	    ObjectNode result = Json.newObject();
		result.put("iTotalRecords", list.getTotalRowCount());
		result.put("iTotalDisplayRecords", list.getTotalRowCount());
		result.put("aaData", Json.toJson(list.getList()));
		result.put("sEcho", echo);
		Logger.info(CLASSNAME + " viewActiveMembers END");
		return ok(result);
	}

    /*
     * Sales Report per Merchant screen.
     * @return Result
     */
    @Transactional(readOnly = true)
    public static Result indexSalesReport() {
        Member member = MemberDao.findByUsername(request().username());
        return ok(views.html.administrator.salesReport.render(member));

    }

    @Transactional(readOnly = true)
    public static Result indexDownloadReport() {
        Member member = MemberDao.findByUsername(request().username());
        return ok(views.html.administrator.reports.render(member));

    }

    /**
     * Display sales report per merchant.
     * @return Result
     */
	@Transactional(readOnly=true)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result viewSalesReport() {
		Logger.info(CLASSNAME + " viewSalesReport START");

		response().setContentType("application/json");

		String[] cols = { "fullName", "username", "amount"};

		int amount = 0;
	    int start = 0;
	    int echo = 0;
	    int col = 0;

	    String name = "";
	    String dir = "asc";
	    String sStart = request().getQueryString("iDisplayStart");
	    String sAmount = request().getQueryString("iDisplayLength");
	    String sEcho = request().getQueryString("sEcho");
	    String sCol = request().getQueryString("iSortCol_0");
	    String sdir = request().getQueryString("sSortDir_0");
	    String fromDateString = request().getQueryString("fromDate");
	    String toDateString = request().getQueryString("toDate");

	    name = request().getQueryString("sSearch");

	    if (sStart != null) {
	        start = Integer.parseInt(sStart);
	        if (start < 0)
	            start = 0;
	    }
	    if (sAmount != null) {
	        amount = Integer.parseInt(sAmount);
//	        if (amount < 10 || amount > 10)
//	            amount = 10;
	    }
	    if (sEcho != null) {
	        echo = Integer.parseInt(sEcho);
	    }
	    if (sCol != null) {
	        col = Integer.parseInt(sCol);
	        if (col < 0 || col > 1)
	            col = 0;
	    }
	    if (sdir != null) {
	        if (!sdir.equals("asc"))
	            dir = "desc";
	    }
	    String colName = cols[col];
	    //create pagination input
	    PaginationControl params = new PaginationControl(start, amount, colName, dir, name);

	    DateTime fromDate = null;

	    DateTime toDate = null;
	    try {
	    	if(!StringUtils.isEmpty(fromDateString) || fromDateString != null) {
	 	    	fromDate = new DateTime(DATE_FORMAT.parse(fromDateString).getTime());
	 	    }
	    	if(!StringUtils.isEmpty(toDateString) || toDateString != null) {
				toDate = new DateTime(DATE_FORMAT.parse(toDateString).getTime());
		    }
	    } catch (ParseException e) {
			//do nothing
	        Logger.error(CLASSNAME + " viewSalesReport " + e.getMessage());
		}

	    //search
	    SearchResult<SalesEntityDto> list = MerchantService.searchSalesReport(params, fromDate, toDate);

	    //convert to json
	    ObjectNode result = Json.newObject();
		result.put("iTotalRecords", list.getTotalRowCount());
		result.put("iTotalDisplayRecords", list.getTotalRowCount());
		result.put("aaData", Json.toJson(list.getList()));
		result.put("sEcho", echo);
		Logger.info(CLASSNAME + " viewSalesReport END");
		return ok(result);
	}


	/**
	 * View list of downloadable reports.
	 * @return
	 */
	@Transactional(readOnly=true)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result viewDownloadReport() {
	    Logger.info(CLASSNAME + " viewDownloadReport START");
		response().setContentType("application/json");

//		String[] cols = { "name"};
//
//		int amount = 0;
//	    int start = 0;
	    int echo = 0;
//	    int col = 0;

//	    String name = "";
//	    String dir = "asc";
//	    String sStart = request().getQueryString("iDisplayStart");
//	    String sAmount = request().getQueryString("iDisplayLength");
	    String sEcho = request().getQueryString("sEcho");
//	    String sCol = request().getQueryString("iSortCol_0");
//	    String sdir = request().getQueryString("sSortDir_0");
//	    String fromDateString = request().getQueryString("fromDate");
//	    String toDateString = request().getQueryString("toDate");
//
//	    name = request().getQueryString("sSearch");
//
//	    if (sStart != null) {
//	        start = Integer.parseInt(sStart);
//	        if (start < 0)
//	            start = 0;
//	    }
//	    if (sAmount != null) {
//	        amount = Integer.parseInt(sAmount);
////	        if (amount < 10 || amount > 10)
////	            amount = 10;
//	    }
	    if (sEcho != null) {
	        echo = Integer.parseInt(sEcho);
	    }
//	    if (sCol != null) {
//	        col = Integer.parseInt(sCol);
//	        if (col < 0 || col > 1)
//	            col = 0;
//	    }
//	    if (sdir != null) {
//	        if (!sdir.equals("asc"))
//	            dir = "desc";
//	    }
//	    String colName = cols[col];
//	    //create pagination input
//	    PaginationControl params = new PaginationControl(start, amount, colName, dir, name);

	    //search
	    List<ReportDto> reportList = ReportService.searchReports();

	    //convert to json
	    ObjectNode result = Json.newObject();
		result.put("iTotalRecords", reportList.size());
		result.put("iTotalDisplayRecords", reportList.size());
		result.put("aaData", Json.toJson(reportList));
		result.put("sEcho", echo);
		Logger.info(CLASSNAME + " viewDownloadReport END");
		return ok(result);
	}

    /**
     * Download file.
     * @param name report name
     * @return Result
     */
    public static Result downloadReport(String name) {
    	Logger.info(CLASSNAME + " downloadReport START");
    	File report = null;
		try {
			response().setContentType("application/x-download");
	    	response().setHeader("Content-disposition","attachment; filename="+name);
	    	Logger.info(CLASSNAME + " Full Path: " + System.getProperty("user.home")
					+ StaticContentUtil.HOME_DIR + File.separator + name);
			report = new File(System.getProperty("user.home")
					+ StaticContentUtil.HOME_DIR + File.separator + name);
		} catch (Exception e) {
			Logger.error(CLASSNAME + " downloadReport " + e.getMessage());
			e.printStackTrace();
		}
		Logger.info(CLASSNAME + " downloadReport END");
        return ok(report);
    }
}