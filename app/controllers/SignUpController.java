package controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.imageio.ImageIO;

import models.dto.MemberDto;
import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import service.MemberService;
import util.StaticContentUtil;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;

import dao.MemberDao;

public class SignUpController extends Controller {

    private static final String CLASSNAME = "SignUpController";

	public static Result captcha() {
		Logger.info(CLASSNAME + " captcha START");
		DefaultKaptcha captchaPro = new DefaultKaptcha();
		captchaPro.setConfig(new Config(new Properties()));
		String text = captchaPro.createText();
		session(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY, text);
		BufferedImage img = captchaPro.createImage(text);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ImageIO.write(img, "jpg", baos);
			baos.flush();
		} catch (IOException e) {
			Logger.error(CLASSNAME + " captcha " + e.getMessage());
		}

		Logger.info(CLASSNAME + " captcha END");
		return ok(baos.toByteArray()).as("image/jpg");
	}

	@Transactional
	@BodyParser.Of(BodyParser.Json.class)
	public static Result submitAjax() {
		Logger.info(CLASSNAME + " submitAjax START");

		boolean hasError = false;
		String errorMsg = "";

		JsonNode json = request().body().asJson();
		String submittedUsername = json.findPath("username").textValue();
		String firstName = json.findPath("firstName").textValue();
		String middleName = json.findPath("middleName").textValue();
		String lastName = json.findPath("lastName").textValue();
		String email = json.findPath("email").textValue();
		String password = json.findPath("password").textValue();
		String repeatPassword = json.findPath("repeatPassword").textValue();
		String sponsor = json.findPath("sponsor").textValue();
		String kaptcha = json.findPath("kaptcha").textValue();

		if(submittedUsername.isEmpty()  ) {
			hasError = true;
			errorMsg = "Invalid username";
		}else if(submittedUsername.length() < 6){
			hasError = true;
			errorMsg = "Username must be atleast 6 character";
		} else if(submittedUsername.length() > 35){
			hasError = true;
			errorMsg = "Username must be maximum 35 character";
		} else if(submittedUsername.matches("^.+?\\d$")){
			hasError = true;
			errorMsg = "Username must not end with numbers";
		}

		if(firstName.isEmpty()) {
			hasError = true;
			errorMsg = "Please enter first name";
		}

		if(email.isEmpty()) {
			hasError = true;
			errorMsg = "Please enter email";
		}
		//TODO email format

		// Check if the username is valid
		if (!hasError) {
			if (submittedUsername.equals("admin")
					|| submittedUsername.equals("guest")) {
				hasError = true;
				errorMsg = "This username is already taken";
			} else {
				if (MemberDao.findByUsername(submittedUsername) != null) {
					hasError = true;
					errorMsg = "This username is already taken";
				}
			}
		}

		if(password.isEmpty()) {
			hasError = true;
			errorMsg = "Please enter password";
		}
		// Check repeated password
		if (!hasError) {
			if (!repeatPassword.equals(password)) {
				hasError = true;
				errorMsg = "Password don't match";
			}
		}

		// Check if the sponsor exists
		if (!hasError) {
			if (MemberDao.findByUsername(sponsor) == null) {
				hasError = true;
				errorMsg = "Sponsor does not exists";
			}
		}

		// Check captcha
		if (!hasError) {
			String kaptchaExpected = session(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
			if (kaptcha == null
					|| !kaptcha.equalsIgnoreCase(kaptchaExpected)) {
				hasError = true;
				errorMsg = "Invalid captcha";
			}
		}

		if (!hasError) {
			MemberDto created = new MemberDto(submittedUsername, firstName, middleName, lastName, email, repeatPassword, sponsor, StaticContentUtil.DEFAULT_RANK);
			MemberService.signUpMember(created);
			//clear the captcha
			session(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY, "");
		}

		ObjectNode result = Json.newObject();
		result.put("status", hasError ? "ERROR" : "OK");
		result.put("error", errorMsg);

		if (hasError) {
			Logger.error(CLASSNAME + " submitAjax " + errorMsg);
		}
		Logger.info(CLASSNAME + " submitAjax END");
		return ok(result);
	}
}