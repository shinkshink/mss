package models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "enrollment")
@SequenceGenerator(name = "enrollment_seq", sequenceName = "enrollment_seq")
public class Enrollment implements Serializable {
	
	private static final long serialVersionUID = 5689209799876563116L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "enrollment_seq")
	@Column(name = "enrollment_id")
	public Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sponsor_account_id", nullable = false)
	public Account sponsor;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "sponsee_account_id", nullable = false)
	public Account sponsee;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "club_member_enroll_type_id")
	public ClubMemberEnrollType clubMemberEnrollType;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "network_affiliate_type_id")
	public NetworkAffiliateType networkAffiliateType;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved_date")
	public Date approvedDate;
	
	public Enrollment(){
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((approvedDate == null) ? 0 : approvedDate.hashCode());
		result = prime * result
				+ ((clubMemberEnrollType == null) ? 0 : clubMemberEnrollType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((networkAffiliateType == null) ? 0 : networkAffiliateType
						.hashCode());
		result = prime * result + ((sponsor == null) ? 0 : sponsor.hashCode());
		result = prime * result
				+ ((sponsee == null) ? 0 : sponsee.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Enrollment other = (Enrollment) obj;
		if (approvedDate == null) {
			if (other.approvedDate != null)
				return false;
		} else if (!approvedDate.equals(other.approvedDate))
			return false;
		if (clubMemberEnrollType == null) {
			if (other.clubMemberEnrollType != null)
				return false;
		} else if (!clubMemberEnrollType.equals(other.clubMemberEnrollType))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (networkAffiliateType == null) {
			if (other.networkAffiliateType != null)
				return false;
		} else if (!networkAffiliateType.equals(other.networkAffiliateType))
			return false;
		if (sponsor == null) {
			if (other.sponsor != null)
				return false;
		} else if (!sponsor.equals(other.sponsor))
			return false;
		if (sponsee == null) {
			if (other.sponsee != null)
				return false;
		} else if (!sponsee.equals(other.sponsee))
			return false;
		return true;
	}
	
	

}
