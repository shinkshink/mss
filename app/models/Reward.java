package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "reward")
@SequenceGenerator(name = "reward_seq", sequenceName = "reward_seq")
public class Reward implements Serializable{
	private static final long serialVersionUID = -8309250713239179479L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reward_seq")
	@Column(name = "reward_id")
	public Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "member_id")
    public Member member;
	
	@ManyToOne(fetch = FetchType.EAGER , optional = false)
    @JoinColumn(name = "reward_type_id")
    public RewardType rewardType;
	
	@Column(name = "amount")
	public BigDecimal amount;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_update")
	public Date lastUpdate;

	public Reward() {
		// TODO Auto-generated constructor stub
	}
	
	public Reward(Member member, RewardType rewardType, BigDecimal amount,
			Date lastUpdate) {
		super();
		this.member = member;
		this.rewardType = rewardType;
		this.amount = amount;
		this.lastUpdate = lastUpdate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((lastUpdate == null) ? 0 : lastUpdate.hashCode());
		result = prime * result
				+ ((rewardType == null) ? 0 : rewardType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reward other = (Reward) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastUpdate == null) {
			if (other.lastUpdate != null)
				return false;
		} else if (!lastUpdate.equals(other.lastUpdate))
			return false;
		if (rewardType == null) {
			if (other.rewardType != null)
				return false;
		} else if (!rewardType.equals(other.rewardType))
			return false;
		return true;
	}
}
