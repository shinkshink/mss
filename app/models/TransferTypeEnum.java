package models;

public enum TransferTypeEnum {
	TRANSFER("transfer"),
	ENROLL("enroll"),
	PURCHASE("purchase");
	
	private String code;
	
	private TransferTypeEnum(String code) {
		this.code  = code;
	}
	
	public String getCode() {
		return code;
	}
}
