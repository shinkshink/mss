package models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "country")
@SequenceGenerator(name = "country_seq", sequenceName = "country_seq")
public class Country implements Serializable {
	private static final long serialVersionUID = -8136971316558043740L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "country_seq")
	@Column(name = "country_id")
	public Long id;
	
	@Column(name = "iso", nullable = false, length = 2)
	public String iso;
	
	@Column(name = "name", nullable = false, length = 80)
	public String name;
	
	@Column(name = "proper_name", nullable = false, length = 80)
	public String properName;
	
	@Column(name = "iso3", length = 3)
	public String iso3 = null;
	
	@Column(name = "num_code", length = 6)
	public Integer numberCode = null;
	
	@Column(name = "phone_code", nullable = false, length = 5)
	public Integer phoneCode;
}
