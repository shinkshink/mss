package models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "network_affiliate_type")
@SequenceGenerator(name = "network_affiliate_type_seq", sequenceName = "network_affiliate_type_seq")
public class NetworkAffiliateType implements Serializable{
	private static final long serialVersionUID = 2549127544035154188L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "network_affiliate_type_seq")
	@Column(name = "network_affiliate_type_id")
	public Long id;
	
	@Column(name = "code", nullable = false)
	public String code;
	
	@Column(name = "description")
	public String description;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NetworkAffiliateType other = (NetworkAffiliateType) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}
	
}
