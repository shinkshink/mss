package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "encashment_detail")
@SequenceGenerator(name = "encashment_detail_seq", sequenceName = "encashment_detail_seq")
public class EncashmentDetail implements Serializable {
	private static final long serialVersionUID = 8864053104224481926L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "encashment_detail_seq")
	@Column(name = "encashment_detail_id")
	public Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "encashment_id")
	public Encashment encashment;
	
	@Column(name = "previous_amount")
	public BigDecimal previousAmount;
	
	@Column(name = "available_amount")
	public BigDecimal availableAmount;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "member_id")
	public Member member;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", nullable = false)
	public Date date;
}
