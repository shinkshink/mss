package models;

public enum MemberTypeEnum {
	FACILITATOR("facilitator"),
	CLUB_MEMBER("club_member"),
	MERCHANT("merchant"),
	ADMIN("admin");

	private String code;

	private MemberTypeEnum(String code) {
		this.code  = code;
	}

	public String getCode() {
		return code;
	}
}
