package models;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "unilevel")
public class Unilevel implements Serializable {
	private static final long serialVersionUID = 6637775507053448577L;

	@Id
	@Column(name = "unilevel_id")
	public Integer rank;
	
	@Column(name = "amount", columnDefinition="decimal(3,2) not null", updatable=false)
	public BigDecimal percent;
	
}
