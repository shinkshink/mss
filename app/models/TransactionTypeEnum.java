package models;

public enum TransactionTypeEnum {
	DEBIT("debit"),
	CREDIT("credit");
	
	private String code;
	
	private TransactionTypeEnum(String code) {
		this.code  = code;
	}
	
	public String getCode() {
		return code;
	}
}
