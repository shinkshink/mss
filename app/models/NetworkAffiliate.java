package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "network_affiliate")
@SequenceGenerator(name = "network_affiliate_seq", sequenceName = "network_affiliate_seq")
public class NetworkAffiliate implements Serializable{
	
	private static final long serialVersionUID = -5921634563239179479L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "network_affiliate_seq")
	@Column(name = "network_affiliate_id")
	public Long id;
	
	@Column(name = "count_reward")
	public BigDecimal count;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "account_id")
    public Account account;
	
	@ManyToOne(fetch = FetchType.EAGER , optional = false)
    @JoinColumn(name = "network_affiliate_type_id")
    public NetworkAffiliateType networkAffiliateType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_update")
	public Date lastUpdate;
	
	public NetworkAffiliate(){
	}
	
	public NetworkAffiliate(Account account,  NetworkAffiliateType networkType,  BigDecimal count, Date lastUpdate){
		this.account = account;
		this.count = count;
		this.lastUpdate = lastUpdate;
		this.networkAffiliateType = networkType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((account == null) ? 0 : account.hashCode());
		result = prime * result + ((count == null) ? 0 : count.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((lastUpdate == null) ? 0 : lastUpdate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NetworkAffiliate other = (NetworkAffiliate) obj;
		if (account == null) {
			if (other.account != null)
				return false;
		} else if (!account.equals(other.account))
			return false;
		if (count == null) {
			if (other.count != null)
				return false;
		} else if (!count.equals(other.count))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastUpdate == null) {
			if (other.lastUpdate != null)
				return false;
		} else if (!lastUpdate.equals(other.lastUpdate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NetworkAffiliate [id=" + id + ", count=" + count + ", account="
				+ account + ", lastUpdate=" + lastUpdate + "]";
	}
	


}
