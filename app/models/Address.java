package models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "address")
@SequenceGenerator(name = "address_seq", sequenceName = "address_seq")
public class Address implements Serializable {
	private static final long serialVersionUID = -4582490585168773305L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_seq")
	@Column(name = "address_id")
	private Long id;
	
    @Column(name = "street", nullable = true)
	public String street;
    
    @Column(name = "state", nullable = true)
	public String state;
    
    @Column(name = "city_province", nullable = true)
	public String cityProvince;
    
    @Column(name = "zip_code", nullable = true)
	public Long zipCode;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_id")
    public Person person;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "country_id")
    public Country country;
}
