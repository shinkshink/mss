package models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "stub_claim")
@SequenceGenerator(name = "stub_claim_seq", sequenceName = "stub_claim_seq")
public class StubClaim implements Serializable{
	
	private static final long serialVersionUID = 8749013599876598033L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "stub_claim_seq")
	@Column(name = "stub_claim__id")
	public Long id;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "stub_id", nullable = false)
	public Stub stub;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "claimant_account_id", nullable = false)
	public Account claimant;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approver_account_id", nullable = false)
	public Account approver;
	
	@ManyToOne(fetch = FetchType.EAGER , optional = false)
    @JoinColumn(name = "network_affiliate_type_id")
    public NetworkAffiliateType networkAffiliateType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "claim_date")
	public Date claimDate;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((approver == null) ? 0 : approver.hashCode());
		result = prime * result
				+ ((claimDate == null) ? 0 : claimDate.hashCode());
		result = prime * result
				+ ((claimant == null) ? 0 : claimant.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StubClaim other = (StubClaim) obj;
		if (approver == null) {
			if (other.approver != null)
				return false;
		} else if (!approver.equals(other.approver))
			return false;
		if (claimDate == null) {
			if (other.claimDate != null)
				return false;
		} else if (!claimDate.equals(other.claimDate))
			return false;
		if (claimant == null) {
			if (other.claimant != null)
				return false;
		} else if (!claimant.equals(other.claimant))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	

}
