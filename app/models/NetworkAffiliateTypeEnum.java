package models;

public enum NetworkAffiliateTypeEnum {
	ORGANO_GOLD("organo_gold"),
	VANTAGE("vantage"),
	AIM("aim");
	
	private String code;
	
	private NetworkAffiliateTypeEnum(String code) {
		this.code  = code;
	}
	
	public String getCode() {
		return code;
	}
}