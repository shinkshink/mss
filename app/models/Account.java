package models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "account")
@SequenceGenerator(name = "account_seq", sequenceName = "account_seq")
public class Account implements Serializable {
	private static final long serialVersionUID = -6982613567494727657L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_seq")
	@Column(name = "account_id")
	public Long id;

	@Column(name = "email", nullable = false)
	public String email;

	@Column(name = "password", nullable = false)
	public String password;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="person_id", nullable = false)
	public Person person;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
	public Set<Member> members  = new HashSet<Member>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "sponsor")
	public Set<Enrollment> enrollAsSponsor  = new HashSet<Enrollment>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
	public Set<NetworkAffiliate> networkAffiliates = new HashSet<NetworkAffiliate>();
	
	public Account() {
		// TODO Auto-generated constructor stub
	}
	
	public Account(String email, String password, Person person) {
		super();
		this.email = email;
		this.password = password;
		this.person = person;
	}
	
	public NetworkAffiliate getVantageNetworkAffiliate() {
		for (NetworkAffiliate r : this.networkAffiliates) {
			if(r.networkAffiliateType.code.equals(NetworkAffiliateTypeEnum.VANTAGE.getCode())) {
				return r;
			}
		}
		return null;
	}
	
	public NetworkAffiliate getOrganoGoldNetworkAffiliate() {
		for (NetworkAffiliate r : this.networkAffiliates) {
			if(r.networkAffiliateType.code.equals(NetworkAffiliateTypeEnum.ORGANO_GOLD.getCode())) {
				return r;
			}
		}
		return null;
	}
	
	public NetworkAffiliate getAimNetworkAffiliate() {
		for (NetworkAffiliate r : this.networkAffiliates) {
			if(r.networkAffiliateType.code.equals(NetworkAffiliateTypeEnum.AIM.getCode())) {
				return r;
			}
		}
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}
	
}