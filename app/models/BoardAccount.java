package models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "board_account")
@SequenceGenerator(name = "board_account_seq", sequenceName = "board_account_seq", allocationSize = 1)
public class BoardAccount implements Serializable  {
	private static final long serialVersionUID = -2141172648392548230L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "board_account_seq")
	@Column(name = "board_account_id")
	public Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sponsor_account_id", nullable = false)
	public Account sponsor;

	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "sponsee_account_id", nullable = false)
	public Account sponsee;

	@ManyToOne(fetch=FetchType.EAGER, optional = true)
	@JoinColumn(name = "parent_board_account_id", referencedColumnName="board_account_id", nullable = true)
	public BoardAccount parentSponsor;

	@OneToOne(fetch=FetchType.EAGER, optional = true)
	@JoinColumn(name = "left_board_account_id", referencedColumnName="board_account_id", nullable = true)
	public BoardAccount leftSponsee;

	@OneToOne(fetch=FetchType.EAGER, optional = true)
	@JoinColumn(name = "right_board_account_id", referencedColumnName="board_account_id", nullable = true)
	public BoardAccount rightSponsee;

	@Column(name = "board_number")
	public Long boardNumber;

	@Column(name = "update_date")
	public Date updateDate;

	public BoardAccount() {
		super();
	}

	public BoardAccount(Account sponsor, Account sponsee, Date updateDate) {
		super();
		this.sponsor = sponsor;
		this.sponsee = sponsee;
		this.boardNumber = 1L;
		this.updateDate = updateDate;
	}

//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result
//				+ ((sponsor == null) ? 0 : sponsor.hashCode());
//		result = prime * result
//				+ ((sponsee == null) ? 0 : sponsee.hashCode());
//		result = prime * result
//				+ ((updateDate == null) ? 0 : updateDate.hashCode());
//		result = prime * result
//				+ ((boardNumber == null) ? 0 : boardNumber.hashCode());
//		result = prime * result
//				+ ((parentSponsor == null) ? 0 : parentSponsor.hashCode());
//		result = prime * result
//				+ ((leftSponsee == null) ? 0 : leftSponsee.hashCode());
//		result = prime * result
//				+ ((rightSponsee == null) ? 0 : rightSponsee.hashCode());
//		return result;
//	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoardAccount other = (BoardAccount) obj;
		if (sponsor == null) {
			if (other.sponsor != null)
				return false;
		} else if (!sponsor.equals(other.sponsor))
			return false;
		if (sponsee == null) {
			if (other.sponsee != null)
				return false;
		} else if (!sponsee.equals(other.sponsee))
			return false;
		if (updateDate == null) {
			if (other.updateDate != null)
				return false;
		} else if (!updateDate.equals(other.updateDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (leftSponsee == null) {
			if (other.leftSponsee != null)
				return false;
			} else if (!leftSponsee.equals(other.leftSponsee))
				return false;
		if (rightSponsee == null) {
			if (other.rightSponsee != null)
				return false;
		} else if (!rightSponsee.equals(other.rightSponsee))
			return false;
		if (parentSponsor == null) {
			if (other.parentSponsor != null)
				return false;
		} else if (!parentSponsor.equals(other.parentSponsor))
			return false;
		if (boardNumber == null) {
			if (other.boardNumber != null)
				return false;
		} else if (!boardNumber.equals(other.boardNumber))
			return false;
		return true;
	}
}
