package models;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "member")
@SequenceGenerator(name = "member_seq", sequenceName = "member_seq")
public class Member implements Serializable {
	private static final long serialVersionUID = 3641209799876563119L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "member_seq")
	@Column(name = "member_id")
	public Long id;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "account_id")
	public Account account;
	
	@Column(name = "username", nullable = false)
	public String username;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sponsor_member_id", nullable = true)
	public Member sponsor;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "placement_member_id", nullable = true)
	public Member placement;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "sponsor")
	public Set<Member> associates = new HashSet<Member>();

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "member_type_id")
	public MemberType memberType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "registration_date", updatable = false)
	public Date registrationDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved_date")
	public Date approvedDate;
	
	@Column(name = "active", nullable = false)
	public boolean active;
	
	@Column(name = "rank")
	public String rank;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expiry_date")
	public Date expiryDate;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "buyer")
	public Set<Purchase> purchasesAsMember  = new HashSet<Purchase>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "merchant")
	public Set<Purchase> purchasesAsMerchant = new HashSet<Purchase>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "member", cascade=CascadeType.ALL)
	public Set<Reward> rewards = new HashSet<Reward>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "sender")
	public Set<Transfer> creditTransfers = new HashSet<Transfer>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "receiver")
	public Set<Transfer> debitTransfers = new HashSet<Transfer>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "member")
	public Set<TransferDetail> transferDetails = new HashSet<TransferDetail>();
	
	public Member() {
		super();
	}

	public Reward getCashReward() {
		for (Reward r : this.rewards) {
			if(r.rewardType.code.equals(RewardTypeEnum.CASH.getCode())) {
				return r;
			}
		}
		return null;
	}
	
	public Reward getEcreditReward() {
		for (Reward r : this.rewards) {
			if(r.rewardType.code.equals(RewardTypeEnum.E_CREDIT.getCode())) {
				return r;
			}
		}
		return null;
	}
	
	public Reward getWealthReward() {
		for (Reward r : this.rewards) {
			if(r.rewardType.code.equals(RewardTypeEnum.WEALTH_POINT.getCode())) {
				return r;
			}
		}
		return null;
	}
	
	public Reward getSponsorReward() {
		for (Reward r : this.rewards) {
			if(r.rewardType.code.equals(RewardTypeEnum.SPONSOR.getCode())) {
				return r;
			}
		}
		return null;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result
				+ ((approvedDate == null) ? 0 : approvedDate.hashCode());
		result = prime * result
				+ ((expiryDate == null) ? 0 : expiryDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((memberType == null) ? 0 : memberType.hashCode());
		result = prime * result + ((rank == null) ? 0 : rank.hashCode());
		result = prime
				* result
				+ ((registrationDate == null) ? 0 : registrationDate.hashCode());
		result = prime * result + ((sponsor == null) ? 0 : sponsor.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Member other = (Member) obj;
		if (active != other.active)
			return false;
		if (approvedDate == null) {
			if (other.approvedDate != null)
				return false;
		} else if (!approvedDate.equals(other.approvedDate))
			return false;
		if (expiryDate == null) {
			if (other.expiryDate != null)
				return false;
		} else if (!expiryDate.equals(other.expiryDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (memberType == null) {
			if (other.memberType != null)
				return false;
		} else if (!memberType.equals(other.memberType))
			return false;
		if (rank == null) {
			if (other.rank != null)
				return false;
		} else if (!rank.equals(other.rank))
			return false;
		if (registrationDate == null) {
			if (other.registrationDate != null)
				return false;
		} else if (!registrationDate.equals(other.registrationDate))
			return false;
		if (sponsor == null) {
			if (other.sponsor != null)
				return false;
		} else if (!sponsor.equals(other.sponsor))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
}