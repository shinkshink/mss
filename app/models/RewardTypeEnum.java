package models;

public enum RewardTypeEnum {
	CASH("cash"),
	E_CREDIT("ecredit"),
	WEALTH_POINT("wealthpoint"),
	SPONSOR("sponsor");
	
	
	private String code;
	
	private RewardTypeEnum(String code) {
		this.code  = code;
	}
	
	public String getCode() {
		return code;
	}
}