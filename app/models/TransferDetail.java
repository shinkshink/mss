package models;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "transfer_detail")
@SequenceGenerator(name = "transfer_detail_seq", sequenceName = "transfer_detail_seq")
public class TransferDetail implements Serializable {
	private static final long serialVersionUID = 8864053104224481926L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transfer_detail_seq")
	@Column(name = "transfer_detail_id")
	public Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "transfer_id")
	public Transfer transfer;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "transaction_type_id")
	public TransactionType transactionType;
	
	@Column(name = "previous_amount")
	public BigDecimal previousAmount;
	
	@Column(name = "available_amount")
	public BigDecimal availableAmount;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "member_id")
	public Member member;
}
