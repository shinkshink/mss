package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * For every transfer of rewards there is one entry of <code>Transfer</code>.
 * For every <code>Transfer</code> there will be at strictly two <code>TransferDetail</code> entry,
 * one is of credit and one is debit.
 * 
 * @author Dan
 *
 */
@Entity
@Table(name = "transfer")
@SequenceGenerator(name = "transfer_seq", sequenceName = "transfer_seq")
public class Transfer implements Serializable{
	private static final long serialVersionUID = 1633037110577922396L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transfer_seq")
	@Column(name = "transfer_id")
	public Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "sender_member_id")
	public Member sender;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "receiver_member_id")
	public Member receiver;
	
	@Column(name = "amount")
	public BigDecimal amount;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "reward_type_id")
	public RewardType rewardType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", nullable = false)
	public Date date;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "transfer")
	public Set<TransferDetail> transferDetails = new HashSet<TransferDetail>();
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "purchase_id")
	public Purchase purchase;
	
	@ManyToOne(fetch = FetchType.EAGER , optional = false)
    @JoinColumn(name = "transfer_type_id")
    public TransferType transferType;
}

