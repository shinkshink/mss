package models;

public enum ClubMemberEnrollTypeEnum {
	CLASSIC("classic"),
	PREMIUM("premium");
	
	private String code;
	
	private ClubMemberEnrollTypeEnum(String code) {
		this.code  = code;
	}
	
	public String getCode() {
		return code;
	}
}