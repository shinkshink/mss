package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "stub")
@SequenceGenerator(name = "stub_seq", sequenceName = "stub_seq")
public class Stub implements Serializable{
	
	private static final long serialVersionUID = 8749013599876598033L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "stub_seq")
	@Column(name = "stub_id")
	public Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "claimant_account_id", nullable = false)
	public Account claimant;
	
	@ManyToOne(fetch = FetchType.EAGER , optional = false)
    @JoinColumn(name = "network_affiliate_type_id")
    public NetworkAffiliateType networkAffiliateType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "generation_date")
	public Date generationDate;
	
	@Column(name= "stub_count")
	public BigDecimal stubCnt;	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((claimant == null) ? 0 : claimant.hashCode());
		result = prime * result
				+ ((generationDate == null) ? 0 : generationDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stub other = (Stub) obj;
		if (claimant == null) {
			if (other.claimant != null)
				return false;
		} else if (!claimant.equals(other.claimant))
			return false;
		if (generationDate == null) {
			if (other.generationDate != null)
				return false;
		} else if (!generationDate.equals(other.generationDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


}
