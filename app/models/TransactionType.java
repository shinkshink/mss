package models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "transaction_type")
@SequenceGenerator(name = "transaction_type_seq", sequenceName = "transaction_type_seq")
public class TransactionType implements Serializable {
	private static final long serialVersionUID = 2263471770680743884L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_type_seq")
	@Column(name = "transaction_type_id")
	public Long id;
	
	@Column(name = "code", nullable = false)
	public String code;
	
	@Column(name = "description")
	public String description;
}
