package models.dto;

import java.util.Date;

public class MemberUnilevelDto {
	
	String fullName;
	String userName;
	String rank;
	Date joinDate;
	Long numberDowlines;
	Boolean active;
	
	public MemberUnilevelDto(String fullName, String userName, String rank,
			Date joinDate, Long numberDowlines, Boolean active) {
		super();
		this.fullName = fullName;
		this.userName = userName;
		this.rank = rank;
		this.joinDate = joinDate;
		this.numberDowlines = numberDowlines;
		this.active = active;
	}
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	public Long getNumberDowlines() {
		return numberDowlines;
	}
	public void setNumberDowlines(Long numberDowlines) {
		this.numberDowlines = numberDowlines;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	
	

}
