package models.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

@Entity
@SqlResultSetMapping(name="stubReport", entities=@EntityResult(entityClass=StubScheduleDto.class))
@NamedNativeQuery(name="stubReportSql", 
	query="select a.account_id account, a.first_name first_name, a.last_name last_name, "
			+ "(count_reward/3) stub_count, code, current_timestamp date_generated, "
			+ "na.network_affiliate_type_id network_affiliate_type_id "
			+ "from network_affiliate na "
			+ "inner join (select acc.account_id, p.first_name, p.last_name from account acc "
			+ "inner join person p on acc.person_id = p.person_id) a on na.account_id = a.account_id "
			+ "inner join network_affiliate_type nat on na.network_affiliate_type_id = nat.network_affiliate_type_id "
			+ "where na.count_reward >= 3 order by account;", resultSetMapping="stubReport")
public class StubScheduleDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long accountId;
	private String fName;
	private String lName;
	private BigDecimal stubCnt;
	private String code;
	private Date dateGen;
	private Long netAffTypId;

	@Id
	@Column(name="account")
	public Long getAccountId() {
		return accountId;
	}
	
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	
	@Column(name="first_name")
	public String getfName() {
		return fName;
	}
	
	public void setfName(String fName) {
		this.fName = fName;
	}
	
	@Column(name="last_name")
	public String getlName() {
		return lName;
	}
	
	public void setlName(String lName) {
		this.lName = lName;
	}
	
	@Column(name="stub_count")
	public BigDecimal getStubCnt() {
		return stubCnt;
	}
	
	public void setStubCnt(BigDecimal stubCnt) {
		this.stubCnt = stubCnt;
	}
	
	@Column(name="code")
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	@Column(name="date_generated")
	public Date getDateGen() {
		return dateGen;
	}
	
	public void setDateGen(Date dateGen) {
		this.dateGen = dateGen;
	}
	
	@Id
	@Column(name="network_affiliate_type_id")
	public Long getNetAffTypId() {
		return netAffTypId;
	}

	public void setNetAffTypId(Long netAffTypId) {
		this.netAffTypId = netAffTypId;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		sb.append("AccountId: " + this.accountId + "/n");
		sb.append("FirstName: " + this.fName + "/n");
		sb.append("LastName: " + this.lName + "/n");
		sb.append("StubCount: " + this.stubCnt + "/n");
		sb.append("Code: " + this.code + "/n");
		sb.append("Date: " + this.dateGen + "/n");
		sb.append("Network Affiliate Type Id: " + this.netAffTypId + "/n");
		
		return sb.toString();
	}
}
