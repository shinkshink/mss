package models.dto;

import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;

public class FacilitatorProfileDto {
	//uneditable fields
	public Long id;

	//editable fields
	@Required
	@MinLength(value = 4)
	public String userName;
	@Required
	public String firstName;
	@Required
	public String middleName;
	@Required
	public String lastName;
	public String gender;
	public String mobileNumber;
	public String email;
	public String password;


	public FacilitatorProfileDto() {
		// TODO Auto-generated constructor stub
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String userName) {
		this.userName = userName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

}
