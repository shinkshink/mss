package models.dto;

import play.data.validation.Constraints.Required;

public class MemberProfileDto {
	//uneditable fields
	public Long id;
	public String username;
	public String sponsorName;
	
	//editable fields
	@Required
	public String firstName;
	@Required
	public String middleName;
	@Required
	public String lastName;
	public String gender;
	public String mobileNumber;
	public String email;
	public String password;
	
	public String street;
	public String state;
	public String cityProvince;
	public Long zipCode;
	public Long countryId;
	
	public MemberProfileDto() {
		// TODO Auto-generated constructor stub
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setSponsorName(String sponsorName) {
		this.sponsorName = sponsorName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setCityProvince(String cityProvince) {
		this.cityProvince = cityProvince;
	}

	public void setZipCode(Long zipCode) {
		this.zipCode = zipCode;
	}
	
}
