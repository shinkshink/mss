package models.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;


@Entity
@SqlResultSetMapping(name="boardData", entities=@EntityResult(entityClass=BoardDto.class))
@NamedNativeQuery(name="boardDataSql", 
query="with recursive boards(board_account_id, username, fullname, email, update_date, board_number) AS ( " +
	
	"select ba.board_account_id, m.username, p.first_name || ' ' || p.last_name as fullname, a.email, ba.update_date,  " +
	"ba.left_board_account_id, ba.right_board_account_id, ba.board_number " +
	"from board_account ba  " +
	"inner join member m on ba.sponsee_account_id = m.account_id  " +
	"inner join account a on m.account_id = a.account_id  " +
	"inner join person p on a.person_id = p.person_id  " +
	"where ba.board_account_id = ? " +
	"and ba.sponsor_account_id = (select account_id from member where member_id = m.sponsor_member_id)  " +
	"and ba.board_number = ? " +

	"union all " +

	"select ba.board_account_id, m.username, p.first_name || ' ' || p.last_name as fullname, a.email, ba.update_date,  " +
	"ba.left_board_account_id, ba.right_board_account_id, ba.board_number " +
	"from board_account ba " +
	"inner join boards boards on ba.parent_board_account_id = boards.board_account_id " +
	"inner join member m on ba.sponsee_account_id = m.account_id " +
	"inner join account a on m.account_id = a.account_id " +
	"inner join person p on a.person_id = p.person_id " +
	"where m.sponsor_member_id not in (select member_id from member where account_id = m.account_id) " +
	"and ba.board_number = ? " +

	") select * from boards " +
	"order by board_account_id;", resultSetMapping="boardData")
public class BoardDto {

	public Long id;
	
	public String username;

	public String name;

	public String email;

	public Date updateDate;

	public Long boardNumber;
	
	public BoardDto() {
	}

	public BoardDto(Long id, String username, String name, String email, Date updateDate) {
		super();
		this.id = id;
		this.username = username;
		this.name = name;
		this.email = email;
		this.updateDate = updateDate;
	}

	@Id
	@Column(name="board_account_id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="username")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name="fullname")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="update_date")
	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Column(name="board_number")
	public Long getBoardNumber() {
		return boardNumber;
	}

	public void setBoardNumber(Long boardNumber) {
		this.boardNumber = boardNumber;
	}

}
