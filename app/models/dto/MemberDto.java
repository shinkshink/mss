package models.dto;

import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;

public class MemberDto {
	@Required
	@MinLength(value = 4)
	public String username;
	@Required
	public String firstName;
	@Required
	public String middleName;
	@Required
	public String lastName;
	@Required
	@Email
	public String email;
	@Required
	@MinLength(value = 6)
	public String password;
	@Required
	public String sponsorUsername;
	@Required
	public String captcha;

	public String rank;

	public boolean active;

	public String fullName;

	public String code;

	public String sponsorFullName;

	public MemberDto() {
		// TODO Auto-generated constructor stub
	}
	public MemberDto(String username, String firstName, String middleName,
			String lastName, String email, String password,
			String sponsorUsername, String rank) {
		super();
		this.username = username;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.sponsorUsername = sponsorUsername;
		this.rank = rank;
	}

   public MemberDto(String username, String fullName, String code, boolean active) {
        super();
        this.username = username;
        this.fullName = fullName;
        this.code = code;
        this.active = active;
    }

   public MemberDto(String username, String password) {
       super();
       this.username = username;
       this.password = password;
   }
}
