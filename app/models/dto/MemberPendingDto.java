package models.dto;

import java.util.Date;

public class MemberPendingDto {
	
	Long id;
	String fullName;
	String userName;
	Date joinDate;
	Date expiryDate;
	
	public MemberPendingDto(Long id, String fullName, String userName, Date joinDate,
			Date expiryDate) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.userName = userName;
		this.joinDate = joinDate;
		this.expiryDate = expiryDate;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}
