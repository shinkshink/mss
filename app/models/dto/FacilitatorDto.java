package models.dto;

import java.math.BigDecimal;

public class FacilitatorDto {

    public String username;;
    public BigDecimal amount;
    public String fullName;
    public boolean action;

    public FacilitatorDto() {
        // TODO Auto-generated constructor stub
    }
    public FacilitatorDto(String username, String fullName) {
        super();
        this.username = username;
        this.fullName = fullName;
    }

}
