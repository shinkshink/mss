package models.dto;

import java.io.File;

public class ReportDto {

	public String name;
	
	public String path;
	
	public File file;
	
	public ReportDto(String name, String path) {
		super();
		this.name = name;
		this.path = path;
	}
}
