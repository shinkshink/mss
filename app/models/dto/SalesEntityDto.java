package models.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@Entity
@SqlResultSetMapping(name="salesReport", entities=@EntityResult(entityClass=SalesEntityDto.class))
public class SalesEntityDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5938763773852496101L;

	@Id
	@Column(name="username")
	public String username;
	
	@Column(name="full_name")
	public String fullName;

	@Column(name="amount")
	public String amount;

	public SalesEntityDto() {
		super();
	}
	
	public SalesEntityDto(String username, String fullName, String amount) {
		this.username = username;
		this.fullName = fullName;
		this.amount = amount;
	}
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@Override
	public String toString() {
		return "SalesEntityDto [username=" + username + ", fullName="
				+ fullName + ", amount=" + amount + "]";
	}

}
