package models.dto;


public class SalesDto {
	public String username;
	public String fullName;
	public String amount;
	public String date;
	public SalesDto(String username, String fullName, String amount, String date) {
		super();
		this.username = username;
		this.fullName = fullName;
		this.amount = amount;
		this.date = date;
	}
}
