package models.dto;

import java.math.BigDecimal;

public class GroceryDto {
	public String username;
	public BigDecimal amount;
}
