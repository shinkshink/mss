package report;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import models.Reward;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import play.Logger;
import util.StaticContentUtil;

public class SponsorSchedulerReport {

    private static final String CLASSNAME = "SponsorSchedulerReport";
    private static final SimpleDateFormat DATE_FORMAT1 =
            new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aaa");
    private static final SimpleDateFormat DATE_FORMAT2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    private XSSFWorkbook wb;
    private XSSFSheet sheet;
    private String fileName;
    private String fileDir;
    private String date;

    public SponsorSchedulerReport() {
        Logger.info(CLASSNAME + " SponsorSchedulerReport");

        this.wb = new XSSFWorkbook();
        this.sheet = this.wb.createSheet("Sponsor Report");
        this.date = DATE_FORMAT2.format(new Date(System.currentTimeMillis()));
        this.fileDir = System.getProperty("user.home") + StaticContentUtil.HOME_DIR;
        this.fileName = "Sponsor_Report_" + this.date + ".xlsx";
    }

    public void writeWeeklySponsorReport(List<Reward> rewardList) {
        Logger.info(CLASSNAME + " writeWeeklySponsorReport START");

        FileOutputStream fileOut = null;

        try {
            this.writeHeader();
            this.writeDetails(rewardList);

            fileOut = new FileOutputStream(this.fileDir + File.separator + this.fileName);

            this.wb.write(fileOut);

            fileOut.flush();
        } catch (FileNotFoundException e) {
            Logger.error(CLASSNAME + " writeWeeklySponsorReport FileNotFoundException " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Logger.error(CLASSNAME + " writeWeeklySponsorReport IOException " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Logger.error(CLASSNAME + " writeWeeklySponsorReport Exception " + e.getMessage());
        	e.printStackTrace();
        } finally {
            if (fileOut != null) {
                try {
                    fileOut.close();
                } catch (IOException e) {
                    Logger.error(CLASSNAME + " writeWeeklySponsorReport IOException2 " + e.getMessage());
                    e.printStackTrace();
                }
            }
            Logger.info(CLASSNAME + " writeWeeklySponsorReport END");
        }

    }

    private void writeHeader() {
        Logger.info(CLASSNAME + " writeHeader START");

        XSSFRow row = sheet.createRow(1);
        XSSFCell cell = row.createCell(0);
        cell.setCellValue("Sponsor Weekly Report");

        row = sheet.createRow(2);
        cell = row.createCell(0);
        cell.setCellValue("Date: " + this.date);

        row = sheet.createRow(4);
        cell = row.createCell(0);
        cell.setCellValue("Last Name");

        cell = row.createCell(1);
        cell.setCellValue("First Name");

        cell = row.createCell(2);
        cell.setCellValue("Username");

        cell = row.createCell(3);
        cell.setCellValue("Amount");

        cell = row.createCell(4);
        cell.setCellValue("10% Tax");

        cell = row.createCell(5);
        cell.setCellValue("Service Charge");

        cell = row.createCell(6);
        cell.setCellValue("Total");

        cell = row.createCell(7);
        cell.setCellValue("Date");
        Logger.info(CLASSNAME + " writeHeader END");
    }

    private void writeDetails(List<Reward> rewardList) {
        Logger.info(CLASSNAME + " writeDetails START");

        int rowCnt = 5;
        XSSFRow row = null;
        XSSFCell cell = null;

        for (Reward reward : rewardList) {

        	BigDecimal amount = reward.amount;
        	BigDecimal tenPercent = amount.multiply(new BigDecimal(.1)).setScale(2, RoundingMode.HALF_EVEN);
        	BigDecimal total = amount.subtract(tenPercent.add(new BigDecimal(StaticContentUtil.SERVICE_CHARGE))).setScale(2, RoundingMode.HALF_EVEN);

            row = sheet.createRow(rowCnt);
            cell = row.createCell(0);
            cell.setCellValue(reward.member.account.person.lastName);

            cell = row.createCell(1);
            cell.setCellValue(reward.member.account.person.firstName);

            cell = row.createCell(2);
            cell.setCellValue(reward.member.username);

            cell = row.createCell(3);
            cell.setCellValue(amount.doubleValue());

            cell = row.createCell(4);
            cell.setCellValue(tenPercent.doubleValue());

            cell = row.createCell(5);
            cell.setCellValue(StaticContentUtil.SERVICE_CHARGE);

            cell = row.createCell(6);
            cell.setCellValue(total.doubleValue());

            cell = row.createCell(7);
            cell.setCellValue(DATE_FORMAT2.format(reward.lastUpdate));

            rowCnt++;
        }
        Logger.info(CLASSNAME + " writeDetails END");
    }
}
