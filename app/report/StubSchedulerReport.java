package report;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import models.dto.StubScheduleDto;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import play.Logger;

public class StubSchedulerReport {

    private static final String CLASSNAME = "StubSchedulerReport";

    private static final SimpleDateFormat DATE_FORMAT1 =
            new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aaa");
    private static final SimpleDateFormat DATE_FORMAT2 = new SimpleDateFormat("yyyy-MM-dd");

    private XSSFWorkbook wb;
    private XSSFSheet sheet;
    private String fileName;
    private String date;

    public StubSchedulerReport() {
        Logger.info(CLASSNAME + " StubSchedulerReport");

        this.wb = new XSSFWorkbook();
        this.sheet = this.wb.createSheet("Stub Report");
        this.date = DATE_FORMAT2.format(new Date(System.currentTimeMillis()));
        this.fileName = System.getProperty("user.dir") + "/report/Stub_Report_" + this.date
                + ".xlsx";
    }

    public void writeWeeklyStubReport(List<StubScheduleDto> stubDtoLst) {
        Logger.info(CLASSNAME + " writeWeeklyStubReport START");

        File file = new File(this.fileName);
        FileOutputStream fileOut = null;

        try {
            fileOut = new FileOutputStream(file);

            this.writeHeader();
            this.writeDetails(stubDtoLst);

            this.wb.write(fileOut);

            fileOut.flush();
        } catch (FileNotFoundException e) {
            Logger.error(CLASSNAME + " writeWeeklyStubReport FileNotFoundException " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Logger.error(CLASSNAME + " writeWeeklyStubReport IOException " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (fileOut != null) {
                try {
                    fileOut.close();
                } catch (IOException e) {
                    Logger.error(CLASSNAME + " writeWeeklyStubReport IOException2 " + e.getMessage());
                    e.printStackTrace();
                }
            }
            Logger.info(CLASSNAME + " writeWeeklyStubReport END");
        }

    }

    private void writeHeader() {
        Logger.info(CLASSNAME + " writeHeader START");

        XSSFRow row = sheet.createRow(1);
        XSSFCell cell = row.createCell(0);
        cell.setCellValue("Stub Weekly Report");

        row = sheet.createRow(2);
        cell = row.createCell(0);
        cell.setCellValue("Date: " + this.date);

        row = sheet.createRow(4);
        cell = row.createCell(0);
        cell.setCellValue("Account ID");

        cell = row.createCell(1);
        cell.setCellValue("Last Name");

        cell = row.createCell(2);
        cell.setCellValue("First Name");

        cell = row.createCell(3);
        cell.setCellValue("No. of Stub");

        cell = row.createCell(4);
        cell.setCellValue("Network Type");

        cell = row.createCell(5);
        cell.setCellValue("Date");
        Logger.info(CLASSNAME + " writeHeader END");
    }

    private void writeDetails(List<StubScheduleDto> stubDtoLst) {
        Logger.info(CLASSNAME + " writeDetails START");

        int rowCnt = 5;
        XSSFRow row = null;
        XSSFCell cell = null;

        for (StubScheduleDto stubDto : stubDtoLst) {

            row = sheet.createRow(rowCnt);
            cell = row.createCell(0);
            cell.setCellValue(stubDto.getAccountId().toString());

            cell = row.createCell(1);
            cell.setCellValue(stubDto.getlName());

            cell = row.createCell(2);
            cell.setCellValue(stubDto.getfName());

            cell = row.createCell(3);
            cell.setCellValue(stubDto.getStubCnt().toPlainString());

            cell = row.createCell(4);
            cell.setCellValue(stubDto.getCode());

            cell = row.createCell(5);
            cell.setCellValue(DATE_FORMAT1.format(stubDto.getDateGen()));

            rowCnt++;
        }
        Logger.info(CLASSNAME + " writeDetails END");
    }
}
