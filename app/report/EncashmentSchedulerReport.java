package report;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import models.Encashment;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import play.Logger;
import util.StaticContentUtil;

public class EncashmentSchedulerReport {

    private static final String CLASSNAME = "EncashmentSchedulerReport";

    private static final SimpleDateFormat DATE_FORMAT1 =
            new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aaa");
    private static final SimpleDateFormat DATE_FORMAT2 = new SimpleDateFormat("yyyy-MM-dd");

    private XSSFWorkbook wb;
    private XSSFSheet sheet;
    private String fileName;
    private String fileDir;
    private String date;

    public EncashmentSchedulerReport() {
        Logger.info(CLASSNAME + " EncashmentSchedulerReport");

        this.wb = new XSSFWorkbook();
        this.sheet = this.wb.createSheet("Encashment Report");
        this.date = DATE_FORMAT2.format(new Date(System.currentTimeMillis()));
        this.fileDir = System.getProperty("user.home") + StaticContentUtil.HOME_DIR;
        this.fileName = "/Encashment_Report_" + this.date + ".xlsx";
    }

    public void writeEncashmentReport(List<Encashment> encashmentList) {
        Logger.info(CLASSNAME + " writeEncashmentReport START");

        File file = new File(this.fileDir + this.fileName);
        FileOutputStream fileOut = null;

        try {
            fileOut = new FileOutputStream(file);

            this.writeHeader();
            this.writeDetails(encashmentList);

            this.wb.write(fileOut);

            fileOut.flush();
        } catch (FileNotFoundException e) {
            Logger.error(CLASSNAME + " writeEncashmentReport FileNotFoundException " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Logger.error(CLASSNAME + " writeEncashmentReport IOException " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (fileOut != null) {
                try {
                    fileOut.close();
                } catch (IOException e) {
                    Logger.error(CLASSNAME + " writeEncashmentReport IOException2 " + e.getMessage());
                    e.printStackTrace();
                }
            }
            Logger.error(CLASSNAME + " writeEncashmentReport END");
        }

    }

    private void writeHeader() {
        Logger.info(CLASSNAME + " writeHeader START");

        XSSFRow row = sheet.createRow(1);
        XSSFCell cell = row.createCell(0);
        cell.setCellValue("Encashment Report");

        row = sheet.createRow(2);
        cell = row.createCell(0);
        cell.setCellValue("Date: " + this.date);

        row = sheet.createRow(4);
        cell = row.createCell(0);
        cell.setCellValue("Name");

        cell = row.createCell(1);
        cell.setCellValue("Amount");

        cell = row.createCell(2);
        cell.setCellValue("Date");
        Logger.info(CLASSNAME + " writeHeader END");
    }

    private void writeDetails(List<Encashment> encashmentList) {
        Logger.info(CLASSNAME + " writeDetails START");

        int rowCnt = 5;
        XSSFRow row = null;
        XSSFCell cell = null;

        for (Encashment encashment : encashmentList) {

            row = sheet.createRow(rowCnt);
            cell = row.createCell(0);
            cell.setCellValue(encashment.account.person.firstName + " " + encashment.account.person.lastName);

            cell = row.createCell(1);
            cell.setCellValue(encashment.amount.toPlainString());

            cell = row.createCell(2);
            cell.setCellValue(DATE_FORMAT1.format(encashment.encashmentDate));

            rowCnt++;
        }
        Logger.info(CLASSNAME + " writeDetails END");
    }

//    public static void main(String[] arg) {
//        File dir = new File(System.getProperty("user.dir") + "/report");
//        File [] files = dir.listFiles(new FilenameFilter() {
//            @Override
//            public boolean accept(File dir, String name) {
//                return name.endsWith(".xlsx");
//            }
//        });
//
//        for (File xmlfile : files) {
//            System.out.println(xmlfile);
//        }
//    }
}
