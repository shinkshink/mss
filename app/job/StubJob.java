package job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import service.StubReportService;

public class StubJob implements Job {

    private static final String CLASSNAME = "StubJob";

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
	    Logger.info(CLASSNAME + " execute");
		//Stub Report
		this.runReport();
	}

	@Transactional
	private void runReport() {
	    Logger.info(CLASSNAME + " runReport START");

		JPA.withTransaction(new play.libs.F.Callback0() {
			@Override
            public void invoke() {

				try {
					StubReportService.generateStubReport();

				} catch (Exception e) {
				    Logger.error(CLASSNAME + " runReport Exception " + e.getMessage());
					e.printStackTrace();
				}
			}
		});

		Logger.info(CLASSNAME + " runReport END");
	}
}
