var TableManaged = function () {

    return {

        //main function to initiate the module
        init: function (data) {
            if (!jQuery().dataTable) {
                return;
            }
            // begin first table
            $('#sample_1').dataTable({
                "aoColumns": [
                  { "bSortable": false },
                  null,
                  { "bSortable": false },
                  null,
                  { "bSortable": false },
                  { "bSortable": false }
                ],
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#sample_1 .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                        $(this).parents('tr').addClass("active");
                    } else {
                        $(this).attr("checked", false);
                        $(this).parents('tr').removeClass("active");
                    }                    
                });
                jQuery.uniform.update(set);
            });

            jQuery('#sample_1 tbody tr .checkboxes').change(function(){
                 $(this).parents('tr').toggleClass("active");
            });

            jQuery('#sample_1_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#sample_1_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            
            // second first table
            $('#unilevel_1').dataTable({
            	"aaData":data,
                "aoColumns": [ {
                        	  "mData": "fullName",
                        	  "bSortable": true,
                         	 "sTitle": "FULL NAME"
                          },
                           { "mData": "userName",
                        	   "bSortable": true,
                             "sTitle": "USERNAME",
                             "mRender": function ( data, type, full ) {
//                                 return '<a href="?userName=' + data + '&level=1" data-id="' + data + '" onclick="loadUser(' +"'"+ data+"'" + ');return false;" id="unilevel_match_user' + data + '">'+data+'</a>';
                            	 return '<a onclick="displayDownlines(' +"'"+ data+"'" + ',1);" id="unilevel_match_user' + data + '">'+data+'</a>';
                                 
                               }
                            	
                              },
                           { 
                              "mData": "active",
                            	 "bSortable": true,
                             "sTitle": "ACTIVE"
                              },
                           { 
                              "mData": "numberDowlines",
                            	 "bSortable": true,
                              "sTitle": "# of DOWNLINES"
                              },
                           { 
                              "mData": "rank",
                            	  "bSortable": true,
                              "sTitle": "RANK"
                              },
                           { 
                              "mData": "joinDate",
                            	  "bSortable": false,
                                "sTitle": "JOIN DATE"
                             }
                ],
                "bRetrieve": true, 
                "bProcessing": true, 
                "bDestroy": true,
                "aLengthMenu": [
                                [5, 15, 20, -1],
                                [5, 15, 20, "All"] // change per page values here
                            ],
                            // set the initial value
                            "iDisplayLength": 5,
                            "sPaginationType": "bootstrap",
                            "oLanguage": {
                                "sLengthMenu": "_MENU_ records",
                                "oPaginate": {
                                    "sPrevious": "Prev",
                                    "sNext": "Next"
                                }
                            },
                            "aoColumnDefs": [{
                                    'bSortable': false,
                                    'aTargets': [0]
                                }
                            ]
            });


            jQuery('#unilevel_1_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#unilevel_1_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            
            
            // second first table
            $('#pending_associates').dataTable({
            	"aaData":data,
                "aoColumns": [ {
                        	  "mData": "fullName",
                        	  "bSortable": true,
                         	 "sTitle": "FULL NAME"
                         	 
                         	
                          },
                           { "mData": "userName",
                        	   "bSortable": true,
                             "sTitle": "USERNAME NAME"
                              },
                           { 
                              "mData": "joinDate",
                            	 "bSortable": true,
                             "sTitle": "JOINED DATE"
                              },
                      
                           { 
                              "mData": "expiryDate",
                            	  "bSortable": true,
                              "sTitle": "EXPIRY DATE"
                              },
                              {
                            	  "mData": "id",
                            	  "bSortable": true,
                             	 "sTitle": "Action",
                             	 "mRender": function ( data, type, full ) {
                                    return '<input type="hidden" value="'+data+'"/>'+'<button type="button" class="demo btn btn-primary btn-small" data-toggle="modal" href="#responsive" id="preApprovedId"  onclick="goMember(this); return false;">approve</button>';
                                  }
                              }
                ],
                "aLengthMenu": [
                                [5, 15, 20, -1],
                                [5, 15, 20, "All"] // change per page values here
                            ],
                            // set the initial value
                            "iDisplayLength": 5,
                            "sPaginationType": "bootstrap",
                            "oLanguage": {
                                "sLengthMenu": "_MENU_ records",
                                "oPaginate": {
                                    "sPrevious": "Prev",
                                    "sNext": "Next"
                                }
                            },
                            "aoColumnDefs": [{
                                    'bSortable': false,
                                    'aTargets': [0]
                                }
                            ]
            });


            jQuery('#pending_associates_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#pending_associates_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            
        }

    };

}();
