
function clearUI() {
	$('#errorBox').hide();
	$('#error1').hide();
	$('#error2').hide();
	$('#error1').html('');
	$('#error2').html('');
	$('#successBox').hide();
	$("#usernameDiv").attr('class', 'form-group');
	$("#amountDiv").attr('class', 'form-group');
}

function submitMemberAccount(username) {
	appRoutes.controllers.MemberController.displayAccountMemberRewards(username).ajax( {
		success : function (response) {
			  clearUI();
			  if(response.status == 'OK') {
				  $('#cashValue').html(response.cash);
				  $('#unilevelValue').html(response.unilevel);
				  $('#wealthValue').html(response.wealth);
				  $('#sponsorValue').html(response.sponsor);
				  $('#usernameSelectedValue').html(response.usernameSelected);
				  
			  }else {
				  $('#errorBox').show();
				  if(response.error1 != "") {
					  $('#errorBox').show();
					  $('#errorBox').append(response.error1);
				  }
				  
			  }
		},
		error : function (error) {
			//TODO
		}
	});
}

function clearFields() {
	$('#memberUserName').val("");
	$('#amount').val("");
}

function errorUsername(message) {
	$("#posDiv").attr('class', 'form-group has-error');
	$('#error2').show();
	$('#error2').html(message);
	$('#amount').val("");
}