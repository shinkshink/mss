$(document).ready(function () {
	clearUI();
	clearFields();
	/*testing */
	$('#submitEncashmentBtn').on( 'click', function (e) {
		var username = $('#memberUserName').val();
		var amount = $('#amount').val();
		

		if(amount === '') {
			errorAmount('Please enter amount');
		} else{
			if (isNaN(amount)) //check if number
			{
				errorAmount('Please enter number only.');
			}else {
				
					$('#errorBox').hide();
					bootbox.confirm("Confirm transfer of " + amount + ".", function(result) {
						if(result) {
							bootbox.hideAll();
							submitTransfer(amount);
						}
					}); 
				
			}
	}
	});
	//reset fields
	$('#resetBtn').on( 'click', function (e) {
		clearUI();
		clearFields();
	});
});

function clearUI() {
	$('#errorBox').hide();
	$('#error1').hide();
	$('#error2').hide();
	$('#error1').html('');
	$('#error2').html('');
	$('#successBox').hide();
	$("#usernameDiv").attr('class', 'form-group');
	$("#amountDiv").attr('class', 'form-group');
}

function submitTransfer(amount) {
	appRoutes.controllers.RewardController.submitCashEncashment(amount).ajax( {
		success : function (response) {
			  clearUI();
			  if(response.status == 'OK') {
				  $('#successBox').show();
				  $('#memberUserName').val("");
				  $('#amount').val("");
			  }else {
				  $('#errorBox').show();
				  if(response.error1 != "") {
					  $("#usernameDiv").attr('class', 'form-group has-error');
					  $('#error1').show();
					  $('#error1').html(response.error1);
				  }
				  if(response.error2 != "") {
					  $('#amount').val("");
					  $("#amountDiv").attr('class', 'form-group has-error');
					  $('#error2').show();
					  $('#error2').html(response.error2);
				  }
			  }
		},
		error : function (error) {
			//TODO
		}
	});
}

function clearFields() {
	$('#memberUserName').val("");
	$('#amount').val("");
}

function errorAmount(message) {
	$("#amountDiv").attr('class', 'form-group has-error');
	$('#error2').show();
	$('#error2').html(message);
	$('#amount').val("");
}