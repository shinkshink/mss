var activeStartDate;
var activeEndDate;

$(document).ready(function () {
	clearPassFields();
	$('#pass-error').hide();
	$('#active-error').hide();
	$('#ecredit-error').hide();
	
	//Change password
	$('#savePassword').on('click', function (e) {
		username = $("#membername").val();
		password = $("#password").val();
		repeatePassword = $("#repeatePassword").val();
		
		appRoutes.controllers.AdminController.changePassword(username, password, repeatePassword).ajax( {
			success : function (response) {
				if(response.status == 'OK') {
					$('#pass-error').hide();
					$("#passwordDialog").modal('hide');
				} else {
					$('#pass-error').show();
					$('#pass-error').html(response.error);
				}
			},
			error : function (error) {
				$('#pass-error').show();
				$('#pass-error').html('Error occurred! Member\'s password was not changed.');
			}
		});

	});
	
	//Add E-Credits
	$('#addECredits').on('click', function (e) {
		username = $("#membername").val();
		addecredit = $("#addecredit").val();
		
		appRoutes.controllers.AdminController.addECredits(username, addecredit).ajax( {
			success : function (response) {
				if(response.status == 'OK') {
					$('#ecredit-error').hide();
					$("#eCreditDialog").modal('hide');
					var oDataTable = $('#faciTable').dataTable({ bRetrieve : true });
					oDataTable.fnDraw();
					$('table').hide();
					$('#faciTable').show();
				} else {
					$('#ecredit-error').show();
					$('#ecredit-error').html(response.error);
				}
			},
			error : function (error) {
				$('#ecredit-error').show();
				$('#ecredit-error').html('Error occurred! Facilitator\'s E-Credit was not added.');
			}
		});
	});
	

});

//deactivate member
function deactivate(username) {
	$('#active-error').hide();
	appRoutes.controllers.AdminController.deactivate(username).ajax( {
		success : function (response) {
			if(response.status == 'OK') {
				var oTable = $('#memberTable').DataTable();
				var data = oTable.fnGetData();
				for (var e in data) {
					if (data[e].username == username) {
				    	oTable.fnUpdate("", parseInt(e), 3, true, true);
				        break;
				    }
				}
			} else {
				$('#active-error').show();
				$('#active-error').html(response.error1);
			}
		},
		error : function (error) {
			$('#active-error').show();
			$('#active-error').html('Error occurred! Member was not deactivated.');
		}
	});
}

//activate member
function activate(username) {
	$('#active-error').hide();
	appRoutes.controllers.AdminController.activate(username).ajax( {
		success : function (response) {
			if(response.status == 'OK') {
				var oTable2 = $('#memberTable').DataTable();
				var data3 = oTable2.fnGetData();
				for (var e in data3) {
				    if (data3[e].username == username) {
				        $('#memberTable').dataTable().fnUpdate("<div class='active-type'>" +
	        		    		"<button type='button' id='deactivateBtn' class='btn blue' " + 
	        		    		"onclick='javascript:deactivate(\"" +username + "\");'>Deactivate</button>" +
	        		    		"</div>", parseInt(e), 3, true, true);
	
				        break;
				    }
				}
			} else {
				$('#active-error').show();
				$('#active-error').html(response.error1);
			}
		},
		error : function (error) {
			$('#active-error').show();
			$('#active-error').html('Error occurred! Member was not activated.');
		}
	});
}
//change password of username.
function changePassword(username) {
	clearPassFields();
	$('#pass-error').hide();
	$(".modal-body #membername").val( username );
	$("#passwordDialog").modal('show');

}
//clear fields of change password modal.
function clearPassFields() {
	$("#membername").val("");
	$("#password").val("");
	$("#repeatePassword").val("");
}
//add e-credits for facilitator.
function addECredits(username) {
	clearECreditFields();
	$('#ecredit-error').hide();
	$(".modal-body #membername").val( username );
	$("#eCreditDialog").modal('show');
}
//clear fields of change password modal.
function clearECreditFields() {
	$("#addecredit").val("");
}


