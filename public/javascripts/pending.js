function goMember(object){
    var selectedPendingId = $(object).parent().find('input:hidden').val();
	  $('#selected_pending_id').val(selectedPendingId);
	  $("#userName_id_text").attr("disabled","true"); 
	  $("#signeeName_id_text").attr("disabled","true"); 
}

function clearUI() {
	$('#errorBox').hide();
	$('#error1').hide();
	$('#error1').html('');
	$('#successBox').hide();
}
$('#affiliate_network_type').hide();

$("#member_type").change(function () {
    var dropVal = $('#member_type').val();
    if(dropVal == "merchant"){
    	$('#club_member_type').hide();
		$('#affiliate_network_type').hide();
    }
});


$('#optionsClubMemberTypePremium').click(function() { 
	$('#affiliate_network_type').show();
});

$('#optionsClubMemberTypeClassic').click(function() { 
	$('#affiliate_network_type').hide();
});


$('#confirmApproveBtn').click(function(){
    $('#approveModal').modal('hide');
    var id = $('#selected_pending_id').val();
	  var memberType = $('#member_type option:selected').val();
	  var clubMemberType = $("input[name=optionsClubMemberType]:checked").val(); 
	  var networkAffiliate = $("input[name=optionAffialiateNetworkType]:checked").val();
	  
	  if(memberType == "merchant"){
		  clubMemberType = null;
		  networkAffiliate = null;
	   }
	  
	  if(clubMemberType == "classic"){
		  networkAffiliate = null;
	   }
	  
	  appRoutes.controllers.MemberController.approveMember(id,memberType, clubMemberType, networkAffiliate).ajax( {
		  success : function (response) {
				  clearUI();
				  if(response.status == 'OK') {
					  $('#successBox').show();
					  $("#confirmApproveBtn").attr("disabled","true"); 
					  //TODO convert tables to AJAX?
					   window.location.reload();
				  }else {
					  $('#errorBox').show();
					  if(response.error1 != "") {
						  $('#error1').show();
						  $('#error1').html(response.error1);
					  }
					  
				  }
		  }
	  });
    resetApproveDialog();
});
function resetApproveDialog() {
	// reset
	$('#selected_pending_id').val("");
    $('input:radio').attr('checked', false);
    var id = $('#selected_pending_id').val();
    
}
