$(document).ready(function () {
	$("#userName").attr("disabled","true"); 
	clearUI();
	clearFields();
	/*testing */
	$('#submitAccountBtn').on( 'click', function (e) {
		var placementUsername = $('#placementUserName').val();
		var userName = $('#userName').val();
		if(placementUsername === '') {
			$("#placementDiv").attr('class', 'form-group has-error');
			$('#error1').show();
			$('#error1').html('Please enter Placement Username!');
		}else {
			$("#usernameDiv").attr('class', 'form-group');
			$('#error1').hide();
		}
		if(userName === '') {
			errorUsername('Please enter username!');
		}
		if(placementUsername && userName ){
			$('#errorBox').hide();
			bootbox.confirm("Confirm addtional account "+ userName + " placement to " + placementUsername  + ".", function(result) {
				if(result) {
					bootbox.hideAll();
					submitTransfer(userName ,placementUsername);
				}
			}); 
		}
	});
	//reset fields
	$('#resetBtn').on( 'click', function (e) {
		clearUI();
		clearFields();
	});
});

function clearUI() {
	$('#errorBox').hide();
	$('#error1').hide();
	$('#error2').hide();
	$('#error1').html('');
	$('#error2').html('');
	$('#successBox').hide();
	$("#usernameDiv").attr('class', 'form-group');
	$("#amountDiv").attr('class', 'form-group');
}

function submitTransfer(username, placementUser) {
	appRoutes.controllers.MemberController.submitAddOwnAccount(username, placementUser).ajax( {
		success : function (response) {
			  clearUI();
			  if(response.status == 'OK') {
				  $('#successBox').show();
				  $('#userName').val(response.username);
				  $('#placementUserName').val("");
				  
			  }else {
				  $('#errorBox').show();
				  if(response.error1 != "") {
					  $('#errorBox').show();
					  $('#errorBox').append(response.error1);
				  }
				  
			  }
		},
		error : function (error) {
			//TODO
		}
	});
}

function clearFields() {
	$('#memberUserName').val("");
	$('#amount').val("");
}

function errorUsername(message) {
	$("#posDiv").attr('class', 'form-group has-error');
	$('#error2').show();
	$('#error2').html(message);
	$('#amount').val("");
}