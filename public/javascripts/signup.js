$(document).ready(function () {
	clearSignUpFields();
	$('#signee').hide();
	$('#success').hide();
	$('#signupBtn').on( 'click', function (e) {
		
		if($("#desired").val() === '') {
			$('#signee').show();
			$('#signee').html('Please enter username.');
		}else {
			if($('#agreed').is(':checked')) {
				processSignUp();
			}else {
				$('#signee').show();
				$('#signee').html('Please check the agreement.');
			}
		}
	});
	
	$('#kaptchaImage').click(function () { $(this).attr('src', '/captcha?' + Math.floor(Math.random()*100) ); })
});

function processSignUp(){
	$('#signee').show();
	$('#success').hide();
	$('#signee').html("Please wait while saving application...");
	var dataParam = {
			"username": $("#desired").val(),
			"firstName": $("#fname").val(),
			"middleName": $("#mname").val(),
			"lastName": $("#lname").val(),
			"email": $("#email").val(),
			"password": $("#pass").val(),
			"repeatPassword": $("#retype").val(),
			"sponsor": $("#sponsor").val(),
			"kaptcha": $("#verification").val()
			};
	$.ajax({
	    type: 'POST',
	    url: '/signup',
	    data: JSON.stringify(dataParam), 
	    contentType: "application/json",
	    dataType: 'json',
	    success: function(response) {
	    	if(response.status == 'OK') {
	    		clearSignUpFields();
	    		$('#kaptchaImage').click();
	    		$('#success').show();
			}else {
				$("#pass").val("");
				$("#retype").val("");
				$("#verification").val("");
				$('#kaptchaImage').click();
				$('#signee').show();
				$('#signee').html(response.error);
			}
	    },
	    error: function (xhr, ajaxOptions, thrownError) {
	        alert(xhr.status);
	        alert(thrownError);
	    }
	});
}

function clearSignUpFields() {
	$("#desired").val("");
	$("#fname").val("");
	$("#mname").val("");
	$("#lname").val("");
	$("#email").val("");
	$("#sponsor").val("");
	$("#pass").val("");
	$("#retype").val("");
	$("#verification").val("");
	$('#agreed').prop('false', true);
	$('#signee').hide();
}