$(document).ready(function () {
	/*set gender*/
	 $("#gender").val($("#selectedGender").val());
	 /*countries*/
	 appRoutes.controllers.MemberProfileController.getCountries().ajax({
		  success : function (response) {
			  if(response.status === 'OK') {
				  var data = response.countries;
				  $(data).each( function(i, item) {
					  $("#countryId").append('<option value="' + item.id + '">' + item.name + '</option>');
				  });
				  $("#countryId").val($("#selectedCountryId").val());
			  }
		  }
	  });
});