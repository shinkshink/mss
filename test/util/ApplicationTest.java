package util;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;

import play.test.FakeApplication;
import play.test.Helpers;

public class ApplicationTest {
	public static final String FACILITATOR_USERNAME_JAY = "jay";
	public static final String CLUB_MEMBER_USERNAME_DAN = "daniel";
	public static final String DEFAULT_MEMBER_RANK = "consultant";
	public static FakeApplication app;

	@Before
	public void startApp() throws IOException {
		app = Helpers.fakeApplication(Helpers.inMemoryDatabase());
	    Helpers.start(app);
	}

	@After
	public void stopApp() {
		Helpers.stop(app);
	}

}
