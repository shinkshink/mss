package models;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import org.junit.Test;

import play.db.jpa.JPA;
import util.ApplicationTest;

public class CountryTest extends ApplicationTest {
	@Test
	public void findById() {
		JPA.withTransaction(new play.libs.F.Callback0() {
			@SuppressWarnings("unchecked")
			public void invoke() {
				List<Country> result = JPA.em()
						.createQuery("from Country order by name")
						.getResultList();
				assertThat(result).isNotEmpty();
			}
		});
	}
}
