package models;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import play.db.jpa.JPA;
import util.ApplicationTest;

public class RewardTest extends ApplicationTest {
	@Test
	public void testRewardAndRewardType() {
		JPA.withTransaction(new play.libs.F.Callback0() {
			public void invoke() {
				/**Member result = JPA.em().find(Member.class, 2L);
				Date today = new GregorianCalendar().getTime();
				RewardType cashRewardType = RewardDao
						.findRewardTypeByCode(RewardTypeEnum.CASH.getCode());
				Reward reward1 = new Reward(result, cashRewardType,
						new BigDecimal(50L), today);
				RewardDao.persist(reward1);

				RewardType eCreditRewardType = RewardDao
						.findRewardTypeByCode(RewardTypeEnum.E_CREDIT.getCode());
				Reward reward2 = new Reward(result, eCreditRewardType,
						new BigDecimal(50L), today);
				RewardDao.persist(reward2);

				result.rewards.add(reward1);
				result.rewards.add(reward2);
				MemberDao.persist(result);*/

				Member actual = JPA.em().find(Member.class, 2L);
				assertEquals(4, actual.rewards.size());
			}
		});
	}
}
