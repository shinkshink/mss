package models;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

import play.db.jpa.JPA;
import util.ApplicationTest;

public class RewardTypeTest extends ApplicationTest {
	@Test
	public void findById() {
		JPA.withTransaction(new play.libs.F.Callback0() {
			public void invoke() {
				RewardType result = JPA.em().find(RewardType.class, 2L);
				assertThat(result.code).isEqualTo("ecredit");
				assertThat(result.description).isEqualTo("E-Credit");
			}
		});
	}
}
