package models;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

import play.db.jpa.JPA;
import util.ApplicationTest;

public class MemberTypeTest extends ApplicationTest {
	@Test
	public void findById() {
		JPA.withTransaction(new play.libs.F.Callback0() {
			public void invoke() {
				MemberType result = JPA.em().find(MemberType.class, 1L);
				assertThat(result.code).isEqualTo("facilitator");
				assertThat(result.description).isEqualTo("Facilitator");
			}
		});
	}
}
