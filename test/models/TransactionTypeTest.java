package models;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

import play.db.jpa.JPA;
import util.ApplicationTest;

public class TransactionTypeTest extends ApplicationTest {
	@Test
	public void findById() {
		JPA.withTransaction(new play.libs.F.Callback0() {
			public void invoke() {
				TransactionType result = JPA.em().find(TransactionType.class,
						2L);
				assertThat(result.code).isEqualTo("credit");
				assertThat(result.description).isEqualTo("Credit");
			}
		});
	}
}
