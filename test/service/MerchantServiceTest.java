package service;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.Set;

import models.ClubMemberEnrollTypeEnum;
import models.Member;
import models.MemberTypeEnum;
import models.Purchase;
import models.Reward;
import models.RewardTypeEnum;
import models.TransactionTypeEnum;
import models.Transfer;
import models.TransferDetail;

import org.junit.Test;

import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import util.ApplicationTest;
import dao.MemberDao;


public class MerchantServiceTest extends ApplicationTest {
	private static final String MERCHANT_USERNAME = "testusermerchant";
	private static final String CLUB_MEMBER_USERNAME_JOHN = "john";
	private static final String CLUB_MEMBER_USERNAME_KELLY = "kelly";
	private static final String CLUB_MEMBER_USERNAME_LINDA = "linda";
	
	private void setUpMerchantData() throws Exception {
		//Create merchanto
		Member member = MemberService.signUpMember(MemberServiceTest.createMemberDto(MERCHANT_USERNAME, FACILITATOR_USERNAME_JAY));
		MemberService.approveMember(member.id, MemberTypeEnum.MERCHANT, member.sponsor ,ClubMemberEnrollTypeEnum.CLASSIC, null);
	}
	
	private void setUpMemberJohn() throws Exception {
		Member member2 = MemberService.signUpMember(MemberServiceTest.createMemberDto(CLUB_MEMBER_USERNAME_JOHN, CLUB_MEMBER_USERNAME_DAN));
		MemberService.approveMember(member2.id, MemberTypeEnum.CLUB_MEMBER, member2.sponsor, ClubMemberEnrollTypeEnum.CLASSIC, null);
		BigDecimal amountTransferred = new BigDecimal("200.00");
		MemberService.transferCredit(member2.sponsor.id, member2.id, amountTransferred);
	}
	
	private void setUpMemberKelly() throws Exception {
		Member member2 = MemberService.signUpMember(MemberServiceTest.createMemberDto(CLUB_MEMBER_USERNAME_KELLY, CLUB_MEMBER_USERNAME_JOHN));
		MemberService.approveMember(member2.id, MemberTypeEnum.CLUB_MEMBER, member2.sponsor, ClubMemberEnrollTypeEnum.CLASSIC, null);
		BigDecimal amountTransferred = new BigDecimal("100.00");
		MemberService.transferCredit(member2.sponsor.id, member2.id, amountTransferred);
	}
	
	private void setUpMemberLinda() throws Exception {
		Member member2 = MemberService.signUpMember(MemberServiceTest.createMemberDto(CLUB_MEMBER_USERNAME_LINDA, CLUB_MEMBER_USERNAME_KELLY));
		MemberService.approveMember(member2.id, MemberTypeEnum.CLUB_MEMBER,  member2.sponsor, ClubMemberEnrollTypeEnum.CLASSIC, null);
	}
	
	@Transactional
	@Test
	public void testPurchase() {
		JPA.withTransaction(new play.libs.F.Callback0() {
			public void invoke() throws Exception {
				setUpMerchantData();
				setUpMemberJohn();
				setUpMemberKelly();
				setUpMemberLinda();
				
				Member merchant = MemberDao.findByUsername(MERCHANT_USERNAME);
				Member jay = MemberDao.findByUsername(FACILITATOR_USERNAME_JAY);
				
				//merchant data has zero ecredits
				//should transfer from facilitator before the members can purchases
				BigDecimal amountTransferred = new BigDecimal("500.00");

				MemberService.transferCredit(jay.id, merchant.id, amountTransferred);
				
				BigDecimal amountPurchased = new BigDecimal("4000.00");
				
				
				Member linda = MemberDao.findByUsername(CLUB_MEMBER_USERNAME_LINDA);
				MerchantService.purchase(merchant.id, linda.id, amountPurchased);
				
				//check if there's purchase
				Member merchantUpdated = MemberDao.findByUsername(MERCHANT_USERNAME);
				Member lindaUpdated = MemberDao.findByUsername(CLUB_MEMBER_USERNAME_LINDA);
				Purchase purchaseActual = lindaUpdated.purchasesAsMember.iterator().next();
				assertNotNull(purchaseActual);
				assertNotNull(purchaseActual.id);
				assertNotNull(purchaseActual.date);
				assertEquals(lindaUpdated.id, purchaseActual.buyer.id);
				assertEquals(merchantUpdated.id, purchaseActual.merchant.id);
				assertEquals(0, purchaseActual.amount.compareTo(new BigDecimal("4000.00")));
				
				//check personal cash reward
				assertEquals(0, lindaUpdated.getCashReward().amount.compareTo(new BigDecimal(".6")));// 0 equals, 1 greater than, -1 less than
				
				//percentages
				//level 1
				Member level1 = lindaUpdated.sponsor;//credit to kelly
				Reward cashReward1 = level1.getCashReward();
				assertEquals(RewardTypeEnum.CASH.getCode(), cashReward1.rewardType.code);
				assertEquals(0, cashReward1.amount.compareTo(new BigDecimal(".16")));// 0 equals, 1 greater than, -1 less than
				//check transfers
				Set<Transfer> transfers = level1.creditTransfers;
				//first transfer
				assertEquals(3, transfers.size());
				Transfer transfer1 = null;
				for (Transfer transfer : transfers) {
					if(RewardTypeEnum.CASH.getCode().equals(transfer.rewardType.code)) {
						transfer1 = transfer;
						assertEquals(level1, transfer1.receiver);
						assertEquals(RewardTypeEnum.CASH.getCode(), transfer1.rewardType.code);
						assertEquals(merchant, transfer1.sender);
					}
				}
				assertNotNull(transfer1);
				//check transfer details
				TransferDetail transferDetail1 = transfer1.transferDetails.iterator().next();
				assertNotNull(transferDetail1);
				assertEquals(TransactionTypeEnum.CREDIT.getCode(), transferDetail1.transactionType.code);
				assertEquals(0, transferDetail1.previousAmount.compareTo(new BigDecimal("0")));
				assertEquals(0, transferDetail1.availableAmount.compareTo(new BigDecimal(".16")));
				assertEquals(level1, transferDetail1.member);
				//level 2
				Member level2 = MemberDao.findByUsername(CLUB_MEMBER_USERNAME_KELLY).sponsor;//credit to john
				Reward cashReward2 = level2.getCashReward();
				assertEquals(RewardTypeEnum.CASH.getCode(), cashReward2.rewardType.code);
				assertEquals(0, cashReward2.amount.compareTo(new BigDecimal(".16")));// 0 equals, 1 greater than, -1 less than
				//check transfers
				Set<Transfer> transfers2 = level2.creditTransfers;
				//first transfer
				assertEquals(3, transfers2.size());
				Transfer transfer2 = null;
				for (Transfer transfer : transfers2) {
					if(RewardTypeEnum.CASH.getCode().equals(transfer.rewardType.code)) {
						transfer2 = transfer;
						assertEquals(level2, transfer2.receiver);
						assertEquals(RewardTypeEnum.CASH.getCode(), transfer2.rewardType.code);
						assertEquals(merchant, transfer2.sender);
					}
				}
				assertNotNull(transfer2);
				//check transfer details
				TransferDetail transferDetail2 = transfer2.transferDetails.iterator().next();
				assertNotNull(transferDetail2);
				assertEquals(TransactionTypeEnum.CREDIT.getCode(), transferDetail2.transactionType.code);
				assertEquals(0, transferDetail2.previousAmount.compareTo(new BigDecimal("0")));
				assertEquals(0, transferDetail2.availableAmount.compareTo(new BigDecimal(".16")));
				assertEquals(level2, transferDetail2.member);
				//level 3
				Member level3 = MemberDao.findByUsername(CLUB_MEMBER_USERNAME_JOHN).sponsor;//credit to dan
				Reward cashReward3 = level3.getCashReward();
				assertEquals(RewardTypeEnum.CASH.getCode(), cashReward3.rewardType.code);
				assertEquals(0, cashReward3.amount.compareTo(new BigDecimal("3000.14")));// (4000/2000) * .07 * 40 = 5.6
				//check transfers
				Set<Transfer> transfers3 = level3.creditTransfers;
				//first transfer
				assertEquals(4, transfers3.size());
				Transfer transfer3 = null;
				for (Transfer transfer : transfers3) {
					if(RewardTypeEnum.CASH.getCode().equals(transfer.rewardType.code)) {
						transfer3 = transfer;
						assertEquals(level3, transfer3.receiver);
						assertEquals(RewardTypeEnum.CASH.getCode(), transfer3.rewardType.code);
						assertEquals(merchant, transfer3.sender);
					}
				}
				assertNotNull(transfer3);
				//check transfer details
				TransferDetail transferDetail3 = transfer3.transferDetails.iterator().next();
				assertNotNull(transferDetail3);
				assertEquals(TransactionTypeEnum.CREDIT.getCode(), transferDetail3.transactionType.code);
				assertEquals(0, transferDetail3.previousAmount.compareTo(new BigDecimal("3000.0")));
				assertEquals(0, transferDetail3.availableAmount.compareTo(new BigDecimal("3000.14")));
				assertEquals(level3, transferDetail3.member);
			}
		});
	}
}