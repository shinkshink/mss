package service;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.Set;

import models.ClubMemberEnrollTypeEnum;
import models.Member;
import models.MemberTypeEnum;
import models.NetworkAffiliateTypeEnum;
import models.Reward;
import models.RewardType;
import models.RewardTypeEnum;
import models.TransactionTypeEnum;
import models.Transfer;
import models.TransferDetail;
import models.dto.MemberDto;

import org.junit.Test;

import play.db.jpa.JPA;
import util.ApplicationTest;
import dao.MemberDao;
import exception.MssException;

public class MemberServiceTest extends ApplicationTest {
	@Test
	public void testSignUpMember() {
		JPA.withTransaction(new play.libs.F.Callback0() {
			public void invoke() {
				String username = "tester";
				MemberService.signUpMember(createMemberDto(username,
						FACILITATOR_USERNAME_JAY));
				Member actual = MemberDao.findByUsername(username);
				assertEquals(username, actual.username);
				assertEquals("jay", actual.sponsor.username);
				Member jay = MemberDao.findByUsername("jay");
				assertEquals(7, jay.associates.size());
			}
		});
	}

	@Test
	public void testApproveMemberClassic() {
		JPA.withTransaction(new play.libs.F.Callback0() {
			public void invoke() throws Exception {
				String username = "tester";
				Member dan = MemberService.signUpMember(createMemberDto(
						username, FACILITATOR_USERNAME_JAY));
				try {
					MemberService.approveMember(dan.id,
							MemberTypeEnum.CLUB_MEMBER, dan.sponsor, ClubMemberEnrollTypeEnum.CLASSIC, null);
				} catch (MssException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Member actual = MemberDao.findByUsername(username);
				assertNotNull(actual.approvedDate);
				assertTrue(actual.active);
				assertEquals(MemberTypeEnum.CLUB_MEMBER.getCode(),
						actual.memberType.code);
				// check the rewards
				int count = 0;
				for (Reward r : actual.rewards) {
					RewardType rewardType = r.rewardType;
					if (rewardType.code.equals(RewardTypeEnum.CASH.getCode())) {
						assertThat(r.amount).isEqualTo(new BigDecimal("0"));
						count++;
					}
					if (rewardType.code.equals(RewardTypeEnum.E_CREDIT
							.getCode())) {
						assertThat(r.amount).isEqualTo(new BigDecimal("0"));
						count++;
					}
				}
				assertThat(count).isEqualTo(2);
			}
		});
	}

	@Test
	public void testTransferCredit() {
		JPA.withTransaction(new play.libs.F.Callback0() {
			public void invoke() throws MssException {
				Member jay = MemberDao.findByUsername(FACILITATOR_USERNAME_JAY);
				Member dan = MemberDao.findByUsername(CLUB_MEMBER_USERNAME_DAN);
				BigDecimal amountTransferred = new BigDecimal("400.00");

				MemberService.transferCredit(jay.id, dan.id, amountTransferred);

				// check sender data
				Member actualSender = MemberDao.findById(jay.id);
				Reward eCreditReward = actualSender.getEcreditReward();
				assertEquals(RewardTypeEnum.E_CREDIT.getCode(),
						eCreditReward.rewardType.code);
				assertEquals(0,
						eCreditReward.amount.compareTo(new BigDecimal("9600.00")));// 0
																				// equals,
																				// 1
																				// greater
																				// than,
																				// -1
																				// less
																				// than
				// check transfers
				Set<Transfer> transfers = actualSender.debitTransfers;
				assertEquals(1, transfers.size());
				Member actualReceiver = MemberDao.findById(dan.id);
				Reward eCreditReward2 = actualReceiver.getEcreditReward();
				assertEquals(RewardTypeEnum.E_CREDIT.getCode(),
						eCreditReward2.rewardType.code);
				assertEquals(0,
						eCreditReward2.amount.compareTo(new BigDecimal("1900")));// 0
																					// equals,
																					// 1
																					// greater
																					// than,
																					// -1
																					// less
																					// than
				// check transfers
				Set<Transfer> transfers2 = actualReceiver.creditTransfers;
				assertEquals(1, transfers2.size());

				Transfer transfer1 = transfers.iterator().next();
				assertEquals(0,
						transfer1.amount.compareTo(new BigDecimal("400")));
				assertEquals(actualSender, transfer1.sender);
				assertEquals(actualReceiver, transfer1.receiver);
				assertEquals(RewardTypeEnum.E_CREDIT.getCode(),
						transfer1.rewardType.code);

				// assertEquals(merchant, transfer1.sender);
				// check transfer details
				assertEquals(2, transfer1.transferDetails.size());
				int counter = 0;
				for (TransferDetail transferDetail : transfer1.transferDetails) {
					if (TransactionTypeEnum.CREDIT.getCode().equals(
							transferDetail.transactionType.code)) {
						assertEquals(0, transferDetail.previousAmount
								.compareTo(new BigDecimal("1500")));
						assertEquals(0, transferDetail.availableAmount
								.compareTo(new BigDecimal("1900")));
						assertEquals(actualReceiver, transferDetail.member);
					}
					if (TransactionTypeEnum.DEBIT.getCode().equals(
							transferDetail.transactionType.code)) {
						assertEquals(0, transferDetail.previousAmount
								.compareTo(new BigDecimal("10000")));
						assertEquals(0, transferDetail.availableAmount
								.compareTo(new BigDecimal("9600")));
						assertEquals(actualSender, transferDetail.member);
					}
					counter++;
				}
				assertEquals(2, counter);
			}
		});
	}
	
	@Test
	public void testApproveMemberPremium() {
		JPA.withTransaction(new play.libs.F.Callback0() {
			public void invoke() throws Exception {
				String username = "tester";
				Member dan = MemberService.signUpMember(createMemberDto(
						username, CLUB_MEMBER_USERNAME_DAN));
				try {
					MemberService.approveMember(dan.id,
							MemberTypeEnum.CLUB_MEMBER, dan.sponsor, ClubMemberEnrollTypeEnum.PREMIUM,  NetworkAffiliateTypeEnum.ORGANO_GOLD);
				} catch (Exception e) {
					fail();
				}

				Member actual = MemberDao.findByUsername(username);
				assertNotNull(actual.approvedDate);
				assertTrue(actual.active);
				assertEquals(MemberTypeEnum.CLUB_MEMBER.getCode(),
						actual.memberType.code);
				// check the rewards
				int count = 0;
				for (Reward r : actual.rewards) {
					RewardType rewardType = r.rewardType;
					if (rewardType.code.equals(RewardTypeEnum.CASH.getCode())) {
						assertThat(r.amount).isEqualTo(new BigDecimal("0"));
						count++;
					}
					if (rewardType.code.equals(RewardTypeEnum.E_CREDIT
							.getCode())) {
						assertThat(r.amount).isEqualTo(new BigDecimal("0"));
						count++;
					}
					if (rewardType.code.equals(RewardTypeEnum.WEALTH_POINT
							.getCode())) {
						assertThat(r.amount).isEqualTo(new BigDecimal("0"));
						count++;
					}
					if (rewardType.code.equals(RewardTypeEnum.SPONSOR
							.getCode())) {
						assertThat(r.amount).isEqualTo(new BigDecimal("0"));
						count++;
					}
				}
				assertThat(count).isEqualTo(4);
				//check 2 accounts
				assertNotNull(MemberDao.findByUsername("tester001"));
				assertNotNull(MemberDao.findByUsername("tester002"));
				
				//check the reward of sponsor if correctly applied
				Member sponsorMember = MemberDao.findByUsername(CLUB_MEMBER_USERNAME_DAN);
				
				for (Reward r : sponsorMember.rewards) {
					RewardType rewardType = r.rewardType;
					if (rewardType.code.equals(RewardTypeEnum.CASH.getCode())) {
						assertThat(r.amount).isEqualTo(new BigDecimal("3000.00000"));
					}
					if (rewardType.code.equals(RewardTypeEnum.E_CREDIT
							.getCode())) {
						assertThat(r.amount).isEqualTo(new BigDecimal("1425.00000"));
					}
					if (rewardType.code.equals(RewardTypeEnum.WEALTH_POINT
							.getCode())) {
						assertThat(r.amount).isEqualTo(new BigDecimal("0.00000"));
					}
					if (rewardType.code.equals(RewardTypeEnum.SPONSOR
							.getCode())) {
						assertThat(r.amount).isEqualTo(new BigDecimal("12.50000"));
					}
				}
				
			}
			
		});
	}
	
	

	public static MemberDto createMemberDto(String username,
			String sponsorUsername) {
		return new MemberDto(username.toLowerCase(), username,
				username.charAt(0) + "", username, username + "@gmail.com",
				"password", sponsorUsername, DEFAULT_MEMBER_RANK);
	}
}