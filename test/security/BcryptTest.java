package security;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test blowfish encryption
 * 
 * @author Dantot
 *
 */
public class BcryptTest {
	@Test
	public void testGenerateAndMatch() {
		String originalPassword = "password";
		String generatedSecuredPasswordHash = BCrypt.hashpw(originalPassword,
				BCrypt.gensalt(12));
		boolean matched = BCrypt.checkpw(originalPassword,
				generatedSecuredPasswordHash);
		Assert.assertTrue(matched);
	}
}
