package dao;

import static org.fest.assertions.Assertions.assertThat;

import java.math.BigDecimal;

import models.Member;
import models.RewardType;
import models.RewardTypeEnum;

import org.junit.Test;

import play.db.jpa.JPA;
import security.BCrypt;
import util.ApplicationTest;

public class MemberDaoTest extends ApplicationTest {
	@Test
	public void testFindById() {
		JPA.withTransaction(new play.libs.F.Callback0() {
			public void invoke() {
				Member member = MemberDao.findById(1L);
				assertThat(member.username).isEqualTo(
						"jay");
				assertThat(member.account.email).isEqualTo("jay@gmail.com");
				assertThat(BCrypt.checkpw("password", member.account.password))
						.isTrue();
			}
		});
	}

	@Test
	public void testFindMemberRewards() {
		JPA.withTransaction(new play.libs.F.Callback0() {
			public void invoke() {
				Member account = MemberDao.findById(1L);
				assertThat(account.rewards.size()).isEqualTo(1);
				RewardType rewardType = account.rewards.iterator().next().rewardType;
				if (rewardType.code.equals(RewardTypeEnum.CASH.getCode())) {
					assertThat(account.rewards.iterator().next().amount).isEqualTo(
							new BigDecimal("10000.00"));
				}
			}
		});
	}
}
