package dao;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import models.Unilevel;

import org.junit.Test;

import play.db.jpa.JPA;
import util.ApplicationTest;

public class UnilevelDaoTest extends ApplicationTest {

	@Test
	public void testFindUnilevelById() {
		JPA.withTransaction(new play.libs.F.Callback0() {
			public void invoke() {
				Unilevel actual = UnilevelDao.findUnilevelById(2);
				assertEquals(0, actual.percent.compareTo(new BigDecimal(".08")));
			}
		});
	}

	@Test
	public void testFindAll() {
		JPA.withTransaction(new play.libs.F.Callback0() {
			public void invoke() {
				assertEquals(12, UnilevelDao.findAll().size());
			}
		});
	}
}
