import play.Project._

name := "mss"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,//enable the database plugin
  javaJpa.exclude("org.hibernate.javax.persistence", "hibernate-jpa-2.0-api"),
  "postgresql" % "postgresql" % "8.4-702.jdbc4",
  "org.hibernate" % "hibernate-entitymanager" % "4.2.3.Final",
  "org.apache.poi" % "poi" % "3.8",
  "org.apache.poi" % "poi-ooxml" % "3.9"
)     

play.Project.playJavaSettings
